<main>
	<?php get_template_part('pools/step/head'); ?>

    <div class="contenu">
        <form class="blcLeft page local  step11" action="<?= CUR_URL; ?>" method="POST">
            <input type="hidden" name="cur_step" value="11">
            <input type="hidden" name="next_step" value="12">
            <div class="content content-page clr">
                <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
                <div class="container blcTitre">
                    <h1>Options non obligatoires</h1>
                </div>
                <div class="container container-local">
                    <div class="row content-local all_options">
                        <?php 
                            $option_supplementaires = get_taxs('option_supplementaire');
                            $tax = 'option_supplementaire';

                             // POSE
                            $pose      = get_term_by( 'id', $pose_id, 'pose' );
                            $class = "";

                            if(!empty($option_supplementaires)):
                                foreach($option_supplementaires as $key => $option_supplementaire):
                                    $option_supplementaire_ID     = $option_supplementaire->term_id;
                                    $option_supplementaire_name   = $option_supplementaire->name;
                                    $numero_option_supplementaire = 'option_supplementaire'.$key;
                                    $option_slug                  = $option_supplementaire->slug;

                                    $visuel              = get_field('visuel_option', $tax.'_'.$option_supplementaire_ID);
                                    
                                    $valeur_vide         = get_field('valeur_vide', $tax.'_'.$option_supplementaire_ID);
                                    $titre_option_supp   = get_field('titre_option_supp', $tax.'_'.$option_supplementaire_ID);
                                    $atout_option_supp   = get_field('atout_option_supp', $tax.'_'.$option_supplementaire_ID);
                                    
                                    $prix_selon_model    = get_field('prix_selon_model', $tax.'_'.$option_supplementaire_ID);
                                    $explain_option_supp = get_field('explain_option_supp', $tax.'_'.$option_supplementaire_ID);

                                    $prix = conditionnal_tax_price_pose($modele_id, $option_supplementaire_ID, 'option_supplementaire', $pose_id);

                                    if ( $option_slug == 'volet-hors-sol-pvc' ){
                                        if ($pose->slug == 'pret-a-plonger-basic' || $pose->slug == 'pret-a-plonger-premium'  ){
                                            $class = ' available';
                                            $Leprix = -2;
                                        }else{
                                            $class = ' pvc-disabled disabled';
                                        }
                                    }

                        ?>
                            <?php if($valeur_vide == 'oui'): ?>
                                <label  for="<?= $numero_option_supplementaire; ?>" class="col itemOption empty_value<?php echo $class; ?>">
                                    <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_option_supp; ?></span></div>
                                    <input type="hidden" name="prix_option_supplementaire" value="<?= $prix; ?>" >
                                    <input type="hidden" name="option_supplementaire_name" value="<?= $option_supplementaire_name; ?>">
                                    <input type="checkbox" name="option_supp[]" id="<?= $numero_option_supplementaire; ?>" class="cb_empty_value" value="<?= $option_supplementaire_ID; ?>">

                                    <div class="blcImg">
                                        <img src="<?= $visuel; ?>" alt="visuel">
                                    </div>

                                    <span class="icon-select"></span>
                                    <div class="blcText">
                                        <div class="titre"><?= $titre_option_supp; ?></div>
                                        <div class="texte"><?= $atout_option_supp; ?></div>
                                    </div>                             
                                </label>
                            <?php else: ?>
                                <label  for="<?= $numero_option_supplementaire; ?>" class="col itemOption other_values<?php echo $class; ?>">
                                    <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_option_supp; ?></span></div>
                                    <input type="hidden" name="prix_option_supplementaire" value="<?= $prix; ?>" >
                                    <input type="hidden" name="option_supplementaire_name" value="<?= $option_supplementaire_name; ?>">
                                    <input type="checkbox" name="option_supp[]" id="<?= $numero_option_supplementaire; ?>" dataid="<?= $numero_option_supplementaire; ?>" value="<?= $option_supplementaire_ID; ?>">

                                    <div class="blcImg">
                                        <img src="<?= $visuel; ?>" alt="visuel">
                                    </div>
                                    
                                    <span class="icon-select"></span>
                                    <div class="blcText">
                                        <div class="titre"><?= $titre_option_supp; ?></div>
                                        <div class="texte"><?= $atout_option_supp; ?></div>
                                    </div>                             
                                </label>
                            <?php endif; ?>

                        <?php endforeach; wp_reset_query(); endif; ?> 
                        
                    </div>
                </div>
            </div>

            <?php 
                // Modele
                $numero_modele = get_field('numero_modele', $modele_id);
                $croquis       = get_field('croquis', $modele_id);
                $prix_modele   = get_field('prix', $modele_id);

                // POSE
                $pose      = get_term_by( 'id', $pose_id, 'pose' );
                $prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');               

                // COULEUR
                $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
                $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

                // Margelle
                $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
                $prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

                // Eclairage
                $eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );
                $eclairage_id     = $data_step->eclairage_id;
                $eclairage_format = $data_step->eclairage_format;
                $eclairage_prix   = $data_step->eclairage_prix;
                $nb_unite         = $data_step->nb_unite;

                if ($eclairage_format == 'perso') {
                    $ecl_price = $nb_unite * $eclairage_prix;
                }else {
                    $ecl_price = $eclairage_prix;
                }
                
                // $prix_eclairage = conditionnal_price($modele_id, $eclairage_id, 'eclairage');

                // Local technique
                $local_technique      = get_term_by( 'id', $local_technique_id, 'local_technique' );
                $prix_local_technique = get_field( 'prix', 'local_technique_'.$local_technique_id );
                $sur_devis            = get_field('sur_devis', 'local_technique_'.$local_technique_id);

                // Distance Local technique
                $distance_type = $data_step->distance_type;
                $distance_value  = $data_step->distance_value;
                $distance_prix   = $data_step->distance_prix;

                if ($distance_type == 'perso') {
                    $d_price = real_distance_price($distance_value, $distance_prix);
                }else {
                    $d_price = $distance_prix;
                }

                // Skimmer
                $skimmer      = get_term_by( 'id', $skimmer_id, 'skimmer' );
                $prix_skimmer = conditionnal_price($modele_id, $skimmer_id, 'skimmer', 'prix_skimmer');

                $acces_maison      = get_term_by( 'id', $acces_maison_id, 'acces_maison' );
                $prix_acces_maison = conditionnal_price($modele_id, $acces_maison_id, 'acces_maison');

                $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle, $ecl_price, $prix_local_technique, $d_price, $prix_skimmer, $prix_acces_maison));
                $total_price_formatted = format_price($total_price);
            ?>
            <aside class="blcRight" id="aside">
                <div class="contentAside">
                    <div class="shadow">
                        <div class="recap">
                            <h2>Récapitulatif de votre simulation</h2>
                            <div class="blcModele clr">
                                <div class="modele">
                                    <span class="title-modele">Modèle</span>
                                    <span class="num-modele"><?= $numero_modele; ?></span>
                                </div>
                                <div class="plan-modele">
                                    <img src="<?= $croquis; ?>" >
                                </div>
                            </div>
                            <div class="blcPrice">
                                <div class="total">prix total</div>
                                <div class="price" oldprice="<?= $total_price ?>">
                                    <span><?= $total_price_formatted; ?></span> € HT
                                </div>
                            </div>
                            <div class="recap-step">
                                <ul>
                                    <li>Version de pose choisie : <span><?= $pose->name; ?></span> <b class="price-step"><?= sanitize_price($prix_pose); ?></b></li>
                                    <li>Couleur : <span><?= $couleur->name; ?></span> <b class="price-step"><?= sanitize_price($prix_couleur); ?></b></li>
                                    <li>Margelle : <span><?= $margelle->name; ?></span> <b class="price-step"><?= sanitize_price($prix_margelle); ?></b></li>
                                    <li>Eclairage : <span><?= $eclairage->name; ?></span> <b class="price-step"><?= sanitize_price($ecl_price); ?></b></li>
                                    <?php if($sur_devis == 'oui') : ?>
                                            <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step">Sur Devis</b></li>
                                        <?php else: ?>
                                            <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step"><?= sanitize_price($prix_local_technique); ?></b></li>
                                        <?php endif; ?>
                                    <li>Local technique : <span><?= $distance_value; ?>m de distance</span> <b class="price-step"><?= sanitize_price($d_price); ?></b></li>
                                    <li>Skimmer : <span><?= $skimmer->name; ?></span> <b class="price-step"><?= sanitize_price($prix_skimmer); ?></b></li>
                                    <li>Accès Maison : <span><?= $acces_maison->name; ?></span> <b class="price-step"><?= sanitize_price($prix_acces_maison); ?></b></li>
                                    <li class="lstOption" style="display: none;">
                                        Option supplémentaire:
                                        <ul>
                                            <li><b class="price-step">€</b></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="boutton boutton2">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </aside>
            <?php include_once('footer-step.php'); ?>
        </form>
    </div>
</main>