<main>
	<?php get_template_part('pools/step/head'); 
    ?>

    <div class="contenu">
        <form class="blcLeft page local step7" action="<?= CUR_URL; ?>" method="POST">
            <input type="hidden" name="cur_step" value="7">
            <input type="hidden" name="next_step" value="8">
            <div class="content content-page clr">
                <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
                <div class="container blcTitre">
                    <h1>Raccordement du réseau hydraulique à votre local technique</h1>
                </div>
                <div class="container container-local">
                    <div class="row content-local">
                        <?php 
                            $local_techniques = get_taxs('local_technique');
                            $tax = 'local_technique';

                            // POSE
                            $pose      = get_term_by( 'id', $pose_id, 'pose' );
                            $class = "";
                            
                            
                            if(!empty($local_techniques)):
                                foreach($local_techniques as $key => $local_technique): 
                                    $local_technique_ID     = $local_technique->term_id;
                                    $local_technique_name   = $local_technique->name;
                                    $numero_local_technique = 'local_technique'.$key;
                                    $local_technique_slug   = $local_technique->slug;

                                    $titre_local = get_field('titre_local', $tax.'_'.$local_technique_ID);
                                    $visuel      = get_field('visuel_local', $tax.'_'.$local_technique_ID);
                                    $atout_local = get_field('atout_local', $tax.'_'.$local_technique_ID);
                                    $prix        = get_field('prix', $tax.'_'.$local_technique_ID);
                                    $explain_local = get_field('explain_local', $tax.'_'.$local_technique_ID);
                                    $sur_devis = get_field('sur_devis', $tax.'_'.$local_technique_ID);

                                    if($sur_devis == 'oui'){
                                        $Leprix = -1;
                                    }else {
                                        $Leprix = $prix;
                                    }
                                    if ( $local_technique_slug == 'volet-hors-sol-pvc' ){
                                        if ($pose->slug == 'pret-a-plonger-basic' || $pose->slug == 'pret-a-plonger-premium'  ){
                                            $class = ' available';
                                            $Leprix = -2;
                                        }else{
                                            $class = ' pvc-disabled disabled';
                                        }
                                    }

                            ?>
                            <label  for="<?= $numero_local_technique; ?>" class="col<?php echo $class; ?>">
                                <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_local; ?></span></div>
                                <input type="hidden" name="prix_local_teknik" value="<?= $Leprix; ?>">
                                <input type="hidden" name="name_local_teknik" value="<?= $local_technique_name; ?>">
                                <input type="radio" name="local_teknik" id="<?= $numero_local_technique; ?>" value="<?= $local_technique_ID; ?>" >
                                <div class="blcImg">
                                    <img src="<?= $visuel; ?>" alt="visuel">
                                </div>
                                <span class="icon-select"></span>
                                <div class="blcText">
                                    <div class="titre"><?= $titre_local; ?></div>
                                    <div class="texte">
                                        <?= $atout_local; ?>
                                    </div>
                                </div>                             
                            </label>
                        <?php endforeach; wp_reset_query(); endif; ?>   

                    </div>
                </div>
            </div>

            <?php 
                // Modele
                $numero_modele = get_field('numero_modele', $modele_id);
                $croquis       = get_field('croquis', $modele_id);
                $prix_modele   = get_field('prix', $modele_id);

                // POSE
                $pose      = get_term_by( 'id', $pose_id, 'pose' );
                $prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');                

                // COULEUR
                $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
                $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

                // Margelle
                $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
                $prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

                // Eclairage
                $eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );
                // $prix_eclairage = conditionnal_price($modele_id, $eclairage_id, 'eclairage'); 

                $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle, $ecl_price));
                $total_price_formatted = format_price($total_price);
            ?>
            <aside class="blcRight" id="aside">
                <div class="contentAside">
                    <div class="shadow">
                        <div class="recap">
                            <h2>Récapitulatif de votre simulation</h2>
                            <div class="blcModele clr">
                                <div class="modele">
                                    <span class="title-modele">Modèle</span>
                                    <span class="num-modele"><?= $numero_modele; ?></span>
                                </div>
                                <div class="plan-modele">
                                    <img src="<?= $croquis; ?>" >
                                </div>
                            </div>
                            <div class="blcPrice">
                                <div class="total">prix total</div>
                                <div class="price" oldprice="<?= $total_price; ?>">
                                    <span><?= $total_price_formatted; ?></span> € HT
                                </div>
                            </div>
                            <div class="recap-step">
                                <ul>
                                    <li>Version de pose choisie : <span><?= $pose->name; ?></span> <b class="price-step"><?= sanitize_price($prix_pose); ?></b></li>
                                    <li>Couleur : <span><?= $couleur->name; ?></span> <b class="price-step"><?= sanitize_price($prix_couleur); ?></b></li>
                                    <li>Margelle : <span><?= $margelle->name; ?></span> <b class="price-step"><?= sanitize_price($prix_margelle); ?></b></li>
                                    <li>Eclairage : <span><?= $eclairage->name; ?></span> <b class="price-step"><?= sanitize_price($ecl_price); ?></b></li>
                                    <li class="item_localteck" style="display: none;">Local technique : <span></span> <b class="price-step"></b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="boutton boutton2">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </aside>
            <?php include_once('footer-step.php'); ?>
        </form>
    </div>
</main>