<div class="ecran0">
    <main>
        <?php get_template_part('pools/step/head'); ?>
    
        <div class="content">
            <div class="container">
                <h1>Simulez le prix de votre piscine</h1>
                <div class="row">
                    <div class="col clr">
                        <div class="blcText">
                            <p>Avec Olsen&G, nous créons votre devis automatiquement, sans même se déplacer*.</p>
                            <p>Plus de crainte à avoir !</p>
                            <p>Nos formules sont complètes de poses.</p>
                            <div class="blcBtn">
                                <form class="" action="<?= CUR_URL; ?>" method="POST" >
                                    <input type="hidden" name="cur_step" value="0">
                                    <input type="hidden" name="next_step" value="1">
                                    <button class="btn" type="submit">
                                        <span>A vous de configurer votre piscine</span>
                                    </button>
                                </form>
                                <p>*Ce devis reste provisoire et sous réserve de visite technique à votre domicile.</p>
                            </div>
                        </div>
                    </div>
    
                    
                </div>
            </div>
        </div>
        <div class="blcFooter">
            <div class="container">
                <div class="row">
                    <div class="col pagination">
                        <?php include_once('pagination-step.php'); ?>
                    </div>
                    <div class="col blcBtn">
                        <span class="question">Une question ? </span>
                        <a href="#" class="btn btn2"> Contactez-nous</a>
                    </div>
                </div>
            </div>        
        </div>
    </main>
</div>