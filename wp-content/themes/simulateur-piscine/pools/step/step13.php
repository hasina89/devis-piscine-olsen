<main>
	<?php get_template_part('pools/step/head'); ?>

    <?php 
        $post_id = get_the_ID();
        // Modele
        $numero_modele = get_field('numero_modele', $modele_id);
        $croquis       = get_field('croquis', $modele_id);
        $prix_modele   = get_field('prix', $modele_id);

        // POSE
        $pose        = get_term_by( 'id', $pose_id, 'pose' );
        $liste_atout = get_field('liste_atout', $tax.'_'.$pose_id); 
        $prix_pose   = conditionnal_tax_price($modele_id, $pose_id, 'pose');                 

        // COULEUR
        $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
        $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

        // Margelle
        $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
        $prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

        // Eclairage
        $eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );
        if ($eclairage_format == 'perso') {
            $ecl_price = $nb_unite * $eclairage_prix;
        }else {
            $ecl_price = $eclairage_prix;
        }      
        // $prix_eclairage = conditionnal_price($modele_id, $eclairage_id, 'eclairage');

        // Local technique
        $local_technique      = get_term_by( 'id', $local_technique_id, 'local_technique' );
        $prix_local_technique = get_field( 'prix', 'local_technique_'.$local_technique_id );
        $sur_devis            = get_field('sur_devis', 'local_technique_'.$local_technique_id);

        // Distance Local technique             
        if ($distance_type == 'perso') {
            $d_price = real_distance_price($distance_value, $distance_prix);
        }else {
            $d_price = $distance_prix;
        }

        // Skimmer
        $skimmer      = get_term_by( 'id', $skimmer_id, 'skimmer' );
        $prix_skimmer = conditionnal_price($modele_id, $skimmer_id, 'skimmer', 'prix_skimmer');

        $acces_maison      = get_term_by( 'id', $acces_maison_id, 'acces_maison' );
        $prix_acces_maison = conditionnal_price($modele_id, $acces_maison_id, 'acces_maison');

        $prix_option_supp = 0;
        if(!empty($option_supp_ids)){
            foreach ($option_supp_ids as $option_supp_id) {
                $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
                $prix_option_supp += $item_price;
            }            
        }

        $traitement_eau      = get_term_by( 'id', $traitement_eau_id, 'eau' );
        // $prix_traitement_eau = conditionnal_price($modele_id, $traitement_eau_id, 'eau');
        $prix_traitement_eau = price_from_pose($pose_id, $traitement_eau_id, 'eau');

        $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle, $ecl_price, $prix_local_technique, $d_price, $prix_skimmer, $prix_acces_maison, $prix_option_supp, $prix_traitement_eau));
        $total_price_final = format_price($total_price);

        $tvac = format_price(total_price_tvac($total_price, 21));


        /*----------------*/

        $user_datas = get_user_pool_data($current_user_id);

        $nom_user = $user_datas[0]->nom;
        $prenom_user = $user_datas[0]->prenom;
        $ville_user = $user_datas[0]->ville;
        $code_postal_user = $user_datas[0]->code_postal;
        $telephone_user = $user_datas[0]->telephone;
        $email_user = $user_datas[0]->email;
        $customer_infos = get_customer_info($current_user_id)[0];

        // Email sender 
        $sender_email = get_field('sender_email', 'option');
        $cg_text = get_field('condition_general', $post_id);
        $cg_pdf = get_field('condition_general_pdf', $post_id);

        // Code promo
        $activation_code_promo = get_field('activation', 'option');
        $code_promo_admin = get_field('code_promo', 'option');
        $validite_code_promo_admin = get_field('validite_code', 'option');

        $my_code_promo = get_customer_code_promo($current_user_id);


        // Subscribe on Mailchimp
        subscribeMailchimp($email_user, $prenom_user, $nom_user, $telephone_user);
        

        // Sending email to Admin
        ob_start();
            include( get_template_directory() ."/pools/email/admin_step13.php");
        $body_mail = ob_get_clean();
             
        // Function to change sender name
        function wpb_sender_name( $original_email_from ) {
            return 'Piscine Olsen & G';
        }
        add_filter( 'wp_mail_from_name', 'wpb_sender_name' );
        
        
        $destinataire = $email_user;
        $cc_email = get_field('cc_email', 'option');
        $headers = array("From: $sender_email", 'Content-Type: text/html; charset=UTF-8');
        if($cc_email) {
            $headers[] = "Cc: $cc_email";
        }

        // send email to Admin
        // wp_mail( $sender_email, "Récapitulatif d'une simulation sans code promo", $body_mail, $headers );

        // show wp_mail() errors
        /*add_action( 'wp_mail_failed', 'onMailError', 10, 1 );
        function onMailError( $wp_error ) {
            echo "<pre>";
            print_r($wp_error);
            echo "</pre>";
        } */     


    ?>

    <div class="contenu">
        <div class="confirmation" >
            <div class="content content-page clr">
                <div class="container">
                    <h1>Récapitulatif de votre simulation</h1>
                </div>
                <div class="container container-confirmation">
                    <div class="row content-confirmation clr">
                        <div  class="col col-left">
                            <div class="recap clr">
                                <div class="blcLeft">
                                    <div class="blcModele clr">
                                        <div class="modele">
                                            <span class="title-modele">Modèle</span>
                                            <span class="num-modele"><?= $numero_modele; ?></span>
                                        </div>
                                        <div class="plan-modele">
                                            <img src="<?= $croquis; ?>">
                                        </div>
                                    </div>
                                    <div class="picture_field">
                                        <div class="blcUpload">
                                            <div id="detail">
                                                <?php 
                                                    if($data_step_img):
                                                ?>
                                                    <div id="preview" style="width:100%;">
                                                        <p class="apercu">Aperçu</p>                                                    
                                                        <img id="previewimg" src="<?= $data_step_img; ?>" style="width:100%;">
                                                    </div>
                                                <?php else: ?>
                                                    <div id="preview" style="width:100%; display: none;">
                                                        <p class="apercu">Aperçu</p>                                                    
                                                        <img id="previewimg" src="" style="width:100%;">
                                                        <span id="deleteimg" title="Supprimer la photo">X</span>
                                                    </div>
                                                <?php endif; ?>
                                                <div id="result_photo"></div>
                                            </div>
                                            <form action="" enctype="multipart/form-data" id="form_photo" method="post" name="form_photo">
                                                <div id="upload">
                                                    <span>Ajouter/modifier<br/> une photo de votre jardin</span>
                                                    <input id="file" name="file" type="file">
                                                </div>
                                                <input type="hidden" name="id_user" value="<?= $current_user_id; ?>">
                                                <input type="hidden" name="cur_simulation_id" value="<?= $cur_simulation_id; ?>">
                                                <input type="hidden" name="step" value="12">
                                                <button id="submit_photo" class="btn" name="submit_photo" type="submit" >Envoyer la photo</button>
                                            </form>
                                        </div>                                        
                                    </div>
                                    <div class="blcPrice">
                                        <div class="total">prix total</div>
                                        <div class="price"><?= $total_price_final; ?> € HT</div>
                                        <div class="tvac" style="font-size: 12px; color:#3d3d3d">Prix TVAC : <?= $tvac; ?> €</div>
                                    </div>
                                </div>
                                <div class="recap-step">
                                    <ul>
                                        <li>Piscine pour : <b class="price-step"><?= $data_step_pool_time; ?></b></li>
                                        <li>Modèle : <span><?= $numero_modele; ?></span> <b class="price-step"><?= sanitize_price($prix_modele, ''); ?></b></li>
                                        <li>Version de pose choisie : <span><?= $pose->name; ?></span><span class="price-step"><?= sanitize_price($prix_pose, ''); ?></span>
                                            <?php if(!empty($liste_atout)): ?>
                                                <ul>
                                                    <?php foreach($liste_atout as $atout) : ?>
                                                        <li><?= $atout['item_atout']; ?></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            <?php endif; ?>
                                        </li>

                                        <?php if($cur_step != 3 && $next_step == 13) : ?>

                                            <li>Couleur : <span><?= $couleur->name; ?></span> <b class="price-step"><?= sanitize_price($prix_couleur, ''); ?></b></li>
                                            <li>Margelle : <span><?= $margelle->name; ?></span> <b class="price-step"><?= sanitize_price($prix_margelle, ''); ?></b></li>
                                            <li>Eclairage : <span><?= $eclairage->name; ?></span> <b class="price-step"><?= sanitize_price($ecl_price, ''); ?></b></li>
                                            <?php if($sur_devis == 'oui') : ?>
                                                <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step">Sur Devis</b></li>
                                            <?php else: ?>
                                                <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step"><?= sanitize_price($prix_local_technique); ?></b></li>
                                            <?php endif; ?>
                                            <li>Distance : <span><?= $distance_value; ?>m</span> <b class="price-step"><?= sanitize_price($d_price, ''); ?></b></li>
                                            <li>Skimmer : <span><?= $skimmer->name; ?></span> <b class="price-step"><?= sanitize_price($prix_skimmer, ''); ?></b></li>
                                            <li>Accès Maison : <span><?= $acces_maison->name; ?></span> <b class="price-step"><?= sanitize_price($prix_acces_maison, ''); ?></b></li>
                                            <li class="lstOption">
                                                Option supplémentaire:
                                                <ul>
                                                    <?php 
                                                        if(!empty($option_supp_ids)):
                                                            foreach ($option_supp_ids as $option_supp_id) :
                                                                $op_supp = get_term_by( 'id', $option_supp_id, 'option_supplementaire' );
                                                                $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
                                                    if ( $op_supp->slug != 'enlevement-des-terres' && $op_supp->slug != 'volet-hors-sol-pvc'){
                                            ?>
                                                            <li><?= $op_supp->name; ?> <b class="price-step"><?= sanitize_price($item_price); ?></b></li>
                                            <?php }else{ ?>
                                                            <li><?= $op_supp->name; ?> <b class="price-step">Sur Devis</b></li>
                                            <?php } endforeach; endif; ?>
                                                </ul>
                                            </li>
                                            <li>Traitement de l'eau : <span><?= $traitement_eau->name; ?></span> <b class="price-step"><?= sanitize_price($prix_traitement_eau, ''); ?></b></li>

                                        <?php endif; ?>

                                    </ul>
                                </div>
                            </div>
                        </div>             
                        <div class="col col-right">
                            <div class="blcConf-mail clr">
                                    <div class="blcDown">
                                        <button onClick="window.print()" class="print">Imprimer ma configuration</button>
                                    </div>
                                    <div class="textConfirm">
                                        <p>
                                            Un email avec votre configuration vous sera envoyé par email et un de nos agents prendra contact avec vous dans les plus brefs délais.
                                        </p>
                                    </div>
                                    <div class="contact">
                                        <p>
                                           Une question ou une demande de rendez-vous ? <br/><a href="tel:32476174336" title="+32 476 174 336 ">+32 476 174 336 </a>
                                        </p>
                                    </div>
                                    <div class="boutton boutton2">
                                        <form id="form_send_mail" method="POST" >                                  
                                            <input type="hidden" name="post_id" value="<?= get_the_ID(); ?>">                      
                                            <input type="hidden" name="id_user" value="<?= $current_user_id; ?>">
                                            <input type="hidden" name="email_user" value="<?= $email_user; ?>">
                                            <input type="hidden" name="modele_id" value="<?= $modele_id; ?>">
                                            <input type="hidden" name="pose_id" value="<?= $pose_id; ?>">
                                            <input type="hidden" name="couleur_id" value="<?= $couleur_id; ?>">
                                            <input type="hidden" name="margelle_id" value="<?= $margelle_id; ?>">
                                            <input type="hidden" name="eclairage_id" value="<?= $eclairage_id; ?>">
                                            <input type="hidden" name="local_technique_id" value="<?= $local_technique_id; ?>">
                                            <input type="hidden" name="distance_locale_id" value="<?= $distance_id; ?>">
                                            <input type="hidden" name="skimmer_id" value="<?= $skimmer_id; ?>">
                                            <input type="hidden" name="acces_maison_id" value="<?= $acces_maison_id; ?>">
                                            <input type="hidden" name="traitement_eau_id" value="<?= $traitement_eau_id; ?>">

                                            <input type="hidden" name="cur_step" value="<?= $cur_step; ?>">
                                            <input type="hidden" name="next_step" value="<?= $next_step; ?>">
                                            
                                            <input type="hidden" name="cur_simulation_id" value="<?= $cur_simulation_id; ?>">
                                            <input type="hidden" name="pool_time" value="<?= $data_step_pool_time; ?>">
                                            
                                            <input type="hidden" name="generate_pdf_email" value="go">

                                            <a href="#" class="btn-submit" id="confirm_pool" >Recevoir les détails du devis <br> par email</a>
                                        </form>
                                    </div>
                                    <div id="response"></div> 

                                    <?php if($activation_code_promo == 'oui') : ?>                           
                                        <div class="blcCP">
                                            <h4 class="show_print">Code promo</h4>
                                            <div id="result_cp">
                                                <?php
                                                    if($my_code_promo) {
                                                        echo '<p class="successMsg">Votre code promo : <b>'.$my_code_promo.'</b></p>';
                                                    }
                                                ?>
                                            </div>
                                            <form class="formulaire clr" id="cpromo_fomr" method="POST" >
                                                <input type="hidden" name="id_user" value="<?= $current_user_id; ?>">
                                                <input type="hidden" name="step" value="12">
                                                <div class="row_">
                                                    <div class="clr col1">
                                                        <div class="blcChpCP">
                                                            <div class="chp">
                                                                <label for="cpromo_chp" class="placeholder">Code Promo</label>
                                                                <input type="text" class="form-control" id="cpromo_chp" name="code_promo" >
                                                            </div>
                                                            <div class="box_btnsend">
                                                                <button class="btnsend btn" id="btnsend" type="button" name="btnsend">Valider</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="info_code">
                                                <p>DEMANDEZ VOTRE CODE PROMO DU MOMENT AU <a href="tel:+32476174336">+32 476 174 336</a></p>
                                            </div>
                                            <div class="retry">
                                                <p>
                                                    <a href="<?= site_url(); ?>" class="btn_retry btn">Recommencer une simulation</a>
                                                </p>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                </div>                          
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
            <?php include_once('footer-step.php'); ?>
        </div>
    </div>
    <div class="blcPdf">
        <p>&nbsp;</p>
        <hr class="line_condition">
        <p>&nbsp;</p>
        <?php the_field('condition_general'); ?>
        <table width="100%">
            <tr>
                <td>
                    <p>
                        <strong>Pour acceptation "lu et approuvé"</strong>
                    </p>
                    <span class="texte">Sous réserve de visite technique de notre technicien.</span>
                </td>
            </tr>
        </table>
    </div>
</main>