<main>
	<?php get_template_part('pools/step/head'); ?>

    <?php 
        // Modele
        $numero_modele = get_field('numero_modele', $modele_id);
        $croquis       = get_field('croquis', $modele_id);
        $prix_modele   = get_field('prix', $modele_id);

        // POSE
        $pose        = get_term_by( 'id', $pose_id, 'pose' );
        $liste_atout = get_field('liste_atout', $tax.'_'.$pose_id); 
        $prix_pose   = conditionnal_tax_price($modele_id, $pose_id, 'pose');                 

        // COULEUR
        $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
        $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

        // Margelle
        $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
        $prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

        // Eclairage
        $eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );
        if ($eclairage_format == 'perso') {
            $ecl_price = $nb_unite * $eclairage_prix;
        }else {
            $ecl_price = $eclairage_prix;
        }        
        // $prix_eclairage = conditionnal_price($modele_id, $eclairage_id, 'eclairage');

        // Local technique
        $local_technique      = get_term_by( 'id', $local_technique_id, 'local_technique' );
        $prix_local_technique = get_field( 'prix', 'local_technique_'.$local_technique_id );

        // Distance Local technique             
        if ($distance_type == 'perso') {
            $d_price = $distance_value * $distance_prix;
        }else {
            $d_price = $distance_prix;
        }

        // Skimmer
        $skimmer      = get_term_by( 'id', $skimmer_id, 'skimmer' );
        $prix_skimmer = conditionnal_price($modele_id, $skimmer_id, 'skimmer', 'prix_skimmer');

        $acces_maison      = get_term_by( 'id', $acces_maison_id, 'acces_maison' );
        $prix_acces_maison = conditionnal_price($modele_id, $acces_maison_id, 'acces_maison');

        $prix_option_supp = 0;
        if(!empty($option_supp_ids)){
            foreach ($option_supp_ids as $option_supp_id) {
                $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
                $prix_option_supp += $item_price;
            }
        }

        $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle, $prix_eclairage, $prix_local_technique, $d_price, $prix_skimmer, $prix_acces_maison, $prix_option_supp));
        $total_price = format_price($total_price);
    ?>

    <div class="contenu">
        <form class="blcLeft page local  step12" action="<?= CUR_URL; ?>" method="POST">
            <input type="hidden" name="cur_step" value="12">
            <input type="hidden" name="next_step" value="13">
            <div class="content content-page clr">
                <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
                <div class="container blcTitre">
                    <h1>Sélectionnez votre traitement de l'eau</h1>
                </div>
                <div class="container container-local">
                    <div class="row content-local all_options">
                        <?php 
                            $eaux = get_taxs('eau');
                            $tax = 'eau';

                            if(!empty($eaux)):
                                foreach($eaux as $key => $eau):
                                    $eau_ID     = $eau->term_id;
                                    $eau_name   = $eau->name;
                                    $numero_eau = 'eau'.$key;

                                    $titre_eau        = get_field('titre_eau', $tax.'_'.$eau_ID);
                                    $atout_eau        = get_field('atout_eau', $tax.'_'.$eau_ID);  
                                    $explain_eau      = get_field('explain_eau', $tax.'_'.$eau_ID);
                                    $prix = price_from_pose($pose_id, $eau_ID, 'eau');
                        ?>

                            <label  for="<?= $numero_eau; ?>" class="col">
                                <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eau; ?></span></div>
                                <input type="hidden" name="prix_eau" value="<?= $prix; ?>" >
                                <input type="hidden" name="eau_name" value="<?= $eau_name; ?>">
                                <input type="radio" name="traitement_eau" id="<?= $numero_eau; ?>" value="<?= $eau_ID; ?>" required >
                                <span class="icon-select"></span>
                                <div class="blcText">
                                    <div class="titre"><?= $titre_eau; ?></div>
                                    <div class="texte"><?= $atout_eau; ?></div>
                                </div>                             
                            </label>
                        <?php endforeach; wp_reset_query(); endif; ?> 
                        
                    </div>
                </div>
            </div>

            <?php 
                // Modele
                $numero_modele = get_field('numero_modele', $modele_id);
                $croquis       = get_field('croquis', $modele_id);
                $prix_modele   = get_field('prix', $modele_id);

                // POSE
                $pose      = get_term_by( 'id', $pose_id, 'pose' );
                $prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');               

                // COULEUR
                $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
                $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

                // Margelle
                $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
                $prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

                // Eclairage
                $eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );
                $eclairage_id     = $data_step->eclairage_id;
                $eclairage_format = $data_step->eclairage_format;
                $eclairage_prix   = $data_step->eclairage_prix;
                $nb_unite         = $data_step->nb_unite;

                if ($eclairage_format == 'perso') {
                    $ecl_price = $nb_unite * $eclairage_prix;
                }else {
                    $ecl_price = $eclairage_prix;
                }
                
                // $prix_eclairage = conditionnal_price($modele_id, $eclairage_id, 'eclairage');

                // Local technique
                $local_technique      = get_term_by( 'id', $local_technique_id, 'local_technique' );
                $prix_local_technique = get_field( 'prix', 'local_technique_'.$local_technique_id );
                $sur_devis            = get_field('sur_devis', 'local_technique_'.$local_technique_id);

                // Distance Local technique
                $distance_type = $data_step->distance_type;
                $distance_value  = $data_step->distance_value;
                $distance_prix   = $data_step->distance_prix;

                if ($distance_type == 'perso') {
                    $d_price = real_distance_price($distance_value, $distance_prix);
                }else {
                    $d_price = $distance_prix;
                }

                // Skimmer
                $skimmer      = get_term_by( 'id', $skimmer_id, 'skimmer' );
                $prix_skimmer = conditionnal_price($modele_id, $skimmer_id, 'skimmer', 'prix_skimmer');

                $acces_maison      = get_term_by( 'id', $acces_maison_id, 'acces_maison' );
                $prix_acces_maison = conditionnal_price($modele_id, $acces_maison_id, 'acces_maison');

                $prix_option_supp = 0;
                if(!empty($option_supp_ids)){
                    foreach ($option_supp_ids as $option_supp_id) {
                        $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
                        $prix_option_supp += conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
                    }                    
                }

                $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle, $ecl_price, $prix_local_technique, $d_price, $prix_skimmer, $prix_acces_maison, $prix_option_supp));
                $total_price_formatted = format_price($total_price);
            ?>
            <aside class="blcRight" id="aside">
                <div class="contentAside">
                    <div class="shadow">
                        <div class="recap">
                            <h2>Récapitulatif de votre simulation</h2>
                            <div class="blcModele clr">
                                <div class="modele">
                                    <span class="title-modele">Modèle</span>
                                    <span class="num-modele"><?= $numero_modele; ?></span>
                                </div>
                                <div class="plan-modele">
                                    <img src="<?= $croquis; ?>" >
                                </div>
                            </div>
                            <div class="blcPrice">
                                <div class="total">prix total</div>
                                <div class="price" oldprice="<?= $total_price ?>">
                                    <span><?= $total_price_formatted; ?></span> € HT
                                </div>
                            </div>
                            <div class="recap-step">
                                <ul>
                                    <li>Version de pose choisie : <span><?= $pose->name; ?></span> <b class="price-step"><?= sanitize_price($prix_pose); ?></b></li>
                                    <li>Couleur : <span><?= $couleur->name; ?></span> <b class="price-step"><?= sanitize_price($prix_couleur); ?></b></li>
                                    <li>Margelle : <span><?= $margelle->name; ?></span> <b class="price-step"><?= sanitize_price($prix_margelle); ?></b></li>
                                    <li>Eclairage : <span><?= $eclairage->name; ?></span> <b class="price-step"><?= sanitize_price($ecl_price); ?></b></li>
                                    <?php if($sur_devis == 'oui') : ?>
                                        <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step">Sur Devis</b></li>
                                    <?php else: ?>
                                        <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step"><?= sanitize_price($prix_local_technique); ?></b></li>
                                    <?php endif; ?>
                                    <li>Local technique : <span><?= $distance_value; ?>m de distance</span> <b class="price-step"><?= sanitize_price($d_price); ?></b></li>
                                    <li>Skimmer : <span><?= $skimmer->name; ?></span> <b class="price-step"><?= sanitize_price($prix_skimmer); ?></b></li>
                                    <li>Accès Maison : <span><?= $acces_maison->name; ?></span> <b class="price-step"><?= sanitize_price($prix_acces_maison); ?></b></li>
                                    <li class="lstOption">
                                        Option supplémentaire:
                                        <ul>
                                            <?php 
                                                if(!empty($option_supp_ids)):
                                                    foreach ($option_supp_ids as $option_supp_id) :
                                                        $op_supp = get_term_by( 'id', $option_supp_id, 'option_supplementaire' );
                                                        $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);

                                                        if ( $op_supp->slug != 'enlevement-des-terres' && $op_supp->slug != 'volet-hors-sol-pvc' ){
                                            ?>
                                                            <li><?= $op_supp->name; ?> <b class="price-step"><?= sanitize_price($item_price); ?></b></li>
                                            <?php }else{ ?>
                                                            <li><?= $op_supp->name; ?> <b class="price-step">Sur Devis</b></li>
                                            <?php } 
                                                    endforeach; 
                                                endif; ?>
                                        </ul>
                                    </li>
                                    <li class="item_eau" style="display: none;">Traitement de l'eau : <span></span> <b class="price-step"></b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="boutton boutton2">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </aside>
            <?php include_once('footer-step.php'); ?>
        </form>
    </div>
</main>