<main>
    <?php get_template_part('pools/step/head'); ?>

    <div class="content">
        <div class="container">
            <h1>Simulez le prix de votre piscine <span>en quelques clics seulement !</span></h1>
            <form class="formulaire off clr" action="<?= CUR_URL; ?>" method="POST" >
                <div class="row">
                    <div class="col clr col1">
                        <input type="hidden" name="cur_step" value="1">
                        <input type="hidden" name="next_step" value="2">                        
                        <div class="blcChp">
                            <div class="chp form-group">
                                <label for="s1_nom" class="placeholder">Nom<span>*</span></label>
                                <input type="text" class="form-control" id="s1_nom" name="nom" required="required">
                            </div>
                            <div class="chp form-group">
                                <label for="prenom" class="placeholder">Prénom<span>*</span></label>
                                <input type="text" class="form-control" name="prenom" value="" id="prenom" required="required">
                            </div>
                            <div class="chp form-group">
                                <label for="code-p" class="placeholder">Code Postal<span>*</span></label>
                                <input type="text" class="form-control" name="code_postal" value="" id="code-p" required="required">
                            </div>
                            <div class="chp form-group">
                                <select name="pool_time" id="pool_time" class="pool_time form-control" required="required">
                                    <option value="">Je veux une piscine pour</option>
                                    <option value="Dans les 6 mois">Dans les 6 mois</option>
                                    <option value="Après 6 mois">Après 6 mois</option>
                                    <option value="Urgent">Urgent</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col col2">
                        <div class="blcChp">
                            <div class="chp form-group">
                                <label for="telephone" class="placeholder">Téléphone<span>*</span></label>
                                <input  class="form-control" type="tel" name="telephone" value=""  id="telephone">
                            </div>
                            <div class="chp form-group">
                                <label for="mail" class="placeholder">Email<span>*</span></label>
                                <input type="mail" class="form-control" name="email" value="" id="mail" required="required">
                            </div><?php 
                                        $provinces = get_field('emails_responsable','option');
                                        if ($provinces)
                                            $provinces = $provinces['le_responsable'];
                                    ?>
                            <div class="chp form-group">
                                <select name="province" id="province" class="province form-control" required="required">
                                    <option value="">Choisir votre province...</option>
                                    <?php if ($provinces) foreach( $provinces as $province ){ ?>
                                    <option value="<?php echo $province['province']; ?>"><?php echo $province['province']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="chp form-group blcCheckbox">
                                <input type="checkbox" id="regle" name="cgu"> 
                                <label for="regle">J’accepte de transmettre mes informations personnelles et accepte les <a href="<?= site_url('rgpd'); ?>" target="_blank">règles générales de protection des données</a></label>
                            </div>
                        </div>
                        <div class="boutton boutton1">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                    </div>
                    <div class="bcl_error clear">
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="blcFooter">
        <div class="container">
            <div class="row">
                <div class="col pagination">
                    <?php include_once('pagination-step.php'); ?>
                </div>
                <div class="col blcBtn">
                    <span class="question">Une question ? </span>
                    <a href="#" class="btn btn2"> Contactez-nous</a>
                </div>
            </div>
        </div>        
    </div>
</main>