<main>
	<?php get_template_part('pools/step/head'); ?>

    <div class="contenu">
        <form class="blcLeft page piscine margelle eclairage skimmer step9" action="<?= CUR_URL; ?>" method="POST">
            <input type="hidden" name="cur_step" value="9">
            <input type="hidden" name="next_step" value="10">
            <div class="content content-page clr">
                <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
                <div class="container blcTitre">
                    <h1>Sélectionnez le type de skimmer <br>pour votre piscine</h1>
                </div>
                <div class="container container-margelle">
                    <div class="row content-margelle" id="slideSkimmer">
                        <?php 
                            $skimmers = get_taxs('skimmer');
                            $tax = 'skimmer';
                            $liste_skimmers = get_field('liste_skimmers', $modele_id);

                            if(!empty($skimmers)):
                                foreach($skimmers as $key => $skimmer):
                                    $skimmer_ID     = $skimmer->term_id;
                                    $skimmer_name   = $skimmer->name;
                                    $numero_skimmer = 'skimmer'.$key;

                                    $labelle_skimmer = get_field('labelle_skimmer', $tax.'_'.$skimmer_ID);
                                    $visuel          = get_field('visuel_skimmer', $tax.'_'.$skimmer_ID);
                                    $explain_skimmer = get_field('explain_skimmer', $tax.'_'.$skimmer_ID);
                                    $prix            = get_field('prix_skimmer', $tax.'_'.$skimmer_ID);
                            ?>

                            <?php if(!empty($liste_skimmers)) : ?>

                                <?php if( in_array($skimmer_ID, $liste_skimmers)) : ?>

                                    <?php if(!empty($skimmer_ids)) : ?>
                                        <?php if (in_array($skimmer_ID, $skimmer_ids)): ?>
                                            <label  for="<?= $numero_skimmer; ?>" class="col obligatoire">
                                                <span class="default">De série</span>
                                                <input type="hidden" name="prix_skimmer" value="0">
                                                <input type="hidden" name="skimmer_name" value="<?= $skimmer_name; ?>">
                                                <input type="radio" name="type_skimmer" id="<?= $numero_skimmer; ?>" value="<?= $skimmer_ID; ?>" >
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="visuel">
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_skimmer; ?></span>
                                                            <span class="num-modele"><?= $skimmer_name; ?></span>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_skimmer; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </label>
                                        <?php else: ?>
                                            <label for="<?= $numero_skimmer; ?>" class="col obligatoire">
                                                <input type="hidden" name="prix_skimmer" value="<?= $prix; ?>">
                                                <input type="hidden" name="skimmer_name" value="<?= $skimmer_name; ?>">
                                                <input type="radio" name="type_skimmer" id="<?= $numero_skimmer; ?>" value="<?= $skimmer_ID; ?>" >
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="visuel">
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_skimmer; ?></span>
                                                            <span class="num-modele"><?= $skimmer_name; ?></span>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_skimmer; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </label>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <label  for="<?= $numero_skimmer; ?>" class="col obligatoire">
                                            <input type="hidden" name="prix_skimmer" value="<?= $prix; ?>">
                                            <input type="hidden" name="skimmer_name" value="<?= $skimmer_name; ?>">
                                            <input type="radio" name="type_skimmer" id="<?= $numero_skimmer; ?>" value="<?= $skimmer_ID; ?>">
                                            <div class="content-col clr">
                                                <div class="left">
                                                    <img src="<?= $visuel; ?>" alt="visuel">
                                                </div>
                                                <div class="right">
                                                    <div class="modele">
                                                        <span class="title-modele"><?= $labelle_skimmer; ?></span>
                                                        <span class="num-modele"><?= $skimmer_name; ?></span>
                                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_skimmer; ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                        </label>
                                    <?php  endif; ?>

                                <?php else: ?> 

                                    <?php if(!empty($skimmer_ids)) : ?>
                                        <?php if (in_array($skimmer_ID, $skimmer_ids)): ?>
                                            <div  for="<?= $numero_skimmer; ?>" class="col disabled">
                                                <span class="notdispo">Pas disponible sur ce modèle</span>
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="visuel">
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_skimmer; ?></span>
                                                            <span class="num-modele"><?= $skimmer_name; ?></span>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_skimmer; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </div>
                                        <?php else: ?>
                                            <div for="<?= $numero_skimmer; ?>" class="col disabled">
                                                <span class="notdispo">Pas disponible sur ce modèle</span>
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="visuel">
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_skimmer; ?></span>
                                                            <span class="num-modele"><?= $skimmer_name; ?></span>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_skimmer; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </div>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <div  for="<?= $numero_skimmer; ?>" class="col disabled">
                                            <span class="notdispo">Pas disponible sur ce modèle</span>
                                            <div class="content-col clr">
                                                <div class="left">
                                                    <img src="<?= $visuel; ?>" alt="visuel">
                                                </div>
                                                <div class="right">
                                                    <div class="modele">
                                                        <span class="title-modele"><?= $labelle_skimmer; ?></span>
                                                        <span class="num-modele"><?= $skimmer_name; ?></span>
                                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_skimmer; ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                        </div>
                                    <?php  endif; ?> 

                                <?php endif; ?>                                    

                            <?php else: ?>
                                <!-- Si aucune option obligatoire -->
                                <?php if(!empty($skimmer_ids)) : ?>
                                    <?php if (in_array($skimmer_ID, $skimmer_ids)): ?>
                                        <label  for="<?= $numero_skimmer; ?>" class="col">
                                            <span class="default">De série</span>
                                            <input type="hidden" name="prix_skimmer" value="0">
                                            <input type="hidden" name="skimmer_name" value="<?= $skimmer_name; ?>">
                                            <input type="radio" name="type_skimmer" id="<?= $numero_skimmer; ?>" value="<?= $skimmer_ID; ?>" >
                                            <div class="content-col clr">
                                                <div class="left">
                                                    <img src="<?= $visuel; ?>" alt="visuel">
                                                </div>
                                                <div class="right">
                                                    <div class="modele">
                                                        <span class="title-modele"><?= $labelle_skimmer; ?></span>
                                                        <span class="num-modele"><?= $skimmer_name; ?></span>
                                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_skimmer; ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                        </label>
                                    <?php else: ?>
                                        <label for="<?= $numero_skimmer; ?>" class="col">
                                            <input type="hidden" name="prix_skimmer" value="<?= $prix; ?>">
                                            <input type="hidden" name="skimmer_name" value="<?= $skimmer_name; ?>">
                                            <input type="radio" name="type_skimmer" id="<?= $numero_skimmer; ?>" value="<?= $skimmer_ID; ?>" >
                                            <div class="content-col clr">
                                                <div class="left">
                                                    <img src="<?= $visuel; ?>" alt="visuel">
                                                </div>
                                                <div class="right">
                                                    <div class="modele">
                                                        <span class="title-modele"><?= $labelle_skimmer; ?></span>
                                                        <span class="num-modele"><?= $skimmer_name; ?></span>
                                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_skimmer; ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                        </label>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <label  for="<?= $numero_skimmer; ?>" class="col">
                                        <input type="hidden" name="prix_skimmer" value="<?= $prix; ?>">
                                        <input type="hidden" name="skimmer_name" value="<?= $skimmer_name; ?>">
                                        <input type="radio" name="type_skimmer" id="<?= $numero_skimmer; ?>" value="<?= $skimmer_ID; ?>">
                                        <div class="content-col clr">
                                            <div class="left">
                                                <img src="<?= $visuel; ?>" alt="visuel">
                                            </div>
                                            <div class="right">
                                                <div class="modele">
                                                    <span class="title-modele"><?= $labelle_skimmer; ?></span>
                                                    <span class="num-modele"><?= $skimmer_name; ?></span>
                                                    <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_skimmer; ?></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                    </label>
                                <?php  endif; ?> 

                            <?php endif; ?>
                        <?php endforeach; wp_reset_query(); endif; ?>  

                    </div>
                </div>
            </div>

            <?php 
                // Modele
                $numero_modele = get_field('numero_modele', $modele_id);
                $croquis       = get_field('croquis', $modele_id);
                $prix_modele   = get_field('prix', $modele_id);

                // POSE
                $pose      = get_term_by( 'id', $pose_id, 'pose' );
                $prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');                

                // COULEUR
                $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
                $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

                // Margelle
                $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
                $prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

                // Eclairage
                $eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );
                // $prix_eclairage = conditionnal_price($modele_id, $eclairage_id, 'eclairage');
                $eclairage_id     = $data_step->eclairage_id;
                $eclairage_format = $data_step->eclairage_format;
                $eclairage_prix   = $data_step->eclairage_prix;
                $nb_unite         = $data_step->nb_unite;

                if ($eclairage_format == 'perso') {
                    $ecl_price = $nb_unite * $eclairage_prix;
                }else {
                    $ecl_price = $eclairage_prix;
                }

                // Local technique
                $local_technique      = get_term_by( 'id', $local_technique_id, 'local_technique' );
                $prix_local_technique = get_field( 'prix', 'local_technique_'.$local_technique_id );
                $sur_devis            = get_field('sur_devis', 'local_technique_'.$local_technique_id);

                // Distance Local technique
                $distance_type = $data_step->distance_type;
                $distance_value  = $data_step->distance_value;
                $distance_prix   = $data_step->distance_prix;

                if ($distance_type == 'perso') {
                    $d_price = real_distance_price($distance_value, $distance_prix);
                }else {
                    $d_price = $distance_prix;
                }

                $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle, $ecl_price, $prix_local_technique, $d_price));
                $total_price_formatted = format_price($total_price);
            ?>
            <aside class="blcRight" id="aside">
                <div class="contentAside">
                    <div class="shadow">
                        <div class="recap">
                            <h2>Récapitulatif de votre simulation</h2>
                            <div class="blcModele clr">
                                <div class="modele">
                                    <span class="title-modele">Modèle</span>
                                    <span class="num-modele"><?= $numero_modele; ?></span>
                                </div>
                                <div class="plan-modele">
                                    <img src="<?= $croquis; ?>" >
                                </div>
                            </div>
                            <div class="blcPrice">
                                <div class="total">prix total</div>
                                <div class="price" oldprice="<?= $total_price; ?>">
                                    <span><?= $total_price_formatted; ?></span> € HT
                                </div>
                            </div>
                            <div class="recap-step">
                                <ul>
                                    <li>Version de pose choisie : <span><?= $pose->name; ?></span> <b class="price-step"><?= sanitize_price($prix_pose); ?></b></li>
                                    <li>Couleur : <span><?= $couleur->name; ?></span> <b class="price-step"><?= sanitize_price($prix_couleur); ?></b></li>
                                    <li>Margelle : <span><?= $margelle->name; ?></span> <b class="price-step"><?= sanitize_price($prix_margelle); ?></b></li>
                                    <li>Eclairage : <span><?= $eclairage->name; ?></span> <b class="price-step"><?= sanitize_price($ecl_price); ?></b></li>
                                    <?php if($sur_devis == 'oui') : ?>
                                        <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step">Sur Devis</b></li>
                                    <?php else: ?>
                                        <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step"><?= sanitize_price($prix_local_technique); ?></b></li>
                                    <?php endif; ?>
                                    <li>Local technique : <span><?= $distance_value; ?>m de distance</span> <b class="price-step"><?= sanitize_price($d_price); ?></b></li>
                                    <li class="item_skimmer" style="display: none;">Skimmer : <span></span> <b class="price-step"></b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="boutton boutton2">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </aside>
            <?php include_once('footer-step.php'); ?>
        </form>
    </div>
</main>