<main class="page piscine">
	<?php get_template_part('pools/step/head'); ?>

	<div class="contenu">
	    <form class="blcLeft page step2" action="<?= CUR_URL; ?>" method="POST">
	    	<input type="hidden" name="cur_step" value="2">
	        <input type="hidden" name="next_step" value="3">
	        <div class="content clr">
	        	<a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
	        	<div class="container blcTitre">
	                <h1>Choisissez le modèle de piscine de vos rêves !</h1>
	            </div>
	            <div class="container-2">
	                <div class="row content-modele">
						<?php 
							$pools = get_pools();
							$i = 0;
							if ( $pools->have_posts() ) : while ( $pools->have_posts() ) : $pools->the_post();
								$model_ID = get_the_ID();
								$numero_model    = 'modele'.$i;
								$aspect          = get_field('aspect');
								$numero_modele   = get_field('numero_modele');
								$visuel_piscine  = get_field('visuel_piscine');
								$croquis         = get_field('croquis');
								$prix            = get_field('prix');
								$explain_piscine = get_field('explain_piscine');

								$numero_modele = get_field('numero_modele');
						?>
		                    <label  for="<?= $numero_model; ?>" class="col" >
		                    	<input type="hidden" name="prix_modele" value="<?= $prix; ?>" >
		                    	<input type="hidden" name="numero_modele" value="<?= $numero_modele; ?>" >
		                        <input type="radio" name="modele" id="<?= $numero_model; ?>" value="<?= $model_ID; ?>" >
		                        <div class="content-col clr">
		                            <div class="left">
		                                <img src="<?= $visuel_piscine; ?>" alt="<?php the_title(); ?>"/>
		                            </div>
		                            <div class="right">
		                                <div class="modele">
		                                    <span class="title-modele">Modèle</span>
		                                    <span class="num-modele"><?= $numero_modele; ?></span>
		                                    <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_piscine; ?></span></div>
		                                </div>
		                                <div class="plan-modele">
		                                    <img src="<?= $croquis; ?>">
		                                </div>
		                            </div>
		                        </div>
		                        <span class="btn3">Sélectionner</span>
		                    </label>
	                    <?php $i++; endwhile; wp_reset_query(); endif; ?> 

	                </div>
	            </div>
	        </div>
	        <aside class="blcRight" id="aside">
	            <div class="contentAside">
	            	<div class="shadow">
	            		<div class="recap">
	            		    <h2>Récapitulatif de votre simulation</h2>
	            		    <div class="blcModele clr">
	            		        <div class="modele">
	            		            <span class="title-modele">Modèle</span>
	            		            <span class="num-modele"></span>
	            		        </div>
	            		        <div class="plan-modele">
	            		            <img src="">
	            		        </div>
	            		    </div>
	            		    <div class="blcPrice">
	            		        <div class="total">prix total</div>
	            		        <div class="price" oldprice="0">
                                    <span>0</span> € HT
                                </div>
	            		    </div>
	            		</div>
	            		<div class="boutton boutton2">
	            		    <button class="btn-submit" type="submit">Etape suivante</button>
	            		</div>
	            		<ul id="error_form"></ul>
	            	</div>
	            </div>
	        </aside>
	        <?php include_once('footer-step.php'); ?>
	    </form>
	</div>
</main>