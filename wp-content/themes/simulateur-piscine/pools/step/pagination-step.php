<?php 
    $arr_steps = array(
        1  => "C’est parti !",
        2  => 'Modèle',
        3  => 'Pose',
        4  => 'Couleur',
        5  => 'Margelles',
        6  => 'Eclairages',
        7  => 'Local technique',
        8  => 'Distance local',
        9  => 'Skimmer',
        10 => 'Accès Maison',
        11 => 'Option Supplémentaire',
        12 => 'Traitement de l\eau',
        13 => 'Récapitulatif'
    );

    if(isset($vstep)){
        $cstep = $vstep;
    }elseif($step){
        $cstep = $step;
    }else {
        $cstep = 1;
    }

    // User id Step
    if(isset($uid)){
        $cuid = $uid;
    }elseif($current_user_id){
        $cuid = $current_user_id;
    }
 
?>

<ul class="clr">
    <?php foreach ( $arr_steps as $key => $value) :
        if ($cstep == $key): ?>
            <li class="active"><span class="number"><?= $key; ?></span><span class="notif"><?= $value; ?></span></li>
        <?php elseif( $cuid && ($key < $cstep) ) : ?>
            <li class="active"><span class="number"><a href="<?= CUR_URL.'?vstep='.$key.'&uid='.$cuid; ?>" title="<?= $value; ?>" ><?= $key; ?></a></span></li>
        <?php else: ?>
            <li><span class="number"><?= $key; ?></span></li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>