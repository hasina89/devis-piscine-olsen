<main class="pose couleur">
	<?php get_template_part('pools/step/head'); ?>

    <div class="contenu">
        <form class="blcLeft page step4" action="<?= CUR_URL; ?>" method="POST">
            <input type="hidden" name="cur_step" value="4">
            <input type="hidden" name="next_step" value="5">
            <div class="content content-page clr">
                <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
                <div class="container blcTitre">
                    <h1>Choisissez la couleur de votre piscine</h1>
                </div>
                <div class="container">
                    <div class="row content-couleur">
                        <?php 
                            $couleurs = get_taxs('couleur');
                            $tax = 'couleur';
                            $liste_couleurs = get_field('liste_couleurs', $modele_id); 
                            
                            if(!empty($couleurs)):
                                foreach($couleurs as $key => $couleur):
                                    $couleur_ID     = $couleur->term_id;
                                    $couleur_name   = $couleur->name;
                                    $numero_couleur = 'couleur'.$key;
                                    $visuel         = get_field('visuel', $tax.'_'.$couleur_ID);
                                    $code_couleur   = get_field('code_couleur', $tax.'_'.$couleur_ID);
                                    $prix           = get_field('prix', $tax.'_'.$couleur_ID);
                                    $explain_couleur = get_field('explain_couleur', $tax.'_'.$couleur_ID);

                                    $style_color = 'style="background-color:'. $code_couleur.'"';
                            ?>

                            <?php if(!empty($liste_couleurs)) : ?>

                                <?php if( in_array($couleur_ID, $liste_couleurs) ): ?>
                                    <?php if(!empty($couleur_ids)) : ?>
                                
                                        <?php if (!in_array($couleur_ID, $couleur_ids)): ?>
                                            <label for="<?= $numero_couleur; ?>" class="col obligatoire">
                                                <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_couleur; ?></span></div>
                                                <div class="content-col">
                                                    <input type="hidden" name="couleur_name" id="couleur_name" value="<?= $couleur_name; ?>" >
                                                    <input type="hidden" name="prix_couleur" value="<?= $prix; ?>">
                                                    <input type="radio" name="choix_couleur" id="<?= $numero_couleur; ?>" value="<?= $couleur_ID; ?>" >
                                                    <div class="blcImg"><span class="color" <?= $style_color; ?>></span><img src="<?= $visuel; ?>"></div>
                                                    <div class="blcCouleur"><?= $couleur_name; ?></div>
                                                    <div class="icon-selected"></div>
                                                </div>                 
                                            </label>   
                                        <?php else: ?> 
                                            <label  for="<?= $numero_couleur; ?>" class="col obligatoire">
                                                <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_couleur; ?></span></div>
                                                <span class="default">De série</span>
                                                <input type="hidden" name="prix_couleur" value="0">
                                                <input type="hidden" name="couleur_name" id="couleur_name<?= $numero_couleur; ?>" value="<?= $couleur_name; ?>" >
                                                <div class="content-col">
                                                    <input type="radio" name="choix_couleur" id="<?= $numero_couleur; ?>" value="<?= $couleur_ID; ?>">
                                                    <div class="blcImg"><span class="color" <?= $style_color; ?>></span><img src="<?= $visuel; ?>"></div>
                                                    <div class="blcCouleur"><?= $couleur_name; ?></div>
                                                    <div class="icon-selected"></div>
                                                </div>                 
                                            </label>   
                                        <?php endif; ?> 
                                    <?php else: ?>
                                        <label  for="<?= $numero_couleur; ?>" class="col obligatoire">
                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_couleur; ?></span></div>
                                            <div class="content-col">
                                                <input type="hidden" name="couleur_name" id="couleur_name<?= $numero_couleur; ?>" value="<?= $couleur_name; ?>" >
                                                <input type="hidden" name="prix_couleur" value="<?= $prix; ?>">
                                                <input type="radio" name="choix_couleur" id="<?= $numero_couleur; ?>" value="<?= $couleur_ID; ?>">
                                                <div class="blcImg"><span class="color" <?= $style_color; ?>></span><img src="<?= $visuel; ?>"></div>
                                                <div class="blcCouleur"><?= $couleur_name; ?></div>
                                                <div class="icon-selected"></div>
                                            </div>                 
                                        </label>   
                                    <?php  endif; ?>   
                                <?php else: ?> 
                                    <div  for="<?= $numero_couleur; ?>" class="col disabled">
                                        <span class="notdispo">Pas disponible sur ce modèle</span>
                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_couleur; ?></span></div>
                                        <div class="content-col">
                                            <input type="radio" name="choix_couleur" id="<?= $numero_couleur; ?>" value="<?= $couleur_ID; ?>">
                                            <div class="blcImg"><span class="color" <?= $style_color; ?>></span><img src="<?= $visuel; ?>"></div>
                                            <div class="blcCouleur"><?= $couleur_name; ?></div>
                                            <div class="icon-selected"></div>
                                        </div>                 
                                    </div>   
                                <?php endif; ?>                                    

                            <?php else: ?>
                                <!-- Si aucune option obligatoire -->
                                <?php if(!empty($couleur_ids)) : ?>
                                
                                    <?php if (!in_array($couleur_ID, $couleur_ids)): ?>
                                        <label for="<?= $numero_couleur; ?>" class="col">
                                            <div class="content-col">
                                                <input type="hidden" name="couleur_name" id="couleur_name" value="<?= $couleur_name; ?>" >
                                                <input type="hidden" name="prix_couleur" value="<?= $prix; ?>">
                                                <input type="radio" name="choix_couleur" id="<?= $numero_couleur; ?>" value="<?= $couleur_ID; ?>" >
                                                <div class="blcImg"><span class="color" <?= $style_color; ?>></span><img src="<?= $visuel; ?>"></div>
                                                <div class="blcCouleur"><?= $couleur_name; ?></div>
                                                <div class="icon-selected"></div>
                                            </div>                 
                                        </label>   
                                    <?php else: ?> 
                                        <label  for="<?= $numero_couleur; ?>" class="col">
                                            <span class="default">De série</span>
                                            <input type="hidden" name="prix_couleur" value="0">
                                            <input type="hidden" name="couleur_name" id="couleur_name<?= $numero_couleur; ?>" value="<?= $couleur_name; ?>" >
                                            <div class="content-col">
                                                <input type="radio" name="choix_couleur" id="<?= $numero_couleur; ?>" value="<?= $couleur_ID; ?>">
                                                <div class="blcImg"><span class="color" <?= $style_color; ?>></span><img src="<?= $visuel; ?>"></div>
                                                <div class="blcCouleur"><?= $couleur_name; ?></div>
                                                <div class="icon-selected"></div>
                                            </div>                 
                                        </label>   
                                    <?php endif; ?> 
                                <?php else: ?>
                                    <label  for="<?= $numero_couleur; ?>" class="col">
                                        <div class="content-col">
                                            <input type="hidden" name="couleur_name" id="couleur_name<?= $numero_couleur; ?>" value="<?= $couleur_name; ?>" >
                                            <input type="hidden" name="prix_couleur" value="<?= $prix; ?>">
                                            <input type="radio" name="choix_couleur" id="<?= $numero_couleur; ?>" value="<?= $couleur_ID; ?>">
                                            <div class="blcImg"><span class="color" <?= $style_color; ?>></span><img src="<?= $visuel; ?>"></div>
                                            <div class="blcCouleur"><?= $couleur_name; ?></div>
                                            <div class="icon-selected"></div>
                                        </div>                 
                                    </label>   
                                <?php  endif; ?> 
                                
                            <?php endif; ?>

                        <?php endforeach; wp_reset_query(); endif; ?>  
                                                  
                    </div>
                </div>
            </div>

            <?php 
                // Modele
                $numero_modele = get_field('numero_modele', $modele_id);
                $croquis       = get_field('croquis', $modele_id);
                $prix_modele   = get_field('prix', $modele_id);

                // POSE
                $pose      = get_term_by( 'id', $pose_id, 'pose' );
                
                // Prix pose
                $prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');

                $total_price = total_price(array($prix_modele, $prix_pose));
                $total_price_formatted = format_price($total_price);
            ?>
            <aside class="blcRight" id="aside">
                <div class="contentAside">
                    <div class="shadow">
                        <div class="recap">
                            <h2>Récapitulatif de votre simulation</h2>
                            <div class="blcModele clr">
                                <div class="modele">
                                    <span class="title-modele">Modèle</span>
                                    <span class="num-modele"><?= $numero_modele; ?></span>
                                </div>
                                <div class="plan-modele">
                                    <img src="<?= $croquis; ?>" >
                                </div>
                            </div>
                            <div class="blcPrice">
                                <div class="total">prix total</div>
                                <div class="price" oldprice="<?= $total_price; ?>">
                                    <span><?= $total_price_formatted; ?></span> € HT
                                </div>
                            </div>
                            <div class="recap-step">
                                <ul>
                                    <li>Version de pose choisie : <span><?= $pose->name; ?></span> <b class="price-step"><?= sanitize_price($prix_pose); ?></b></li>
                                    <li class="item_couleur" style="display: none;">Couleur : <span></span> <b class="price-step"></b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="boutton boutton2">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </aside>
            <?php include_once('footer-step.php'); ?>
        </form>
    </div>
</main>