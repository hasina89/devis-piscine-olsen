<main class="pose page">
	<?php get_template_part('pools/step/head'); ?>

    <div class="contenu">
        <form class="blcLeft step3" action="<?= CUR_URL; ?>" method="POST">
            <input type="hidden" name="cur_step" value="3">
            <input type="hidden" id="id_next_step" name="next_step" value="4">
            <div class="content content-page clr">
                <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
            	<div class="container blcTitre">
                    <h1>Choisissez le type de pose</h1>
                </div>
                <div class="container">
                    <div class="row content-modele">
                        <?php 
                            $exclure_poses      = get_field('exclure_poses', $modele_id);
                            $pose_a_exclure = array();
                            if($exclure_poses == 'oui'){
                                $pose_a_exclure      = get_field('pose_a_exclure', $modele_id);
                            }

                            $poses = get_taxs('pose');
                            $tax = 'pose';

                        if(!empty($poses)):

                            foreach($poses as $key => $pose):
                                $pose_ID               = $pose->term_id;
                                $numero_pose           = 'modele'.$key;
                                $icone_pose            = get_field('icone_pose', $tax.'_'.$pose_ID);
                                $introduction_pose     = get_field('introduction_pose', $tax.'_'.$pose_ID);
                                $couleur               = get_field('couleur_pose', $tax.'_'.$pose_ID);
                                $liste_atout           = get_field('liste_atout', $tax.'_'.$pose_ID); 
                                $explain_pose          = get_field('explain_pose', $tax.'_'.$pose_ID);
                                $installation_manuelle = get_field('installation_manuelle', $tax.'_'.$pose_ID);

                                $prix = conditionnal_tax_price($modele_id, $pose_ID, 'pose'); 
             

                                if($installation_manuelle == 'oui'){
                                    $class_pose = 'to_final_step';
                                }else {
                                    $class_pose = '';
                                }                         
                            ?>

                            <style>
                                .<?= $numero_pose; ?>.selected {border: 2px solid <?= $couleur; ?> !important;}
                                .pose .<?= $numero_pose; ?>.selected .blcImg {background: <?= $couleur; ?>;}
                                .pose .<?= $numero_pose; ?>.selected .texte {color: <?= $couleur; ?>;}
                                .pose .<?= $numero_pose; ?>.selected .blcImg .caret { border-top-color: <?= $couleur; ?> !important; }
                            </style>

                        
                            <?php if(!empty($pose_a_exclure)) : ?>

                                <?php if (in_array($pose_ID, $pose_a_exclure)): ?>
                                    <div class="col disabled" >
                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_pose; ?></span></div>
                                        <span class="notdispo">Pas disponible sur ce modèle</span>
                                        <div class="content-col">
                                            <div>
                                                <div class="blcImg">
                                                    <img src="<?= $icone_pose; ?>" >
                                                    <span class="icon-select"></span>
                                                    <span class="caret"></span>
                                                </div>
                                                <div class="texte">
                                                    <div class="titre"><?= $introduction_pose; ?></div>
                                                    <div class="contentText">
                                                        <?php if(!empty($liste_atout)): ?>
                                                            <ul>
                                                                <?php foreach($liste_atout as $atout) : ?>
                                                                    <li><?= $atout['item_atout']; ?></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                    </div>  
                                                    <div class="blcScroll">
                                                        <span>
                                                           voir plus de caractéristiques 
                                                        </span>
                                                    </div>                                          
                                                </div>
                                            </div>
                                        </div>                 
                                    </div>
                                <?php else: ?> 
                                    <?php if(!empty($pose_ids)) : ?>

                                        <?php if (in_array($pose_ID, $pose_ids)): ?>
                                            <label for="<?= $numero_pose; ?>" class="col <?= $numero_pose .' ' .$class_pose; ?>" >
                                                <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_pose; ?></span></div>
                                                <span class="default">De série</span>
                                                <div class="content-col">
                                                    <input type="hidden" name="prix_pose" value="0" >
                                                    <input type="hidden" name="name_pose" id="<?= 'name_pose_'.$numero_pose; ?>" value="<?= $pose->name; ?>" >
                                                    <input type="radio" name="type_pose" id="<?= $numero_pose; ?>" value="<?= $pose_ID; ?>">
                                                    <div>
                                                        <div class="blcImg">
                                                            <img src="<?= $icone_pose; ?>" >
                                                            <span class="icon-select"></span>
                                                            <span class="caret"></span>
                                                        </div>
                                                        <div class="texte">
                                                            <div class="titre"><?= $introduction_pose; ?></div>
                                                            <div class="contentText">
                                                                <?php if(!empty($liste_atout)): ?>
                                                                    <ul>
                                                                        <?php foreach($liste_atout as $atout) : ?>
                                                                            <li><?= $atout['item_atout']; ?></li>
                                                                        <?php endforeach; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </div>  
                                                            <div class="blcScroll">
                                                                <span>
                                                                   voir plus de caractéristiques 
                                                                </span>
                                                            </div>                                          
                                                        </div>
                                                    </div>
                                                </div>                 
                                            </label>
                                        <?php else: ?> 
                                            <label for="<?= $numero_pose; ?>" class="col <?= $numero_pose; ?>" >
                                                <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_pose; ?></span></div>
                                                <div class="content-col">
                                                    <input type="hidden" name="prix_pose" value="<?= $prix; ?>" >
                                                    <input type="hidden" name="name_pose" id="<?= 'name_pose_'.$numero_pose; ?>" value="<?= $pose->name; ?>" >
                                                    <input type="radio" name="type_pose" id="<?= $numero_pose; ?>" value="<?= $pose_ID; ?>" >
                                                    <div>
                                                        <div class="blcImg">
                                                            <img src="<?= $icone_pose; ?>" >
                                                            <span class="icon-select"></span>
                                                            <span class="caret"></span>
                                                        </div>
                                                        <div class="texte">
                                                            <div class="titre"><?= $introduction_pose; ?></div>
                                                            <div class="contentText">
                                                                <?php if(!empty($liste_atout)): ?>
                                                                    <ul>
                                                                        <?php foreach($liste_atout as $atout) : ?>
                                                                            <li><?= $atout['item_atout']; ?></li>
                                                                        <?php endforeach; ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            </div>  
                                                            <div class="blcScroll">
                                                                <span>
                                                                   voir plus de caractéristiques 
                                                                </span>
                                                            </div>                                           
                                                        </div>
                                                    </div>
                                                </div>                 
                                            </label>
                                        <?php endif; ?>

                                    <?php endif; ?>
                                <?php endif; ?>

                            <?php elseif(!empty($pose_ids)) : ?>

                                <?php if (in_array($pose_ID, $pose_ids)): ?>
                                    <label for="<?= $numero_pose; ?>" class="col <?= $numero_pose.' '. $class_pose; ?>" >
                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_pose; ?></span></div>
                                        <span class="default">De série</span>
                                        <div class="content-col">
                                            <input type="hidden" name="prix_pose" value="0" >
                                            <input type="hidden" name="name_pose" id="<?= 'name_pose_'.$numero_pose; ?>" value="<?= $pose->name; ?>" >
                                            <input type="radio" name="type_pose" id="<?= $numero_pose; ?>" value="<?= $pose_ID; ?>">
                                            <div>
                                                <div class="blcImg">
                                                    <img src="<?= $icone_pose; ?>" >
                                                    <span class="icon-select"></span>
                                                    <span class="caret"></span>
                                                </div>
                                                <div class="texte">
                                                    <div class="titre"><?= $introduction_pose; ?></div>
                                                    <div class="contentText">
                                                        <?php if(!empty($liste_atout)): ?>
                                                            <ul>
                                                                <?php foreach($liste_atout as $atout) : ?>
                                                                    <li><?= $atout['item_atout']; ?></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                    </div>  
                                                    <div class="blcScroll">
                                                        <span>
                                                           voir plus de caractéristiques 
                                                        </span>
                                                    </div>                                          
                                                </div>
                                            </div>
                                        </div>                 
                                    </label>
                                <?php else: ?> 
                                    <label for="<?= $numero_pose; ?>" class="col <?= $numero_pose; ?>" >
                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_pose; ?></span></div>
                                        <div class="content-col">
                                            <input type="hidden" name="prix_pose" value="<?= $prix; ?>" >
                                            <input type="hidden" name="name_pose" id="<?= 'name_pose_'.$numero_pose; ?>" value="<?= $pose->name; ?>" >
                                            <input type="radio" name="type_pose" id="<?= $numero_pose; ?>" value="<?= $pose_ID; ?>" >
                                            <div>
                                                <div class="blcImg">
                                                    <img src="<?= $icone_pose; ?>" >
                                                    <span class="icon-select"></span>
                                                    <span class="caret"></span>
                                                </div>
                                                <div class="texte">
                                                    <div class="titre"><?= $introduction_pose; ?></div>
                                                    <div class="contentText">
                                                        <?php if(!empty($liste_atout)): ?>
                                                            <ul>
                                                                <?php foreach($liste_atout as $atout) : ?>
                                                                    <li><?= $atout['item_atout']; ?></li>
                                                                <?php endforeach; ?>
                                                            </ul>
                                                        <?php endif; ?>
                                                    </div>  
                                                    <div class="blcScroll">
                                                        <span>
                                                           voir plus de caractéristiques 
                                                        </span>
                                                    </div>                                           
                                                </div>
                                            </div>
                                        </div>                 
                                    </label>
                                <?php endif; ?>

                            <?php else: ?>
                                <label  for="<?= $numero_pose; ?>" class="col  <?= $numero_pose; ?>" >
                                    <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_pose; ?></span></div>
                                    <div class="content-col">
                                        <input type="hidden" name="prix_pose" value="<?= $prix; ?>" >
                                        <input type="hidden" name="name_pose" id="<?= 'name_pose_'.$numero_pose; ?>" value="<?= $pose->name; ?>" >
                                        <input type="radio" name="type_pose" id="<?= $numero_pose; ?>" value="<?= $pose_ID; ?>" >
                                        <div>
                                            <div class="blcImg">
                                                <img src="<?= $icone_pose; ?>" >
                                                <span class="icon-select"></span>
                                                <span class="caret"></span>
                                            </div>
                                            <div class="texte">
                                                <div class="titre"><?= $introduction_pose; ?></div>
                                                <div class="contentText">
                                                    <?php if(!empty($liste_atout)): ?>
                                                        <ul>
                                                            <?php foreach($liste_atout as $atout) : ?>
                                                                <li><?= $atout['item_atout']; ?></li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    <?php endif; ?>
                                                </div>    
                                                <div class="blcScroll">
                                                    <span>
                                                       voir plus de caractéristiques 
                                                    </span>
                                                </div>                                         
                                            </div>
                                        </div>
                                    </div>                 
                                </label>
                            <?php  endif; ?> 
                        <?php endforeach; wp_reset_query(); endif; ?> 
                                             
                    </div>
                </div>
            </div>

            <?php 
                $numero_modele = get_field('numero_modele', $modele_id);
                $croquis       = get_field('croquis', $modele_id);
                $prix          = get_field('prix', $modele_id);
            ?>
            <aside class="blcRight" id="aside">
                <div class="contentAside">
                    <div class="shadow">
                        <div class="recap">
                            <h2>Récapitulatif de votre simulation</h2>
                            <div class="blcModele clr">
                                <div class="modele">
                                    <span class="title-modele">Modèle</span>
                                    <span class="num-modele"><?= $numero_modele; ?></span>
                                </div>
                                <div class="plan-modele">
                                    <img src="<?= $croquis; ?>" alt="Croquis">
                                </div>
                            </div>
                            <div class="blcPrice">
                                <div class="total">prix total</div>
                                <div class="price" oldprice="<?= $prix; ?>">
                                    <span><?= format_price($prix); ?></span> € HT
                                </div>
                            </div>
                            <div class="recap-step">
                                <ul>
                                    <li class="item_pose" style="display: none;"><span></span> <b class="price-step"></b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="boutton boutton2">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </aside>

            <?php include_once('footer-step.php'); ?>
        </form>
    </div>
</main>

<!-- <script type="text/javascript">
    changeValRecapClick('label', 'input[name=name_pose]', 'input[name=prix_pose]', '.item_pose');
    changeValPrice('label', 'input[name=prix_pose]');
</script> -->
<?php //wp_enqueue_script( 'step3', JS_URL . 'step3.js', array(), true, false, true ); ?>

<script type="text/javascript">
    /*jQuery(document).on("click","#modele0", function () {
        jQuery("#id_next_step").val(13);
    });*/
    jQuery(document).on("click", 'label.col', function (e) {
        var vis = $(this);
        if(vis.hasClass('to_final_step')){
            jQuery("#id_next_step").val(13);
        }else {
            jQuery("#id_next_step").val(4);
        }
    });
</script>