<main>
	<?php get_template_part('pools/step/head'); ?>

    <div class="contenu">
        <form class="blcLeft page local local-2 step8" action="<?= CUR_URL; ?>" method="POST">
            <input type="hidden" name="cur_step" value="8">
            <input type="hidden" name="next_step" value="9">
            <div class="content content-page clr">
                <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
                <div class="container blcTitre">
                    <h1>Votre local technique va se trouver à :</h1>
                </div>
                <div class="container container-local">
                    <div class="row content-local">  
                        <?php 
                            $distances = get_taxs('distance');
                            $tax = 'distance';

                            if(!empty($distances)):
                                foreach($distances as $key => $distance):
                                    $distance_ID     = $distance->term_id;
                                    $distance_name   = $distance->name;
                                    $numero_distance = 'distance'.$key;

                                    $titre_distance   = get_field('titre_distance', $tax.'_'.$distance_ID);
                                    $visuel           = get_field('visuel_distance', $tax.'_'.$distance_ID);
                                    $atout_distance   = get_field('atout_distance', $tax.'_'.$distance_ID);
                                    $prix             = get_field('prix', $tax.'_'.$distance_ID);
                                    $nombre_distance  = get_field('nombre_distance', $tax.'_'.$distance_ID);
                                    $perso            = get_field('personnalisable_distance', $tax.'_'.$distance_ID);
                                    $prix_par_m       = get_field('prix_par_m', $tax.'_'.$distance_ID);
                                    $explain_distance = get_field('explain_distance', $tax.'_'.$distance_ID);
                                    $inclus_pose      = get_field('inclus_pose', $tax.'_'.$distance_ID);
                            ?>
                            <?php if($perso == 'non'): ?>

                                <label  for="<?= $numero_distance; ?>" class="col">
                                    <?php if($inclus_pose == 'oui'): ?>
                                        <span class="default">Inclus dans la pose</span>
                                    <?php endif; ?>
                                    <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_distance; ?></span></div>
                                    <input type="hidden" name="distance_id" id="id_<?= $numero_distance; ?>" value="<?= $distance_ID; ?>">
                                    <input type="hidden" name="distance_name" value="<?= $distance_name; ?>">
                                    <input type="hidden" name="distance_prix" value="<?= $prix; ?>">
                                    <input type="radio" name="distance_locale" id="<?= $numero_distance; ?>" value="simple">
                                    <input type="hidden" name="distance_value" class="distance_value" value="<?= $nombre_distance; ?>" >
                                    <div class="blcImg">
                                        <img src="<?= $visuel; ?>" alt="Image">
                                    </div>
                                    <span class="icon-select"></span>
                                    <div class="blcText">
                                        <div class="titre"><?= $titre_distance; ?></div>
                                        <div class="texte"><?= $atout_distance; ?></div>
                                    </div>                             
                                </label>

                            <?php else: ?>

                                <label  for="<?= $numero_distance; ?>" class="col">
                                    <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_distance; ?></span></div>
                                    <input type="hidden" name="distance_id" id="id_<?= $numero_distance; ?>" value="<?= $distance_ID; ?>">
                                    <input type="hidden" name="distance_name" value="<?= $distance_name; ?>">
                                    <input type="hidden" name="prix_par_m" value="<?= $prix_par_m; ?>">
                                    <input type="radio" name="distance_locale" id="<?= $numero_distance; ?>" value="perso">
                                    <div class="blcImg">
                                        <img src="<?= $visuel; ?>" alt="Image">
                                    </div>
                                    <span class="icon-select"></span>
                                    <div class="blcText">
                                        <div class="titre"><?= $titre_distance; ?></div>
                                        <div class="texte clr">
                                            <div class="numbers-row clr">
                                                <span class="dec button">-</span>
                                                <input type="text" name="nb_distance" id="qtt" value="6" class="qtt">      
                                                <span class="inc button">+</span>
                                            </div>
                                            <div class="textDistance"><?= $atout_distance; ?></div>
                                        </div>
                                    </div>                             
                                </label>
                            <?php endif; ?>
                        <?php endforeach; wp_reset_query(); endif; ?>


                    </div>
                </div>
            </div>

            <?php 
                // Modele
                $numero_modele = get_field('numero_modele', $modele_id);
                $croquis       = get_field('croquis', $modele_id);
                $prix_modele   = get_field('prix', $modele_id);

                // POSE
                $pose      = get_term_by( 'id', $pose_id, 'pose' );
                $prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');             

                // COULEUR
                $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
                $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

                // Margelle
                $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
                $prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

                // Eclairage
                $eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );

                if ($eclairage_format == 'perso') {
                    $ecl_price = $nb_unite * $eclairage_prix;
                }else {
                    $ecl_price = $eclairage_prix;
                }

                // $prix_eclairage = conditionnal_price($modele_id, $eclairage_id, 'eclairage'); 

                // Local technique
                $local_technique      = get_term_by( 'id', $local_technique_id, 'local_technique' );
                $prix_local_technique = get_field( 'prix', 'local_technique_'.$local_technique_id );
                $sur_devis            = get_field('sur_devis', 'local_technique_'.$local_technique_id);


                $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle, $ecl_price, $prix_local_technique));
                $total_price_formatted = format_price($total_price);
            ?>
            <aside class="blcRight" id="aside">
                <div class="contentAside">
                    <div class="shadow">
                        <div class="recap">
                            <h2>Récapitulatif de votre simulation</h2>
                            <div class="blcModele clr">
                                <div class="modele">
                                    <span class="title-modele">Modèle</span>
                                    <span class="num-modele"><?= $numero_modele; ?></span>
                                </div>
                                <div class="plan-modele">
                                    <img src="<?= $croquis; ?>" >
                                </div>
                            </div>
                            <div class="blcPrice">
                                <div class="total">prix total</div>
                                <div class="price" oldprice="<?= $total_price; ?>">
                                    <span><?= $total_price_formatted; ?></span> € HT
                                </div>
                            </div>
                            <div class="recap-step">
                                <ul>
                                    <li>Version de pose choisie : <span><?= $pose->name; ?></span> <b class="price-step"><?= sanitize_price($prix_pose); ?></b></li>
                                    <li>Couleur : <span><?= $couleur->name; ?></span> <b class="price-step"><?= sanitize_price($prix_couleur); ?></b></li>
                                    <li>Margelle : <span><?= $margelle->name; ?></span> <b class="price-step"><?= sanitize_price($prix_margelle); ?></b></li>
                                    <li>Eclairage : <span><?= $eclairage->name; ?></span> <b class="price-step"><?= sanitize_price($ecl_price); ?></b></li>
                                    <?php if($sur_devis == 'oui') : ?>
                                        <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step">Sur Devis</b></li>
                                    <?php else: ?>
                                        <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step"><?= sanitize_price($prix_local_technique); ?></b></li>
                                    <?php endif; ?>
                                    <li class="item_distance_local" style="display: none;">Local technique : <span></span> <b class="price-step"></b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="boutton boutton2">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </aside>
            <?php include_once('footer-step.php'); ?>
        </form>
    </div>
</main>