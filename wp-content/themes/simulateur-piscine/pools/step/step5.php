<main class="piscine margelle">
	<?php get_template_part('pools/step/head'); ?>

    <div class="contenu">
        <form class="blcLeft page step5" action="<?= CUR_URL; ?>" method="POST">
            <input type="hidden" name="cur_step" value="5">
            <input type="hidden" name="next_step" value="6">
            <div class="content content-page clr">
                <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
                <div class="container blcTitre">
                    <h1>Choisissez la margelle idéale pour votre piscine</h1>
                    <div class="titre">La margelle est une option</div>
                </div>
                <div class="container container-margelle">
                    <div class="row content-margelle" id="slideMargelle">
                        <?php 
                            $margelles = get_taxs('margelle');
                            $tax = 'margelle';

                            if(!empty($margelles)):
                                foreach($margelles as $key => $margelle):
                                    $margelle_ID     = $margelle->term_id;
                                    $margelle_name   = $margelle->name;
                                    $numero_margelle = 'margelle'.$key;
                                    $visuel           = get_field('visuel', $tax.'_'.$margelle_ID);
                                    $labelle_margelle = get_field('labelle_margelle', $tax.'_'.$margelle_ID);
                                    $explain_margelle = get_field('explain_margelle', $tax.'_'.$margelle_ID);
                                    $prix             = get_field('prix', $tax.'_'.$margelle_ID);
                            ?>

                            <?php if( !$labelle_margelle): ?>
                                <label  for="<?= $numero_margelle; ?>" class="col vide">
                                    <input type="hidden" name="prix_margelle" value="<?= $prix; ?>">
                                    <input type="hidden" name="choix_margelle_name" id="margelle_name<?= $numero_margelle; ?>" value="<?= $margelle_name; ?>" >
                                    <input type="radio" name="choix_margelle" id="<?= $numero_margelle; ?>" value="<?= $margelle_ID; ?>">
                                    <div class="content-col clr">
                                       <div class="title-modele"><?= $margelle_name; ?></div>
                                       <div class="icon-x"><img src="<?= $visuel; ?>"></div>
                                    </div>
                                    <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                </label>
                            <?php else: ?>
                                <label  for="<?= $numero_margelle; ?>" class="col">
                                    <input type="hidden" name="prix_margelle" value="<?= $prix; ?>">
                                    <input type="hidden" name="choix_margelle_name" id="margelle_name<?= $numero_margelle; ?>" value="<?= $margelle_name; ?>" >
                                    <input type="radio" name="choix_margelle" id="<?= $numero_margelle; ?>" value="<?= $margelle_ID; ?>" >
                                    <div class="content-col clr">
                                        <div class="left">
                                            <img src="<?= $visuel; ?>"/>
                                        </div>
                                        <div class="right">
                                            <div class="modele">
                                                <span class="title-modele"><?= $labelle_margelle; ?></span>
                                                <span class="num-modele"><?= $margelle_name; ?></span>
                                                <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_margelle; ?></span></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                </label>
                            <?php endif; ?>  
                        <?php endforeach; wp_reset_query(); endif; ?> 

                    </div>
                </div>
            </div>

            <?php 
                // Modele
                $numero_modele = get_field('numero_modele', $modele_id);
                $croquis       = get_field('croquis', $modele_id);
                $prix_modele   = get_field('prix', $modele_id);

                // POSE
                $pose      = get_term_by( 'id', $pose_id, 'pose' );
                // Prix pose
                $prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');               

                // COULEUR
                $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
                $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur');  

                $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur));
                $total_price_formatted = format_price($total_price);
            ?>
            <aside class="blcRight" id="aside">
                <div class="contentAside">
                    <div class="shadow">
                        <div class="recap">
                            <h2>Récapitulatif de votre simulation</h2>
                            <div class="blcModele clr">
                                <div class="modele">
                                    <span class="title-modele">Modèle</span>
                                    <span class="num-modele"><?= $numero_modele; ?></span>
                                </div>
                                <div class="plan-modele">
                                    <img src="<?= $croquis; ?>" >
                                </div>
                            </div>
                            <div class="blcPrice">
                                <div class="total">prix total</div>
                                <div class="price" oldprice="<?= $total_price; ?>">
                                    <span><?= $total_price_formatted; ?></span> € HT
                                </div>
                            </div>
                            <div class="recap-step">
                                <ul>
                                    <li>Version de pose choisie : <span><?= $pose->name; ?></span> <b class="price-step"><?= sanitize_price($prix_pose); ?></b></li>
                                    <li>Couleur : <span><?= $couleur->name; ?></span> <b class="price-step"><?= sanitize_price($prix_couleur); ?></b></li>
                                    <li class="item_margelle" style="display: none;">Margelle : <span></span> <b class="price-step"></b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="boutton boutton2">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </aside>
            <?php include_once('footer-step.php'); ?>
        </form>
    </div>
</main>