<main>
	<?php get_template_part('pools/step/head'); ?>

    <div class="contenu">
            <form class="blcLeft page local local-2 step11" action="<?= CUR_URL; ?>" method="POST">
                <input type="hidden" name="cur_step" value="10">
                <input type="hidden" name="next_step" value="11">
                <div class="content content-page clr">
                    <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
                    <div class="container blcTitre">
                        <h1>Déterminez l'accès à votre maison</h1>
                    </div>
                    <div class="container container-local">
                        <div class="row content-local">

                            <?php 
                                $acces_maisons = get_taxs('acces_maison');
                                $tax = 'acces_maison';

                                if(!empty($acces_maisons)):
                                    foreach($acces_maisons as $key => $acces_maison):
                                        $acces_maison_ID     = $acces_maison->term_id;
                                        $acces_maison_name   = $acces_maison->name;
                                        $numero_acces_maison = 'acces_maison'.$key;

                                        $titre_acces           = get_field('titre_acces', $tax.'_'.$acces_maison_ID);
                                        $visuel                = get_field('visuel_acces', $tax.'_'.$acces_maison_ID);
                                        $atout_acces           = get_field('atout_acces', $tax.'_'.$acces_maison_ID);
                                        $nombre_distance_acces = get_field('nombre_distance_acces', $tax.'_'.$acces_maison_ID);
                                        $prix                  = get_field('prix', $tax.'_'.$acces_maison_ID);
                                        $explain_access = get_field('explain_access', $tax.'_'.$acces_maison_ID);
                            ?>

                                <label  for="<?= $numero_acces_maison; ?>" class="col">
                                    <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_access; ?></span></div>
                                    <!-- <span class="default">De série</span> -->
                                    <input type="hidden" name="prix_acces_maison" value="<?= $prix; ?>" >
                                    <input type="hidden" name="acces_maison_name" value="<?= $acces_maison_name; ?>">
                                    <input type="radio" name="type_acces_maison" id="<?= $numero_acces_maison; ?>" value="<?= $acces_maison_ID; ?>" required >
                                    <div class="blcImg">
                                        <img src="<?= $visuel; ?>" alt="visuel">
                                    </div>
                                    <span class="icon-select"></span>
                                    <div class="blcText">
                                        <div class="titre"><?= $titre_acces; ?></div>
                                        <div class="texte"><?= $atout_acces; ?></div>
                                    </div>                             
                                </label>
                            
                            <?php endforeach; wp_reset_query(); endif; ?>  

                        </div>
                    </div>
                </div>

                <?php 
                    // Modele
                    $numero_modele = get_field('numero_modele', $modele_id);
                    $croquis       = get_field('croquis', $modele_id);
                    $prix_modele   = get_field('prix', $modele_id);

                    // POSE
                    $pose      = get_term_by( 'id', $pose_id, 'pose' );
                    $prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');                

                    // COULEUR
                    $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
                    $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

                    // Margelle
                    $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
                    $prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

                    // Eclairage
                    $eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );

                    $eclairage_id     = $data_step->eclairage_id;
                    $eclairage_format = $data_step->eclairage_format;
                    $eclairage_prix   = $data_step->eclairage_prix;
                    $nb_unite         = $data_step->nb_unite;

                    if ($eclairage_format == 'perso') {
                        $ecl_price = $nb_unite * $eclairage_prix;
                    }else {
                        $ecl_price = $eclairage_prix;
                    }
                    // $prix_eclairage = conditionnal_price($modele_id, $eclairage_id, 'eclairage');

                    // Local technique
                    $local_technique      = get_term_by( 'id', $local_technique_id, 'local_technique' );
                    $prix_local_technique = get_field( 'prix', 'local_technique_'.$local_technique_id );
                    $sur_devis            = get_field('sur_devis', 'local_technique_'.$local_technique_id);

                    // Distance Local technique
                    $distance_type = $data_step->distance_type;
                    $distance_value  = $data_step->distance_value;
                    $distance_prix   = $data_step->distance_prix;

                    if ($distance_type == 'perso') {
                        $d_price = real_distance_price($distance_value, $distance_prix);
                    }else {
                        $d_price = $distance_prix;
                    }

                    // Skimmer
                    $skimmer      = get_term_by( 'id', $skimmer_id, 'skimmer' );
                    $prix_skimmer = conditionnal_price($modele_id, $skimmer_id, 'skimmer', 'prix_skimmer');

                    $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle, $ecl_price, $prix_local_technique, $d_price, $prix_skimmer));
                    $total_price_formatted = format_price($total_price);
                ?>
                <aside class="blcRight" id="aside">
                    <div class="contentAside">
                        <div class="shadow">
                            <div class="recap">
                                <h2>Récapitulatif de votre simulation</h2>
                                <div class="blcModele clr">
                                    <div class="modele">
                                        <span class="title-modele">Modèle</span>
                                        <span class="num-modele"><?= $numero_modele; ?></span>
                                    </div>
                                    <div class="plan-modele">
                                        <img src="<?= $croquis; ?>" >
                                    </div>
                                </div>
                                <div class="blcPrice">
                                    <div class="total">prix total</div>
                                    <div class="price" oldprice="<?= $total_price; ?>">
                                        <span><?= $total_price_formatted; ?></span> € HT
                                    </div>
                                </div>
                                <div class="recap-step">
                                    <ul>
                                        <li>Version de pose choisie : <span><?= $pose->name; ?></span> <b class="price-step"><?= sanitize_price($prix_pose); ?></b></li>
                                        <li>Couleur : <span><?= $couleur->name; ?></span> <b class="price-step"><?= sanitize_price($prix_couleur); ?></b></li>
                                        <li>Margelle : <span><?= $margelle->name; ?></span> <b class="price-step"><?= sanitize_price($prix_margelle); ?></b></li>
                                        <li>Eclairage : <span><?= $eclairage->name; ?></span> <b class="price-step"><?= sanitize_price($ecl_price); ?></b></li>
                                        <?php if($sur_devis == 'oui') : ?>
                                            <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step">Sur Devis</b></li>
                                        <?php else: ?>
                                            <li>Local technique : <span><?= $local_technique->name; ?></span> <b class="price-step"><?= sanitize_price($prix_local_technique); ?></b></li>
                                        <?php endif; ?>
                                        <li>Local technique : <span><?= $distance_value; ?>m de distance</span> <b class="price-step"><?= sanitize_price($d_price); ?></b></li>
                                        <li>Skimmer : <span><?= $skimmer->name; ?></span> <b class="price-step"><?= sanitize_price($prix_skimmer); ?></b></li>
                                        <li class="item_acces" style="display: none;">Accès Maison : <span></span> <b class="price-step"></b></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="boutton boutton2">
                                <button class="btn-submit" type="submit">Etape suivante</button>
                            </div>
                            <ul id="error_form"></ul>
                        </div>
                    </div>
                </aside>
                <?php include_once('footer-step.php'); ?>
            </form>
        </div>
</main>