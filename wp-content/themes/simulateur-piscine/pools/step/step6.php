<main>
	<?php get_template_part('pools/step/head'); ?>

    <div class="contenu">
        <form class="blcLeft page piscine margelle eclairage step6" action="<?= CUR_URL; ?>" method="POST">
            <input type="hidden" name="cur_step" value="6">
            <input type="hidden" name="next_step" value="7">
            <div class="content content-page clr">
                <a href="aside" class="scrollTo" title="Vers le haut">Récapitulatif</a>
                <div class="container blcTitre">
                    <h1>Choisissez l’éclairage qui sublimera votre piscine</h1>
                </div>
                <div class="container container-margelle">
                    <div class="row content-margelle" id="slideMargelle">
                        <?php 
                            $eclairages = get_taxs('eclairage');
                            $tax = 'eclairage';
                            $liste_eclairages = get_field('liste_eclairages', $modele_id); 

                            if(!empty($eclairages)):
                                foreach($eclairages as $key => $eclairage):
                                    $eclairage_ID     = $eclairage->term_id;
                                    $eclairage_name   = $eclairage->name;
                                    $numero_eclairage = 'eclairage'.$key;

                                    $visuel            = get_field('visuel', $tax.'_'.$eclairage_ID);
                                    $labelle_eclairage = get_field('labelle_eclairage', $tax.'_'.$eclairage_ID);
                                    $explain_eclairage = get_field('explain_eclairage', $tax.'_'.$eclairage_ID);
                                    $prix              = get_field('prix', $tax.'_'.$eclairage_ID);
                                    $unite_eclairage   = get_field('unite_eclairage', $tax.'_'.$eclairage_ID);

                                    $perso           = get_field('personnalisable_eclairage', $tax.'_'.$eclairage_ID);
                                    $prix_par_unite      = get_field('prix_par_unite', $tax.'_'.$eclairage_ID);
                            ?>

                            <?php if(!empty($liste_eclairages)) : ?>

                                <?php if(in_array($eclairage_ID, $liste_eclairages)): ?>
                                    <?php if (in_array($eclairage_ID, $eclairage_ids)): ?>
                                        <label  for="<?= $numero_eclairage; ?>" class="col obligatoire">
                                            <span class="default">De série</span>
                                            <input type="radio" name="eclairage_id" id="<?= $numero_eclairage; ?>" value="<?= $eclairage_ID; ?>" >
                                            <input type="hidden" name="prix_eclairage<?= $eclairage_ID; ?>" class="prix_eclairage" value="0">
                                            <input type="hidden" name="eclairage_format<?= $eclairage_ID; ?>" class="eclairage_format" value="simple">
                                            <input type="hidden" name="name_eclairage<?= $eclairage_ID; ?>" class="name_eclairage" value="<?= $eclairage_name; ?>">
                                            <div class="content-col clr">
                                                <div class="left">
                                                    <img src="<?= $visuel; ?>" alt="Visuel" />
                                                </div>
                                                <div class="right">
                                                    <div class="modele">
                                                        <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                        <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                        <span class="nombre-ecl"><?= $unite_eclairage; ?></span>
                                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                        </label>
                                    <?php else: ?> 
                                        <?php if($perso == 'oui'): ?>
                                            <label  for="<?= $numero_eclairage; ?>" class="col obligatoire">
                                                <input type="radio" name="eclairage_id" id="<?= $numero_eclairage; ?>" value="<?= $eclairage_ID; ?>">
                                                <input type="hidden" name="name_eclairage<?= $eclairage_ID; ?>" class="name_eclairage" value="<?= $eclairage_name; ?>">
                                                <input type="hidden" name="eclairage_format<?= $eclairage_ID; ?>" class="eclairage_format" value="perso">
                                                <input type="hidden" name="prix_par_unite<?= $eclairage_ID; ?>" class="prix_par_unite" value="<?= $prix_par_unite; ?>">
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="Visuel" />
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                            <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                            <div class="texte clr">
                                                                <div class="numbers-row clr">
                                                                    <span class="dec button">-</span>
                                                                    <input type="text" name="nb_unite<?= $eclairage_ID; ?>" class="nb_unite" id="qtt" value="2" class="qtt">      
                                                                    <span class="inc button">+</span>
                                                                </div>
                                                                <div class="textDistance">
                                                                    Unités
                                                                </div>
                                                            </div>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </label> 
                                        <?php else: ?> 
                                            <label  for="<?= $numero_eclairage; ?>" class="col obligatoire">
                                                <input type="radio" name="eclairage_id" id="<?= $numero_eclairage; ?>" value="<?= $eclairage_ID; ?>" >
                                                <input type="hidden" name="prix_eclairage<?= $eclairage_ID; ?>" class="prix_eclairage" value="<?= $prix; ?>">
                                                <input type="hidden" name="eclairage_format<?= $eclairage_ID; ?>" class="eclairage_format" value="simple">
                                                <input type="hidden" name="name_eclairage<?= $eclairage_ID; ?>" class="name_eclairage" value="<?= $eclairage_name; ?>">
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="Visuel" />
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                            <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                            <span class="nombre-ecl"><?= $unite_eclairage; ?></span>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </label>
                                        <?php  endif; ?> 
                                    <?php  endif; ?>

                                <?php else: ?>
                                    <!-- Si aucun modèl rattaché -->
                                    <?php if (in_array($eclairage_ID, $eclairage_ids)): ?>
                                        <div  for="<?= $numero_eclairage; ?>" class="col disabled">
                                            <span class="notdispo">Pas disponible sur ce modèle</span>
                                            <div class="content-col clr">
                                                <div class="left">
                                                    <img src="<?= $visuel; ?>" alt="Visuel" />
                                                </div>
                                                <div class="right">
                                                    <div class="modele">
                                                        <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                        <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                        <span class="nombre-ecl"><?= $unite_eclairage; ?></span>
                                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                        </div>
                                    <?php else: ?>
                                        <?php if($perso == 'oui'): ?>
                                            <div  for="<?= $numero_eclairage; ?>" class="col disabled">
                                                <span class="notdispo">Pas disponible sur ce modèle</span>
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="Visuel" />
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                            <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                            <div class="texte clr">
                                                                <div class="numbers-row clr">
                                                                    <span class="dec button">-</span>
                                                                    <input type="text" name="nb_unite<?= $eclairage_ID; ?>" class="nb_unite" id="qtt" value="2" class="qtt">      
                                                                    <span class="inc button">+</span>
                                                                </div>
                                                                <div class="textDistance">
                                                                    Unités
                                                                </div>
                                                            </div>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </div> 
                                        <?php else: ?> 
                                            <div  for="<?= $numero_eclairage; ?>" class="col disabled">
                                                <span class="notdispo">Pas disponible sur ce modèle</span>
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="Visuel" />
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                            <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                            <span class="nombre-ecl"><?= $unite_eclairage; ?></span>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </div>
                                        <?php  endif; ?> 
                                    <?php  endif; ?> 

                                <?php endif; ?>                            

                            <?php else: ?>
                                <!-- Si liste obligatoire est vide -->
                                <?php if(!empty($eclairage_ids)) : ?>
                                    <?php if (in_array($eclairage_ID, $eclairage_ids)): ?>
                                        <label  for="<?= $numero_eclairage; ?>" class="col">
                                            <span class="default">De série</span>
                                            <input type="radio" name="eclairage_id" id="<?= $numero_eclairage; ?>" value="<?= $eclairage_ID; ?>" >
                                            <input type="hidden" name="prix_eclairage<?= $eclairage_ID; ?>" class="prix_eclairage" value="0">
                                            <input type="hidden" name="eclairage_format<?= $eclairage_ID; ?>" class="eclairage_format" value="simple">
                                            <input type="hidden" name="name_eclairage<?= $eclairage_ID; ?>" class="name_eclairage" value="<?= $eclairage_name; ?>">
                                            <div class="content-col clr">
                                                <div class="left">
                                                    <img src="<?= $visuel; ?>" alt="Visuel" />
                                                </div>
                                                <div class="right">
                                                    <div class="modele">
                                                        <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                        <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                        <span class="nombre-ecl"><?= $unite_eclairage; ?></span>
                                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                        </label>
                                    <?php else: ?> 
                                        <?php if($perso == 'oui'): ?>
                                            <label  for="<?= $numero_eclairage; ?>" class="col">
                                                <input type="radio" name="eclairage_id" id="<?= $numero_eclairage; ?>" value="<?= $eclairage_ID; ?>">
                                                <input type="hidden" name="name_eclairage<?= $eclairage_ID; ?>" class="name_eclairage" value="<?= $eclairage_name; ?>">
                                                <input type="hidden" name="eclairage_format<?= $eclairage_ID; ?>" class="eclairage_format" value="perso">
                                                <input type="hidden" name="prix_par_unite<?= $eclairage_ID; ?>" class="prix_par_unite" value="<?= $prix_par_unite; ?>">
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="Visuel" />
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                            <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                            <div class="texte clr">
                                                                <div class="numbers-row clr">
                                                                    <span class="dec button">-</span>
                                                                    <input type="text" name="nb_unite<?= $eclairage_ID; ?>" class="nb_unite" id="qtt" value="2" class="qtt">      
                                                                    <span class="inc button">+</span>
                                                                </div>
                                                                <div class="textDistance">
                                                                    Unités
                                                                </div>
                                                            </div>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </label> 
                                        <?php else: ?> 
                                            <label  for="<?= $numero_eclairage; ?>" class="col">
                                                <input type="radio" name="eclairage_id" id="<?= $numero_eclairage; ?>" value="<?= $eclairage_ID; ?>" >
                                                <input type="hidden" name="prix_eclairage<?= $eclairage_ID; ?>" class="prix_eclairage" value="<?= $prix; ?>">
                                                <input type="hidden" name="eclairage_format<?= $eclairage_ID; ?>" class="eclairage_format" value="simple">
                                                <input type="hidden" name="name_eclairage<?= $eclairage_ID; ?>" class="name_eclairage" value="<?= $eclairage_name; ?>">
                                                <div class="content-col clr">
                                                    <div class="left">
                                                        <img src="<?= $visuel; ?>" alt="Visuel" />
                                                    </div>
                                                    <div class="right">
                                                        <div class="modele">
                                                            <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                            <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                            <span class="nombre-ecl"><?= $unite_eclairage; ?></span>
                                                            <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                            </label>
                                        <?php  endif; ?> 
                                    <?php endif; ?>

                                <?php else: ?>
                                    <?php if($perso == 'oui'): ?>
                                        <label  for="<?= $numero_eclairage; ?>" class="col">
                                            <input type="radio" name="eclairage_id" id="<?= $numero_eclairage; ?>" value="<?= $eclairage_ID; ?>">
                                            <input type="hidden" name="name_eclairage<?= $eclairage_ID; ?>" class="name_eclairage" value="<?= $eclairage_name; ?>">
                                            <input type="hidden" name="eclairage_format<?= $eclairage_ID; ?>" class="eclairage_format" value="perso">
                                            <input type="hidden" name="prix_par_unite<?= $eclairage_ID; ?>" class="prix_par_unite" value="<?= $prix_par_unite; ?>">
                                            <div class="content-col clr">
                                                <div class="left">
                                                    <img src="<?= $visuel; ?>" alt="Visuel" />
                                                </div>
                                                <div class="right">
                                                    <div class="modele">
                                                        <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                        <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                        <div class="texte clr">
                                                            <div class="numbers-row clr">
                                                                <span class="dec button">-</span>
                                                                <input type="text" name="nb_unite<?= $eclairage_ID; ?>" class="nb_unite" id="qtt" value="2" class="qtt">      
                                                                <span class="inc button">+</span>
                                                            </div>
                                                            <div class="textDistance">
                                                                Unités
                                                            </div>
                                                        </div>
                                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                        </label> 
                                    <?php else : ?>
                                        <label  for="<?= $numero_eclairage; ?>" class="col">
                                            <input type="radio" name="eclairage_id" id="<?= $numero_eclairage; ?>" value="<?= $eclairage_ID; ?>">
                                            <input type="hidden" name="prix_eclairage<?= $eclairage_ID; ?>" class="prix_eclairage" value="<?= $prix; ?>">
                                            <input type="hidden" name="eclairage_format<?= $eclairage_ID; ?>" class="eclairage_format" value="simple">
                                            <input type="hidden" name="name_eclairage<?= $eclairage_ID; ?>" class="name_eclairage" value="<?= $eclairage_name; ?>">
                                            <div class="content-col clr">
                                                <div class="left">
                                                    <img src="<?= $visuel; ?>" alt="Visuel" />
                                                </div>
                                                <div class="right">
                                                    <div class="modele">
                                                        <span class="title-modele"><?= $labelle_eclairage; ?></span>
                                                        <span class="num-modele"> <?= $eclairage_name; ?></span>
                                                        <span class="nombre-ecl"><?= $unite_eclairage; ?></span>
                                                        <div class="tooltips"><span class="btn-tooltips">?</span><span class="tooltip-text"><?= $explain_eclairage; ?></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="blcBtn"><span class="btn3">Sélectionner</span></div>
                                        </label>
                                    <?php  endif; ?>  
                                <?php  endif; ?>

                            <?php endif; ?>  

                        <?php endforeach; wp_reset_query(); endif; ?>   
                    </div>
                </div>
            </div>

            <?php 
                // Modele
                $numero_modele = get_field('numero_modele', $modele_id);
                $croquis       = get_field('croquis', $modele_id);
                $prix_modele   = get_field('prix', $modele_id);

                // POSE
                $pose      = get_term_by( 'id', $pose_id, 'pose' );
                // Prix pose
                $prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');              

                // COULEUR
                $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
                $prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

                // COULEUR
                $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
                $prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

                $total_price = total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle));
                $total_price_formatted = format_price($total_price);
            ?>
            <aside class="blcRight" id="aside">
                <div class="contentAside">
                    <div class="shadow">
                        <div class="recap">
                            <h2>Récapitulatif de votre simulation</h2>
                            <div class="blcModele clr">
                                <div class="modele">
                                    <span class="title-modele">Modèle</span>
                                    <span class="num-modele"><?= $numero_modele; ?></span>
                                </div>
                                <div class="plan-modele">
                                    <img src="<?= $croquis; ?>" >
                                </div>
                            </div>
                            <div class="blcPrice">
                                <div class="total">prix total</div>
                                <div class="price" oldprice="<?= $total_price; ?>">
                                    <span><?= $total_price_formatted; ?></span> € HT
                                </div>
                            </div>
                            <div class="recap-step">
                                <ul>
                                    <li>Version de pose choisie : <span><?= $pose->name; ?></span> <b class="price-step"><?= sanitize_price($prix_pose); ?></b></li>
                                    <li>Couleur : <span><?= $couleur->name; ?></span> <b class="price-step"><?= sanitize_price($prix_couleur); ?></b></li>
                                    <li>Margelle : <span><?= $margelle->name; ?></span> <b class="price-step"><?= sanitize_price($prix_margelle); ?></b></li>
                                    <li class="item_eclairage" style="display: none;">Eclairage : <span></span> <b class="price-step"></b></li>
                                </ul>
                            </div>
                        </div>
                        <div class="boutton boutton2">
                            <button class="btn-submit" type="submit">Etape suivante</button>
                        </div>
                        <ul id="error_form"></ul>
                    </div>
                </div>
            </aside>
            <?php include_once('footer-step.php'); ?>
        </form>
    </div>
</main>