<?php 
    $img_url = get_template_directory_uri().'/pools/pdf/images'; 
    $t_price = $prix_modele + $prix_pose + $prix_couleur + $prix_margelle + $prix_eclairage + $prix_local_technique + $d_price + $prix_skimmer + $prix_acces_maison + $prix_option_supp + $prix_traitement_eau;
    $all_price = number_format($t_price, 0, '', ' ');

    $tvac = format_price(total_price_tvac($t_price, 21));
    ?>
<!DOCTYPE html>
<html
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Récapitulatif de votre simulation</title>
        <style type="text/css">
            @font-face{font-family:kh-r;src:url(fonts/khand-regular.eot?#iefix) format('embedded-opentype'),url(fonts/khand-regular.woff) format('woff'),url(fonts/khand-regular.ttf) format('truetype'),url(fonts/khand-regular.svg#Khand-Regular) format('svg');font-weight:400;font-style:normal}@font-face{font-family:kh-sb;src:url(fonts/khand-semibold.eot);src:url(fonts/khand-semibold.eot?#iefix) format('embedded-opentype'),url(fonts/khand-semibold.woff) format('woff'),url(fonts/khand-semibold.ttf) format('truetype'),url(fonts/khand-semibold.svg#webfont) format('svg');font-weight:400;font-style:normal}@page{margin: 1cm}body,div,form,h1,h2,h3,h4,h5,h6,li,ol,p,span,ul{padding:0;margin:0;border:0;-webkit-text-size-adjust:none;-moz-text-size-adjust:none;text-size-adjust:none}article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}input,textarea{-webkit-appearance:none;-ms-appearance:none;appearance:none;-moz-appearance:none;-o-appearance:none;border-radius:0}*{outline:0!important}b,strong{font-weight:400;font-family:kh-sb}ul{list-style-type:none}body{font:normal 16px/24px kh-r,Arial;color:#151515;background:#fff}body *{box-sizing:border-box;outline:0}a{text-decoration:none;color:#41c2cd;outline:0}img{border:none}p{margin-bottom:10px}.clear{clear:both;float:none!important;width:100%!important;padding:0!important;margin:0!important;display:block}.clr:after,.row:after{content:'';display:table;width:100%;clear:both}.container{max-width:800px;margin:auto}.title_page p{margin-bottom:0;padding-bottom:0;padding-left:20px;line-height:14px}.title_page p b{font-family:kh-sb}h1{font:normal 32px/32px kh-r;letter-spacing:.6px;text-align:center;display:block;padding-bottom:30px}h1 span{display:block;font-size:20px;line-height:20px;font-family:kh-sb}
            .logo{text-align:center;padding:38px 0; width: 317px; display:inline-block;vertical-align:top} .logo img{width:250px}.titre{font:normal 30px/32px;font-family:kh-sb;font-weight:400}
            .coord{display:inline-block;width:28%;vertical-align:bottom}.coord li,.coordClient li{margin:0;padding:0;line-height:16px}.coordClient{float:right}..date{font:normal 18px/32px}.blcModele>div{display:inline-block;width:50%}.titreModele{font:normal 18px;font-family:kh-sb}.numModele{font:normal 40px/40px}main{padding:0 20px!important}.images{display:inline-block!important}.version{display:inline-block}.titre-vers{color:#41c2cd;font:normal 30px/32px;font-family:kh-sb}.version{background-color:#f5f5f5;text-align:center;padding:15px 0 15px 15px!important;clear:both;float:none!important;width:100%!important}.texte{display:block;line-height:13px}.texte1{line-height:10px}.chiffre{color:#41c2cd}.chiffre-end{text-align:center;color:#41c2cd}.prix-total{color:#41c2cd;font:normal 30px/32px;font-family:kh-sb}.puce-icons{padding-top:16px}.caract{line-height:16px}
                .condition p{ font-size: 9.5px;line-height: 9px; text-align: justify;margin-bottom: 0;padding-bottom: 5px }
                 .condition h3{ font-size: 11px;margin: 0;padding: 0  }
                 .condition h2{ font-size: 16px;margin: 0;padding: 0 }
                .line_condition { page-break-after: always; break-after: always; } 
                .condition .acceptation{ font-size: 9px;line-height: 9px;margin: 0;padding: 0 }
                .condition .texte{ font-size: 9px;line-height: 9px;margin: 0;padding: 0 }
                .blcAcceptation{ margin:0;padding: 0 }
                @page { margin: 0px; }
                body { margin: 0px; }
        </style>
    </head>
    <body>
        <main>
            <div class="blcTop clr">
                <ul class="coord">
                    <li>
                        <strong>Energy & Welness srl</strong>
                    </li>
                    <li>Rue des Wallants, 1</li>
                    <li>7370 Dour</li>
                    <li>info@olsenandg.com</li>
                    <li>+32 476 174 336 </li>
                </ul>
                <div class="logo">
                    <img src="<?= $img_url; ?>/logo-print.png" >
                </div>
            </div>
            <div class="coordClient">
                <ul>
                    <li><strong>Nom :</strong> <?= $nom_user; ?></li>
                    <li><strong>Prénom : </strong> <?= $prenom_user; ?></li>
                    <li><strong>Code Postal :</strong> <?= $code_postal_user; ?></li>
                    <li><strong>Province :</strong> <?= $province_user; ?></li>
                    <li><strong>Téléphone :</strong> <?= $telephone_user; ?></li>
                    <li><strong>Email :</strong> <?= $email_user; ?></li>
                </ul>
            </div>
            <div class="clear"></div>
            <div class="titre">Récapitulatif de votre simulation</div>
            <div class="date"><?= date('d-m-Y H:i:s'); ?></div>
            <table width="100%">
                <tr>
                    <td>
                        <div class="left">
                            <div class="text">
                                <div class="titreModele" style="border-bottom: solid 2px #151515;width: 50px">MODELE</div>
                                <div class="numModele"><?= $numero_modele; ?></div>
                            </div>
                            <div class="images">
                                <img src="<?= $croquis; ?>">
                            </div>
                        </div>
                        <br>
                    </td>
                    <td width="100px">
                        <?php if($data_step_img): ?>
                            <img src="<?= $data_step_img; ?>" width="300" >
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
            <div class="clear"></div>
            <br>
            <div class="version">
                <div class="clear"></div>
                <div class="titre-vers">
                    Version de pose choisie : <?= strtoupper($pose->name); ?>
                </div>
                <table width="100%" class="table-recap" cellspacing="0">
                    <tr>
                        <td width="25px">
                            <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                        </td>
                        <td>
                            <strong>Délai :</strong>
                        </td>
                        <td width="35px" style="text-align: right;padding-right:15px;">
                            <span class="chiffre"><?= $data_step_pool_time; ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td width="25px">
                            <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                        </td>
                        <td>
                            <strong>Modèle :</strong>
                        </td>
                        <td width="35px" style="text-align: right;padding-right:15px;">
                            <span class="chiffre"><?= sanitize_price($prix_modele, ''); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td width="25px">
                            <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                        </td>
                        <td>
                            <strong>Inclus dans la pose :</strong>
                        </td>
                        <td width="35px" style="text-align: right;padding-right:15px;">
                            <span class="chiffre"><?= sanitize_price($prix_pose, ''); ?></span>
                        </td>
                    </tr>
                    <?php if(!empty($liste_atout)): ?>
                        <?php foreach($liste_atout as $atout) : ?>
                            <tr class="caract">
                                <td width="25px"></td>
                                <td><?= $atout['item_atout']; ?></td>
                                <td width="35px" style="text-align: right;padding-right:15px;"></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    
                    <?php // Do it Yourself
                        if($cur_step != 3 && $next_step == 13) : ?>
                        <tr>
                            <td width="25px">
                                <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                            </td>
                            <td>
                                <strong>Couleur :</strong>
                                <span><?= strtoupper($couleur->name); ?></span>
                            </td>
                            <td width="55px" style="text-align: right;padding-right:15px;">
                                <span class="chiffre"><?= sanitize_price($prix_couleur, ''); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="25px">
                                <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                            </td>
                            <td>
                                <strong>Margelle :</strong>
                                <span> <?= strtoupper($margelle->name); ?></span>
                            </td>
                            <td width="55px" style="text-align: right;padding-right:15px;">
                                <span class="chiffre"><?= sanitize_price($prix_margelle, ''); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="25px">
                                <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                            </td>
                            <td>
                                <strong>Eclairage </strong>
                                <span> <?= strtoupper($eclairage->name); ?></span>
                            </td>
                            <td width="55px" style="text-align: right;padding-right:15px;">
                                <span class="chiffre"><?= sanitize_price($prix_eclairage, ''); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="25px">
                                <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                            </td>
                            <?php if ( $local_technique->slug != 'volet-hors-sol-pvc'){ ?>
                            <td>
                                <strong>Local technique: </strong>
                                <span>  LOCAL TECHNIQUE <?= strtoupper($local_technique->name); ?> <u>avec</u> <?= $distance_value; ?>M DE DISTANCE</span>
                            </td>
                            <td width="55px" style="text-align: right;padding-right:15px;">
                                <span class="chiffre"><?php $loc_price = $prix_local_technique + $d_price; echo sanitize_price($loc_price, ''); ?></span>
                            </td>
                            <?php }else{ ?>
                            <td>
                                <strong>Local technique: </strong>
                                <span>  LOCAL TECHNIQUE <?= strtoupper($local_technique->name); ?></span>
                            </td>
                            <td width="55px" style="text-align: right;padding-right:15px;">
                                <span class="chiffre">Prix à définir lors de la visite technique</span>
                            </td>
                            <?php }?>
                        </tr>
                        <tr>
                            <td width="25px">
                                <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                            </td>
                            <td>
                                <strong>Skimmer:</strong>
                                <span><?= strtoupper($skimmer->name); ?></span>
                            </td>
                            <td width="55px" style="text-align: right;padding-right:15px;">
                                <span class="chiffre"><?= sanitize_price($prix_skimmer, ''); ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td width="25px">
                                <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                            </td>
                            <td>
                                <strong>Accès Maison: </strong>
                                <span> <?= strtoupper($acces_maison->name); ?></span>
                            </td>
                            <td width="55px" style="text-align: right;padding-right:15px;">
                                <span class="chiffre"><?= sanitize_price($prix_acces_maison, ''); ?></span>
                            </td>
                        </tr>
                        <br>
                        <tr>
                            <td width="25px">
                                <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                            </td>
                            <td>
                                <strong>Option supplémentaire:</strong>
                            </td>
                            <td width="55px" style="text-align: right;padding-right:15px;">
                                <span class="chiffre-end"></span>
                            </td>
                        </tr>
                        <!-- option supp -->
                        <?php 
                        if(!empty($option_supp_ids)):
                        foreach ($option_supp_ids as $option_supp_id) :
                                $op_supp = get_term_by( 'id', $option_supp_id, 'option_supplementaire' );
                                // $item_price = conditionnal_price($modele_id, $option_supp_id, 'option_supplementaire');
                                $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
                            ?>
                            <tr class="caract">
                                <td width="25px"></td>
                                <td>
                                    <span class="texte"><?= $op_supp->name; ?></span>
                                </td>
                                <td width="275px" style="text-align: right;padding-right:15px;">
                                <?php if ( $op_supp->slug != 'enlevement-des-terres' && $op_supp->slug != 'volet-hors-sol-pvc'){ ?>
                                    <span class="chiffre-end"><?= sanitize_price($item_price, ''); ?></span>
                                <?php }else{ ?>
                                    <span class="chiffre-end">Prix à définir lors de la visite technique</span>
                                <?php } ?>
                            </td>
                            </tr>
                        <?php endforeach; endif; ?>
                        <!-- # option supp -->

                        <tr>
                            <td width="25px">
                                <img src="<?= $img_url; ?>/icon-selected-2.png" class="puce-icons">
                            </td>
                            <td>
                                <strong>Traitement de l'eau: </strong>
                                <span> <?= strtoupper($traitement_eau->name); ?></span>
                            </td>
                            <td width="55px" style="text-align: right;padding-right:15px;">                            
                                <span class="chiffre"><?= sanitize_price($prix_traitement_eau, ''); ?></span>
                            </td>
                        </tr>
                    <?php endif; ?>

                </table>
            </div>
            <table width="100%">
                <tr>
                    <td>
                        <strong>Pour acceptation "lu et approuvé"</strong>
                        <span class="texte">Sous réserve de visite technique de notre technicien.</span>
                    </td>
                    <td width="50px">
                        <span style="border-bottom: solid 2px #151515;">PRIX TOTAL</span>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td width="180px">
                        <span class="prix-total chiffre-end"><?= $all_price; ?> € HT</span><br>
                        <div class="tvac" style="font-size: 12px; color:#3d3d3d">Prix TVAC : <?= $tvac; ?> €</div>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td width="180px">
                        <?php 

                            if($activation_code_promo == 'oui') :

                                if (empty($my_code_promo) || $my_code_promo == null ){
                                    echo 'Pas de code promo inséré';
                                }elseif($code_promo_admin && trim($code_promo_admin) == trim($my_code_promo) ) {
                                    echo 'Remise code "'.$my_code_promo.'" valable jusqu\'au '.$validite_code_promo_admin.'';
                                } else {
                                    echo "Vous avez inséré un mauvais code promo '".$my_code_promo."'";
                                } 
                                
                            endif;
                        ?>
                    </td>
                </tr>
            </table>
            <hr class="line_condition">
            <div class="condition">
                <?= $cg_text; ?>
                <table width="100%">
                    <tr>
                        <td>
                            <strong class="acceptation">Pour acceptation "lu et approuvé"</strong>
                            <span class="texte">Sous réserve de visite technique de notre technicien.</span>
                        </td>
                    </tr>
                </table>
            </div>
        </main>
    </body>
</html>