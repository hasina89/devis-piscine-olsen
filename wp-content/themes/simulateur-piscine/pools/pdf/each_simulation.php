<?php

// add_action( 'wp_ajax_pool', 'pool' );
// add_action( 'wp_ajax_nopriv_pool', 'pool' );

use Dompdf\Dompdf;
use Dompdf\Options;

$data_step = sanitize_content_step(get_item_step_data($each_simulation_id, 'content_step'));
$data_step_img = get_item_step_data($each_simulation_id, 'img');  
$data_step_pool_time = get_item_step_data($each_simulation_id, 'pool_time'); 

$option_supp_ids = $data_step->option_supp;

require_once 'dompdf/autoload.inc.php';
$dompdf = new Dompdf();


$content_path = 'wp-content/uploads/pdf-piscine';
$path = ABSPATH.$content_path;
if (!file_exists($path)) {
    mkdir($path, 0755, true);
}

/* Delete Cache Files Here */
$dir = $path.'/';
foreach (glob($dir."*") as $old_file) {
    if ( time() - filemtime($old_file) > 86400 ) {
        unlink($old_file);
    }
}

// Get Data
$modele_id          = $data_step->modele;
$pose_id            = $data_step->type_pose;
$couleur_id         = $data_step->choix_couleur;
$margelle_id        = $data_step->choix_margelle;

$eclairage_id     = $data_step->eclairage_id;
$eclairage_format = $data_step->eclairage_format;
$eclairage_prix   = $data_step->eclairage_prix;
$nb_unite         = $data_step->nb_unite;

$local_technique_id = $data_step->local_teknik;
$distance_locale_id = $data_step->distance_id;

$distance_id        = $data_step->distance_id;
$distance_type      = $data_step->distance_type;
$distance_value     = $data_step->distance_value;
$distance_prix      = $data_step->distance_prix;

$skimmer_id         = $data_step->type_skimmer;
$acces_maison_id    = $data_step->type_acces_maison;
$option_supp_ids    = $data_step->option_supp;
$traitement_eau_id  = $data_step->traitement_eau;


// Modele
$numero_modele = get_field('numero_modele', $modele_id);
$croquis       = get_field('croquis', $modele_id);
$prix_modele   = get_field('prix', $modele_id);

// POSE
$pose      = get_term_by( 'id', $pose_id, 'pose' );
$prix_pose = conditionnal_tax_price($modele_id, $pose_id, 'pose');      
$liste_atout = get_field('liste_atout', 'pose_'.$pose_id); 

// COULEUR
$couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
$prix_couleur = conditionnal_price($modele_id, $couleur_id, 'couleur'); 

// Margelle
$margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
$prix_margelle = get_field( 'prix', 'margelle_'.$margelle_id );

// Eclairage
$eclairage_id     = $data_step->eclairage_id;
$eclairage_format = $data_step->eclairage_format;
$eclairage_prix   = $data_step->eclairage_prix;
$nb_unite         = $data_step->nb_unite;

$eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );
if ($eclairage_format == 'perso') {
    $prix_eclairage = $nb_unite * $eclairage_prix;
}else {
    $prix_eclairage = $eclairage_prix;
}   

// Local technique
$local_technique      = get_term_by( 'id', $local_technique_id, 'local_technique' );
$prix_local_technique = get_field( 'prix', 'local_technique_'.$local_technique_id );
$sur_devis            = get_field('sur_devis', 'local_technique_'.$local_technique_id);

// Distance Local technique   
$distance_id        = $data_step->distance_id;
$distance_type      = $data_step->distance_type;
$distance_value     = $data_step->distance_value;
$distance_prix      = $data_step->distance_prix;

if ($distance_type == 'perso') {
    $d_price = real_distance_price($distance_value, $distance_prix);
}else {
    $d_price = $distance_prix;
}

// Skimmer
$skimmer      = get_term_by( 'id', $skimmer_id, 'skimmer' );
$prix_skimmer = conditionnal_price($modele_id, $skimmer_id, 'skimmer', 'prix_skimmer');

$acces_maison      = get_term_by( 'id', $acces_maison_id, 'acces_maison' );
$prix_acces_maison = conditionnal_price($modele_id, $acces_maison_id, 'acces_maison');

$prix_option_supp = 0;
if(!empty($option_supp_ids)){
    foreach ($option_supp_ids as $option_supp_id) {
        $item_price = $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
        $prix_option_supp += conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
    }
}


$traitement_eau      = get_term_by( 'id', $traitement_eau_id, 'eau' );
$prix_traitement_eau = price_from_pose($pose_id, $traitement_eau_id, 'eau');


$user_datas = get_user_pool_data($id_user);
$user = get_lead_by_id($id_user);
$nom_user = $user_datas[0]->nom;
$prenom_user = $user_datas[0]->prenom;
if ( $user[0]->code_postal_step != '0'){
    $code_postal_user = $user[0]->code_postal_step;
}else{
    $code_postal_user = $user_datas[0]->code_postal;
}
if ( $user[0]->province_step != ''){
    $province_user = $user[0]->province_step;
}else{
    $province_user = $user_datas[0]->province;
}
$telephone_user = $user_datas[0]->telephone;
$email_user = $user_datas[0]->email;

// Code promo
$activation_code_promo = get_field('activation', 'option');
$code_promo_admin = get_field('code_promo', 'option');
$validite_code_promo_admin = get_field('validite_code', 'option');

$my_code_promo = get_customer_code_promo($id_user);

ob_start();
    include("content_bo.php");
$html = ob_get_clean();  

$cl_reference = date('Y-m-d_H-i');
$doctitle = $nom_user.'_'.$prenom_user.'_'.$each_simulation_id.'_'. $cl_reference.'.pdf';   
$doctitle = str_replace(' ', '', $doctitle);  

$dompdf->loadHtml($html);
$dompdf->set_option('isRemoteEnabled', TRUE);
$dompdf->setPaper('A4', 'portrait');
$dompdf->render();

// $dompdf->stream($doctitle.'.pdf');

$pdfToAttach = $path.'/'.$doctitle;
$output = $dompdf->output();

$relativePath = site_url('/').$content_path.'/'.$doctitle;

$saveFile = 0;
$saveFile = file_put_contents($pdfToAttach, $output);

if($saveFile){
    echo $relativePath;
}

die();

?>
