<?php 

/* All Functions */
require_once 'functions/helper.php';
require_once 'functions/db.php';

// init CPT
function pool_cpt() {
	$domaine = 'pool';    

    /* Piscine */
    register_post_type( "pool", array(
        'labels' => array(
            'name'                  => __( "Modèles Piscines", $domaine ),
            'singular_name'         => __( "Modèle Piscine", $domaine ),
            'all_items'             => __( "Toutes les piscines", $domaine ),
            'add_new'               => __( "Nouvelle piscine", $domaine ),
            'add_new_item'          => __( "Nouvelle piscine", $domaine ),
            'edit_item'             => __( "Édition", $domaine ),
            'new_item'              => __( "Nouveau", $domaine ),
            'view_item'             => __( "Édition", $domaine ),
            'not_found'             => __( "Aucun résultat trouvé", $domaine ),
            'not_found_in_trash'    => __( "Aucun résultat trouvé", $domaine )
        ),
        'publicly_queryable'        => true,
        'public'                    => true,
        'has_archive'               => true,
        'supports'                  => array( 'title' ,'editor'),
        'capability_type'           => 'post',
        'rewrite'                   => array ( 'with_front' => false, "slug" => "pool" ),
        'menu_position'             => 10,
        'menu_icon'   				=> 'dashicons-palmtree',
        'label'                     => __( 'pool', $domaine ),
        'show_ui'                   => true,
        'show_in_menu'              => true,
        'show_in_nav_menus'         => true,
        'description'               => __( "pool", $domaine )
        )
    ); 
}
add_action( 'init', 'pool_cpt' );

/* BO */
function pool_list_menu() {
    add_submenu_page( 
        'edit.php?post_type=pool',
        'Liste des Leads',
        'Liste des Leads',
        'manage_options',
        'pool_simulation_leads',
        'user_pool_list_fx'
    );

    /*add_submenu_page( 
        '',
        'Liste de Simulation',
        'Liste de Simulation',
        'manage_options',
        'pool_simulation_list',
        'pool_fx'
    );*/

    add_submenu_page( 
        '',
        'Détail Simulation',
        'Détail Simulation',
        'manage_options',
        'pool_simulation_detail',
        'pool_detail_fx'
    );

    add_submenu_page( 
        '',
        'Export Leads',
        'Export Leads',
        'manage_options',
        'pool_simulation_export',
        'pool_export_fx'
    );
}
add_action('admin_menu', 'pool_list_menu'); 

function user_pool_list_fx(){

    global $wpdb;
    // $sql = "SELECT * FROM {$wpdb->prefix}pool_customers ORDER BY nom DESC";
    $users = get_leads(); 

    $total = count($users);

    ?>
    <style>
        .liste_news .wp-list-table { width: 99% !important; }
        .liste_news .wp-list-table thead tr{ background: #eeecec !important; }
        .alternate, .striped > tbody > tr td { border-bottom: 1px solid #f1f1f1; }
        .alternate, .striped > tbody > :nth-child(2n+1), ul.striped > :nth-child(2n+1){ background-color: #f9f9f9 !important; }
        /*.alternate, .striped > tbody > tr td.unfinished { border-left: 5px solid #f00; border-bottom: 3px solid transparent; border-top: 3px solid transparent; }*/
        .alternate, .striped > tbody > tr td.finished { border-left: 5px solid #10b805; border-bottom: 3px solid transparent; border-top: 3px solid transparent; background: rgba(16, 184, 5, 0.16); }
        .sorting-arrow {display: block;visibility: hidden;width: 10px;height: 4px;margin-top: 8px;margin-left: 7px;}
        .sorting-arrow:before{ content: "\f142"; font: normal 20px/1 dashicons; speak: none;display: inline-block;padding: 0;top: -4px;left: -8px; color: #444;line-height: .5;position: relative;vertical-align: top;-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale; text-decoration: none!important;color: #444; }
        .d-flex{ display: flex;justify-content: space-between;align-items: center }
        .no-result{text-align: center;font-size: 15px;font-weight: 700;}
    </style>
    <div class="wrap">
        <div class="d-flex">
            <h1 class="wp-heading-inline">Liste de Simulations de Piscines <a id="export-result" href="<?= link_export(); ?>" class="page-title-action export-csv-leads">Exporter en XLS</a></h1>
            
            <h2 class="filtre-province">
                Trier la liste par province
                <select id="tri-province-select">
                    <?php
                        $provinces = get_field('emails_responsable','option');
                        if ($provinces)
                            $provinces = $provinces['le_responsable']; ?>
                        <option value="all">Tous les provinces</option>
                        <?php
                        foreach( $provinces as $province ){ ?>
                        <option value="<?php echo $province['province']; ?>"><?php echo $province['province']; ?></option>
                        <?php } ?>
                    ?>
                </select>
                <a href="#" class="page-title-action" id="tri-province">Procéder au tri</a>
            </h2>
        </div>
        <div class="clear"></div>
        <p>Il y a <b id="total-result"><?= $total; ?></b> lead(s) dans votre base de données</p>
        <table class="wp-list-table widefat fixed striped tablesorter">
            <thead>
                <tr>
                    <th class="sorter-false">Nom & Prénom</th>
                    <th class="sorter-false">Email</th>
                    <th class="sorter-false">Code Postal</th>
                    <th class="sortable-province">
                        <a href="#" title="">
                            <span>Province</span>
                        </a>
                    </th>
                    <th class="sorter-false">Téléphone</th>
                    <th class="sorter-false">Etape</th>
                    <th class="sorter-false">Status</th>
                    <th class="sorter-false">Délai</th>
                    <th class="sorter-false">Prix du lead</th>
                    <!-- <th class="sortable-prix">
                        <a href="#" title="">
                            <span>Prix du lead</span>
                            <span class="sorting-arrow"></span>
                        </a>
                    </th> -->
                    <th class="sorter-false">Rappel</th>
                    <th class="sorter-false">Date</th>
                </tr>
            </thead>
            
            <tbody id="the-list">
                <?php foreach ($users as $user) :
                    $pool_id   = $user->simulation_id;
                    $lead_id   = $user->customer_id;
                    $nom       = $user->nom .' '.$user->prenom;
                    $email     = $user->email;
                    if ( $user->code_postal_step != '0' ){
                        $codepost  = $user->code_postal_step;
                    }else{
                        $codepost  = $user->code_postal;
                    }
                    if ( $user->province_step != '' ){
                        $province  = $user->province_step;
                    }else{
                        $province  = $user->province;
                    }
                    
                    $step      = $user->step;
                    $telephone = $user->telephone;
                    $finished  = $user->finished;
                    $pool_time = $user->pool_time;
                    if ( $user->prix_lead_step != ''){
                        $prix_lead = $user->prix_lead_step;
                    }else{
                        $prix_lead = $user->prix_lead;
                    }
                    $remind    = $user->remind;
                    $date      = $user->last_modify;

                    if ($finished == 1) {
                        $txt_finished = 'Terminé';
                        $class_finished = 'finished';
                    }else {
                        $txt_finished = 'En cours';
                        $class_finished = 'unfinished';
                    }

                ?>            
                    <tr>
                        <td><?= $nom; ?> <br><a href="<?= link_detail( $lead_id, $pool_id ); ?>">Voir détail</a></td>
                        <td><?= $email; ?></td>
                        <td><?= $codepost; ?></td>
                        <td><?= $province; ?></td>
                        <td><?= $telephone; ?></td>
                        <td><?= $step; ?></td>
                        <td class="<?= $class_finished; ?>"><?= $txt_finished; ?></td>
                        <td><?= $pool_time; ?></td>
                        <td><?= $prix_lead; ?></td>
                        <td><?= $remind; ?></td>
                        <td><?= $date; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php }

// Liste pool
function pool_fx(){

    $url_leads = admin_url('edit.php?post_type=pool&page=pool_simulation_leads');
    if (isset($_GET['lead_id']) && $_GET['lead_id'] != null) {
        $lead_id = $_GET['lead_id'];
    }else {
        wp_redirect( $url_leads );
    }
    
    global $wpdb;

    $user_data = get_user_pool_data($lead_id)[0];

    $user_simulation = get_user_simulation($lead_id);
    $total_user_simulation = count($user_simulation);

    ?>
    <style>
        .liste_news .wp-list-table { width: 99% !important; }
        .liste_news .wp-list-table thead tr{ background: #eeecec !important; }
        .alternate, .striped > tbody > tr td { border-bottom: 1px solid #f1f1f1; }
        .alternate, .striped > tbody > :nth-child(2n+1), ul.striped > :nth-child(2n+1){ background-color: #f9f9f9 !important; }
        /*.alternate, .striped > tbody > .unfinished { background: #fbadad !important; }*/
        .alternate, .striped > tbody > tr td.unfinished { border-left: 5px solid #f00; border-bottom: 3px solid transparent; border-top: 3px solid transparent; }
    </style>
    <div class="wrap">
        <h1 class="wp-heading-inline">Liste de Simulations de Piscines de <b><?= $user_data->nom.' '. $user_data->prenom ?></b></h1>
        <p>Il y a <b><?= $total_user_simulation; ?></b> simulation(s)</p>
        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th>Modèle</th>
                    <th>Etape</th>
                    <th>Piscine</th>
                    <th>Code Promo</th>
                    <th>Rappel</th>
                    <th>Etat</th>
                    <th>Date</th>
                </tr>
            </thead>
            
            <tbody id="the-list">
                <?php foreach ($user_simulation as $simulation) : 
                    $simulation_id = $simulation->id;
                    $content_step_data = json_decode( $simulation->content_step);
                    $modele_id = $content_step_data->modele;
                    $numero_modele = get_field('numero_modele', $modele_id);
                    $step = $simulation->step;
                    $code_promo = $simulation->code_promo;
                    $remind = $simulation->remind;
                    $last_modify = $simulation->last_modify;
                    $pool_time = $simulation->pool_time;
                    $finished = $simulation->finished;

                    if ($finished == 1) {
                        $txt_finished = 'Terminé';
                        $class_finished = 'finished';
                    }else {
                        $txt_finished = 'En cours';
                        $class_finished = 'unfinished';
                    }
                    ?>            
                    <tr>
                        <td  class="<?= $class_finished; ?>"><?php echo $numero_modele; ?> <br><a href="<?= link_detail($lead_id, $simulation_id); ?>">Voir détail</a></td>
                        <td><?= $step; ?></td>
                        <td><?= $pool_time; ?></td>
                        <td><?= $code_promo; ?></td>
                        <td><?= $remind; ?></td>
                        <td><?= $txt_finished; ?></td>
                        <td><?= $last_modify; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php } 

// Détail Pool
function pool_detail_fx(){ 
    $url_list = admin_url('edit.php?post_type=pool&page=pool_simulation_leads');
    if (isset($_GET['lead_id']) && $_GET['lead_id'] != null && isset($_GET['pool_id']) && $_GET['pool_id'] != null ) {
        $pool_id = $_GET['pool_id'];
        $lead_id = $_GET['lead_id'];
    }else {
        wp_redirect( $url_list );
    }

    $info_user = get_user_pool_data($lead_id)[0];
    $info_pool = get_simulation_detail($pool_id)[0];

    $img = $info_pool->img;

    $data_step = sanitize_content_step($info_pool->content_step);

    // Get Data
    $modele_id          = $data_step->modele;
    $pose_id            = $data_step->type_pose;
    $couleur_id         = $data_step->choix_couleur;
    $margelle_id        = $data_step->choix_margelle;
    $eclairage_id       = $data_step->eclairage_id;
    $local_technique_id = $data_step->local_teknik;
    $distance_locale_id = $data_step->distance_id;
    $distance_value     = $data_step->distance_value;
    $skimmer_id         = $data_step->type_skimmer;
    $acces_maison_id    = $data_step->type_acces_maison;
    $traitement_eau_id  = $data_step->traitement_eau;
    $option_supp_ids    = $data_step->option_supp;

    // Modele
    $numero_modele = give_field('numero_modele', $modele_id);
    $croquis       = give_field('croquis', $modele_id);
    $prix_modele   = give_field('prix', $modele_id);

    // POSE
    $pose      = get_term_by( 'id', $pose_id, 'pose' );
    $prix_pose = give_field( 'prix', $pose_id, 'pose' );

    // COULEUR
    $couleur      = get_term_by( 'id', $couleur_id, 'couleur' );
    $prix_couleur = give_field( 'prix', $couleur_id, 'couleur' );

    // Margelle
    $margelle      = get_term_by( 'id', $margelle_id, 'margelle' );
    $prix_margelle = give_field( 'prix', $margelle_id, 'margelle' );

    // Eclairage
    $eclairage      = get_term_by( 'id', $eclairage_id, 'eclairage' );
    $prix_eclairage = give_field( 'prix', $eclairage_id, 'eclairage' );

    // Local technique
    $local_technique      = get_term_by( 'id', $local_technique_id, 'local_technique' );
    $prix_local_technique = give_field( 'prix', $local_technique_id, 'local_technique' );

    // Distance Local technique
    $distance_locale      = get_term_by( 'id', $distance_locale_id, 'distance' );
    $prix_distance_locale = give_field( 'prix', $distance_locale_id, 'distance' );

    // Skimmer
    $skimmer      = get_term_by( 'id', $skimmer_id, 'skimmer' );
    $prix_skimmer = give_field( 'prix', $skimmer_id, 'skimmer' );

    // Accès maison
    $acces_maison = get_term_by( 'id', $acces_maison_id, 'acces_maison' );

    // Traitement de l'eau
    $traitement_eau      = get_term_by( 'id', $traitement_eau_id, 'eau' ); 


    ?>
    <style>
        .liste_news .wp-list-table { width: 99% !important; }
        .liste_news .wp-list-table thead tr{ background: #eeecec !important; }
        .alternate, .striped > tbody > tr td { border-bottom: 1px solid #f1f1f1; }
        .alternate, .striped > tbody > :nth-child(2n+1), ul.striped > :nth-child(2n+1){ background-color: #f9f9f9 !important; }

        .btn_pdf_export span { font-size: 40px; width: auto; height: auto; display: block; text-align: center; }
    </style>
    <div class="liste_news" style="margin: 50px auto;">
        <h2>Détail de la Simulations</h2>
        <p><a href="<?= $url_list; ?>" class="">Revenir à la liste</a></p>
        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th width="25%">Info Client</th>
                    <th width="65%">Info Piscine</th>
                    <th width="15%">Télécharger en PDF</th>
                </tr>
            </thead>
            
            <tbody id="the-list">           
                <tr class="<?= $class; ?>">
                    <td>
                        <ul>
                            <li><span>Nom :</span> <b><?= $info_user->nom; ?></b></li>
                            <li><span>Prénom :</span> <b><?= $info_user->prenom; ?></b></li>
                            <li><span>Téléphone :</span> <b><?= $info_user->telephone; ?></b></li>
                            <li><span>Email :</span> <b><?= $info_user->email; ?></b></li>
                            <?php if ( $info_pool->code_postal_step != '0' ){ ?>
                                <li><span>Code postal :</span> <b><?= $info_pool->code_postal_step; ?></b></li>
                            <?php }else{ ?>
                                <li><span>Code postal :</span> <b><?= $info_user->code_postal; ?></b></li>
                            <?php }
                                if ( $info_pool->province_step != '' ){ ?>
                                <li><span>Province :</span> <b><?= $info_pool->province_step; ?></b></li>
                            <?php }else{ ?>
                                <li><span>Province :</span> <b><?= $info_user->province; ?></b></li>
                            <?php }
                                if ( $info_pool->prix_lead_step != '' ){ ?>
                                <li><span>Prix du lead :</span> <b><?= $info_pool->prix_lead_step; ?></b></li>
                            <?php }else{ ?>
                                <li><span>Prix du lead :</span> <b><?= $info_user->prix_lead; ?></b></li>
                            <?php } ?>
                        </ul>
                    </td>
                    <td>                            
                        <div class="recap-step">
                            <table class="">
                                <tr>
                                    <td>Modèle</td>
                                    <td><strong><?= $numero_modele; ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Croquis</td>
                                    <td><img src="<?= $croquis; ?>" alt="Croquis"></td>
                                </tr>
                                <tr>
                                    <td>Prix total (€ TTC)</td>
                                    <td><strong><?= total_price(array($prix_modele, $prix_pose, $prix_couleur, $prix_margelle, $prix_eclairage, $prix_local_technique, $prix_skimmer)); ?></strong></td>
                                </tr>

                                <tr>
                                    <td>Pose</td>
                                    <td><strong><?= $pose->name; ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Couleur</td>
                                    <td><strong><?= $couleur->name; ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Margelle</td>
                                    <td><strong><?= $margelle->name; ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Eclairage</td>
                                    <td><strong><?= $eclairage->name; ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Local technique</td>
                                    <td><strong><?= $local_technique->name; ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Distance</td>
                                    <td><strong><?= isset($distance_value) ? $distance_value.' m de distance' : ''; ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Skimmer</td>
                                    <td><strong><?= $skimmer->name; ?></strong></td>
                                </tr>
                                <tr>
                                    <td>Accès Maison</td>
                                    <td><strong><?= $acces_maison->name; ?></strong></td>
                                </tr> 
                                <tr>
                                    <td>Options supplémentaires</td>
                                    <td>
                                        <ul>
                                            <?php 
                                                if(!empty($option_supp_ids)):
                                                    foreach ($option_supp_ids as $option_supp_id) :
                                                        $op_supp = get_term_by( 'id', $option_supp_id, 'option_supplementaire' );
                                                        $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
                                            ?>
                                                <li> > <?= $op_supp->name; ?> <!-- <b class="price-step"><?= sanitize_price($item_price, ''); ?></b> --></li>
                                            <?php endforeach; endif; ?>
                                        </ul>
                                    </td>
                                </tr>                               
                                <tr>
                                    <td>Traitement de l'eau</td>
                                    <td><strong><?= $traitement_eau->name; ?></strong></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td>
                        <form class="form_pdf">
                            <input type="hidden" name="each_simulation_id" value="<?= $pool_id; ?>">
                            <input type="hidden" name="id_user" value="<?= $lead_id; ?>">
                            <button class="button btn_pdf_export" name="" value=""><span class="dashicons dashicons-media-text"></span>PDF</button>
                            <p id="box_pdf"></p>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
<?php } 