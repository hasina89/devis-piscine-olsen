<?php 
$img_url = get_template_directory_uri().'/pools/email/images'; 
$t_price = $prix_modele + $prix_pose + $prix_couleur + $prix_margelle + $prix_eclairage + $prix_local_technique + $d_price + $prix_skimmer + $prix_acces_maison + $prix_option_supp + $prix_traitement_eau;
$prix_modele = number_format($prix_modele, 0, '', ' ');
$all_price = number_format($t_price, 0, '', ' ');

$tvac = format_price(total_price_tvac($t_price, 21));

?>
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <title>
         Récapitulatif de votre simulation
      </title>
      <!--[if !mso]>-->

      <meta http-equiv="X-UA-Compatible" content="IE=edge      <!--<![endif]-->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style type="text/css">
         #outlook a { padding:0; }
         body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
         table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
         img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
         p { display:block;margin:13px 0; }
      </style>
      <!--[if mso]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if lte mso 11]>
      <style type="text/css">
         .mj-outlook-group-fix { width:100% !important; }
      </style>
      <![endif]-->
      <style type="text/css">
         @media only screen and (min-width:540px) {
         .mj-column-per-100 { width:100% !important; max-width: 100%; }
         .mj-column-per-35-5 { width:35.5% !important; max-width: 35.5%; }
         .mj-column-per-28-5 { width:28.5% !important; max-width: 28.5%; }
         .mj-column-per-36 { width:36% !important; max-width: 36%; }
         .mj-column-per-60 { width:60% !important; max-width: 60%; }
         .mj-column-per-40 { width:40% !important; max-width: 40%; }
         .mj-column-per-50 { width:50% !important; max-width: 50%; }
         .mj-column-per-80 { width:80% !important; max-width: 80%; }
         .mj-column-per-20 { width:20% !important; max-width: 20%; }
         .mj-column-per-70 { width:70% !important; max-width: 70%; }
         .mj-column-per-30 { width:30% !important; max-width: 30%; }
         }
      </style>
      <style type="text/css">
         [owa] .mj-column-per-100 { width:100% !important; max-width: 100%; }
         [owa] .mj-column-per-35-5 { width:35.5% !important; max-width: 35.5%; }
         [owa] .mj-column-per-28-5 { width:28.5% !important; max-width: 28.5%; }
         [owa] .mj-column-per-36 { width:36% !important; max-width: 36%; }
         [owa] .mj-column-per-60 { width:60% !important; max-width: 60%; }
         [owa] .mj-column-per-40 { width:40% !important; max-width: 40%; }
         [owa] .mj-column-per-50 { width:50% !important; max-width: 50%; }
         [owa] .mj-column-per-80 { width:80% !important; max-width: 80%; }
         [owa] .mj-column-per-20 { width:20% !important; max-width: 20%; }
         [owa] .mj-column-per-70 { width:70% !important; max-width: 70%; }
         [owa] .mj-column-per-30 { width:30% !important; max-width: 30%; }
      </style>
      <style type="text/css">
         @media only screen and (max-width:540px) {
         table.mj-full-width-mobile { width: 100% !important; }
         td.mj-full-width-mobile { width: auto !important; }
         }
      </style>
      <style type="text/css">
        a{ text-decoration:none}
         span{ text-transform:uppercase;color:#0c0c0c!important}
         @media only screen and (max-width:569px) {
         .col-left>table> tbody> tr> td{ padding-right:5px!important}
         .text-recap{ width:180px!important}
         .vide{ width:50%!important;max-width:50%}
         .coord-client{ width:50%!important;max-width:50%}
         .title>table> tbody> tr> td{ padding-top:20px!important}
         .blcAcceptation{ width:70%!important;max-width:70%!important}
         .totalPrice{ width:30%!important;max-width:30%!important}
         }
         @media only screen and (max-width:540px) {
         .pad-top>table> tbody> tr> td{ padding-top:0!important}
         .pad-mob>table> tbody> tr> td{ padding:0 20px 25px!important}
         .h-mob>div{ height:20px!important}
         .col-left>table> tbody> tr> td{ padding-right:0!important}
         .coord-client>table> tbody> tr> td{ padding:0 0 20px 0!important}
         .blcCoord>table> tbody> tr> td{ padding:20px 0!important}
         .title>table> tbody> tr> td{ padding:20px 20px 30px!important}
         .totalPrice>table> tbody> tr> td{ padding-top:20px!important}
         .titleChoice{ padding-left:0!important}
         .blcAcceptation,
         .totalPrice,
         .coord-client{ width:100%!important;max-width:100%!important}
         .icon-pdf>table{ margin:auto!important}
         .blcPrix>table> tbody> tr> td{ padding-bottom:30px!important}
         }
      </style>
   </head>
   <body style="background-color:#eeeeee;">
      <div
         style="background-color:#eeeeee;"
         >
         <!--[if mso | IE]>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:710px;" width="710"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="margin:0px auto;max-width:710px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:0;text-align:center;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:710px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td height="45" style="vertical-align:top;height:45px;">
                                                                           <![endif]-->
                                                                           <div
                                                                              style="height:45px;"
                                                                              >
                                                                              &nbsp;
                                                                           </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:710px;" width="710"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:710px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:rtl;font-size:0px;padding:34px 20px 5px;text-align:center;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:237.85px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-35-5 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          align="center" class="logo-outlook" style="vertical-align:top;width:190.95px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-28-5 mj-outlook-group-fix logo" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                                                     >
                                                                     <tbody>
                                                                        <tr>
                                                                           <td  style="width:60px;">
                                                                              <a
                                                                                 href="#" target="_blank"
                                                                                 >
                                                                              <img
                                                                                 alt="Piscine OlsenG" height="auto" src="<?= $img_url; ?>/logo.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:12px;" width="60"
                                                                                 />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          align="center" class="blcCoord-outlook" style="vertical-align:top;width:241.2px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-36 mj-outlook-group-fix blcCoord" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:14px;line-height:22px;text-align:left;color:#0c0c0c;"
                                                                     ><strong>Energy & Welness srl</strong><br/>
                                                                     Rue des Wallants, 1<br/>
                                                                     7370 Dour<br/>
                                                                     <a href="mailto:info@olsenandg.com" target="_blank">
                                                                     <font color="#0c0c0c">
                                                                     info@olsenandg.com<br/>
                                                                     </font>
                                                                     </a>
                                                                     <a href="tel:32476174336" target="_blank">
                                                                     <font color="#0c0c0c">
                                                                     +32 476 174 336
                                                                     </font>
                                                                     </a>
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:710px;" width="710"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:710px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:0 20px;text-align:center;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="vide-outlook" style="vertical-align:top;width:402px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-60 mj-outlook-group-fix vide" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          align="center" class="coord-client-outlook" style="vertical-align:top;width:268px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-40 mj-outlook-group-fix coord-client" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;padding-left:25px;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:14px;line-height:22px;text-align:left;color:#0c0c0c;"
                                                                     ><strong>Nom :</strong> <?= $nom_user; ?><br/>
                                                                     <strong>Prénom :</strong> <?= $prenom_user; ?><br/>
                                                                     <strong>Code Postal</strong> <?= $code_postal_user; ?><br/>
                                                                     <strong>Province</strong> <?= $province_user; ?><br/>
                                                                     <strong>Téléphone :</strong> <?= $telephone_user; ?><br/>
                                                                     <strong>Email :</strong><font color="#0c0c0c"> <?= $email_user; ?><br/></font>
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="title-outlook" style="width:710px;" width="710"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  class="title" style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:710px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:0 20px;text-align:center;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:670px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:26px;line-height:26px;text-align:left;color:#0c0c0c;"
                                                                     >Récapitulatif de votre simulation</div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;padding-top:5px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:16px;line-height:16px;text-align:left;color:#0c0c0c;"
                                                                     ><strong><?= date('d-m-Y H:i:s'); ?></strong></div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:710px;" width="710"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:710px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:0 20px;text-align:center;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:bottom;width:335px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:bottom;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     cellpadding="0" cellspacing="0" width="100%" border="0" style="color:#0c0c0c;font-family:Arial;font-size:12px;line-height:22px;table-layout:auto;width:100%;border:none;"
                                                                     >
                                                                     <tr>
                                                                        <td width="200px" valign="top">
                                                                           <table>
                                                                              <tr>
                                                                                 <td style="font-size: 18px; line-height: 18px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial" align="center">
                                                                                    <font color="#0c0c0c"><u>Modèle</u></font> <strong><font color="#41c2cd"></font></strong>
                                                                                 </td>
                                                                              </tr>
                                                                              <tr>
                                                                                 <td style="height:5px"></td>
                                                                              </tr>
                                                                              <tr>
                                                                                 <td style="font-size: 62px; line-height: 62px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;letter-spacing:-2px">
                                                                                    <font color="#0c0c0c"><?= $numero_modele; ?></font>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                        </td>
                                                                        <td style="line-height:82px;padding-top:25px" align="left" valign="bottom">
                                                                           <div style="height:85px"><img src="<?= $croquis; ?>" width="125" style="width:125px"/></div>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          align="center" class="" style="vertical-align:bottom;width:335px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-50 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:bottom;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:bottom;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                                                     >
                                                                     <tbody>
                                                                        <tr>
                                                                           <td  style="width:330px;">
                                                                              <?php if($data_step_img): ?>
                                                                                 <img
                                                                                 alt="Piscine OlsenG" height="auto" src="<?= $data_step_img; ?>" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:12px;" width="330"
                                                                                 />
                                                                              <?php endif; ?>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td height="15" style="vertical-align:top;height:15px;">
                                                                           <![endif]-->
                                                                           <div
                                                                              style="height:15px;"
                                                                              >
                                                                              &nbsp;
                                                                           </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:710px;" width="710"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:710px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:0 20px;text-align:center;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:670px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="background-color:#e4e4e4;vertical-align:top;padding:20px;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="left" class="titleChoice" style="font-size:0px;padding:0 0 20px 28px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:24px;line-height:26px;text-align:left;color:#3cc1cc;"
                                                                     ><strong>Version de pose choisie : "<?= $pose->name; ?>"</strong></div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     cellpadding="0" cellspacing="0" width="100%" border="0" style="color:#0c0c0c;font-family:Arial;font-size:14px;line-height:17px;table-layout:auto;width:100%;border:none;"
                                                                     >
                                                                     <tr>
                                                                        <td style="width:28px" valign="top" >
                                                                           <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                        </td>
                                                                        <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                           <strong>Piscine pour :</strong><span> </span>
                                                                        </td>
                                                                        <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                           <strong><font color="#41c2cd"><?= $data_step_pool_time; ?></font></strong>
                                                                        </td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td style="height:20px" height="20"></td>
                                                                     </tr>

                                                                     <tr>
                                                                        <td style="width:28px" valign="top" >
                                                                           <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                        </td>
                                                                        <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                           <strong>Modèle :</strong><span> <?= $numero_modele; ?></span>
                                                                        </td>
                                                                        <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                           <strong><font color="#41c2cd"><?= sanitize_price($prix_modele, ''); ?></font></strong>
                                                                        </td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td style="height:20px" height="20"></td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td style="width:28px" valign="top" >
                                                                           <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                        </td>
                                                                        <td style="width:600px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap text-recap1">
                                                                           <strong>Inclus dans la pose:</strong><br/> 

                                                                           <ul>
                                                                              <?php if(!empty($liste_atout)): ?>
                                                                                 <?php foreach($liste_atout as $atout) : ?>
                                                                                    <li><?= $atout['item_atout']; ?></li>
                                                                                 <?php endforeach; ?>
                                                                              <?php endif; ?>
                                                                           </ul>

                                                                        </td>
                                                                        <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                           <strong><font color="#41c2cd"><?= sanitize_price($prix_pose, ''); ?></font></strong>
                                                                        </td>
                                                                     </tr>
                                                                     <tr>
                                                                        <td style="height:20px" height="20"></td>
                                                                     </tr>
                                                                  </table>
                                                               </td>
                                                            </tr>

                                                            <?php // Do it Yourself
                                                                  if($cur_step != 3 && $next_step == 13) : ?>
                                                               <tr>
                                                                  <td
                                                                     align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                     >
                                                                     <table
                                                                        cellpadding="0" cellspacing="0" width="100%" border="0" style="color:#0c0c0c;font-family:Arial;font-size:14px;line-height:17px;table-layout:auto;width:100%;border:none;"
                                                                        >
                                                                        <tr>
                                                                           <td style="width:28px" valign="top" >
                                                                              <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                           </td>
                                                                           <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                              <strong>Couleur :</strong><span> <?= $couleur->name; ?></span>
                                                                           </td>
                                                                           <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                              <strong><font color="#41c2cd"><?= sanitize_price($prix_couleur, ''); ?></font></strong>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="height:20px" height="20"></td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width:28px" valign="top" >
                                                                              <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                           </td>
                                                                           <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                              <strong>Margelle :</strong><span> <?= $margelle->name; ?></span>
                                                                           </td>
                                                                           <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                              <strong><font color="#41c2cd"><?= sanitize_price($prix_margelle, ''); ?></font></strong>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="height:20px" height="20"></td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width:28px" valign="top" >
                                                                              <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                           </td>
                                                                           <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                              <strong>Eclairage :</strong><span> <?= $eclairage->name; ?></span>
                                                                           </td>
                                                                           <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                              <strong><font color="#41c2cd"><?= sanitize_price($prix_eclairage, ''); ?></font></strong>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="height:20px" height="20"></td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width:28px" valign="top" >
                                                                              <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                           </td>
                                                                           <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                              <strong>Local technique :</strong><span> <?= $local_technique->name; ?></span>
                                                                           </td>
                                                                           <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                              <?php if($sur_devis == 'oui') : ?>
                                                                                 <strong><font color="#41c2cd">Sur Devis</font></strong>
                                                                              <?php else: ?>
                                                                                 <strong><font color="#41c2cd"><?= sanitize_price($prix_local_technique, ''); ?></font></strong>
                                                                              <?php endif; ?>  
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="height:20px" height="20"></td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width:28px" valign="top" >
                                                                              <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                           </td>
                                                                           <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                              <strong>Distance : </strong><span> <?= $distance_value; ?>m </span>
                                                                           </td>
                                                                           <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                              <strong><font color="#41c2cd"><?= sanitize_price($d_price, ''); ?></font></strong>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="height:20px" height="20"></td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width:28px" valign="top" >
                                                                              <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                           </td>
                                                                           <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                              <strong>Skimmer: </strong> <span> <?= $skimmer->name; ?></span>
                                                                           </td>
                                                                           <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                              <strong><font color="#41c2cd"><?= sanitize_price($prix_skimmer, ''); ?></font></strong>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="height:20px" height="20"></td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width:28px" valign="top" >
                                                                              <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                           </td>
                                                                           <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                              <strong>Accès Maison : </strong> <span> <?= $acces_maison->name; ?></span>
                                                                           </td>
                                                                           <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                              <strong><font color="#41c2cd"><?= sanitize_price($prix_acces_maison, ''); ?></font></strong>
                                                                           </td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="height:20px" height="20"></td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width:28px" valign="top" >
                                                                              <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                           </td>
                                                                           <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                              <strong>Option supplémentaire:</strong><br/>
                                                                              <ul>
                                                                                  <?php 
                                                                                  if(!empty($option_supp_ids)):
                                                                                  foreach ($option_supp_ids as $option_supp_id) :
                                                                                      $op_supp = get_term_by( 'id', $option_supp_id, 'option_supplementaire' );
                                                                                      // $item_price = conditionnal_price($modele_id, $option_supp_id, 'option_supplementaire');
                                                                                      $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);
                                                                                  ?>
                                                                                      <li><?= $op_supp->name; ?></li>
                                                                                  <?php endforeach; endif; ?>
                                                                              </ul>
                                                                           </td>
                                                                           
                                                                           <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                              <strong></strong><br/>
                                                                              <ul style="padding-left: 0;list-style:none;">
                                                                                  <?php 
                                                                                  if(!empty($option_supp_ids)):
                                                                                  foreach ($option_supp_ids as $option_supp_id) :
                                                                                      $op_supp = get_term_by( 'id', $option_supp_id, 'option_supplementaire' );
                                                                                      
                                                                                      $item_price = conditionnal_tax_price_pose($modele_id, $option_supp_id, 'option_supplementaire', $pose_id);

                                                                                      if ( $op_supp->slug != 'enlevement-des-terres' && $op_supp->slug != 'volet-hors-sol-pvc'){
                                                                                  ?>
                                                                                      <li><strong><font color="#41c2cd"><?= sanitize_price($item_price); ?></font></strong></li>
                                                                                      <?php }else{ ?>
                                                                                      <li><strong><font color="#41c2cd">Sur Devis</font></strong></li>
                                                                                      <?php } ?>
                                                                                  <?php endforeach; endif; ?>
                                                                              </ul>
                                                                              
                                                                           </td>
                                                                           
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="height:20px" height="20"></td>
                                                                        </tr>
                                                                        <tr>
                                                                           <td style="width:28px" valign="top" >
                                                                              <img src="<?= $img_url; ?>/puce.png" width="28" height="auto"/>
                                                                           </td>
                                                                           <td style="width:425px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:left;font-family:Arial;" valign="top" class="text-recap">
                                                                              <strong>Traitement de l'eau: </strong> <span> <?= $traitement_eau->name; ?></span>
                                                                           </td>
                                                                           <td style="width:70px;font-size: 14px; line-height: 17px; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;text-align:right;font-family:Arial;" valign="top">
                                                                              <strong><font color="#41c2cd"><?= sanitize_price($prix_traitement_eau, ''); ?></font></strong>
                                                                           </td>
                                                                        </tr>
                                                                     </table>
                                                                  </td>
                                                               </tr>
                                                            <?php endif; ?>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:710px;" width="710"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:710px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:18px 20px 40px;text-align:center;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="blcAcceptation-outlook" style="vertical-align:top;width:536px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-80 mj-outlook-group-fix blcAcceptation" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:16px;line-height:16px;text-align:left;color:#0c0c0c;"
                                                                     ><strong> Pour acceptation "lu et approuvé"</strong></div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:14px;line-height:16px;text-align:left;color:#0c0c0c;"
                                                                     >Sous réserve de visite technique de notre téchnicien</div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          align="center" class="totalPrice-outlook" style="vertical-align:top;width:134px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-20 mj-outlook-group-fix totalPrice" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:16px;line-height:16px;text-align:left;color:#0c0c0c;"
                                                                     ><u>PRIX TOTAL</u></div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;padding-top:20px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:22px;line-height:22px;text-align:left;color:#41c2cd;"
                                                                     ><strong><?= $all_price; ?>€ HT</strong></div>
                                                                     
                                                                  <div class="tvac" style="font-size: 12px; color:#3d3d3d">Prix TVAC : <?= $tvac; ?> €</div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;padding-top:20px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:Arial;font-size:18px;line-height:22px;text-align:left;color:#0c0c0c;"
                                                                     >
                                                                     <?php
                                                                        if($activation_code_promo == 'oui') :

                                                                            if (empty($my_code_promo) || $my_code_promo == null ){
                                                                                echo 'Pas de Code Promo inséré, contactez-nous au <a href="tel:+32476174336">+32 476 174 336</a> pour connaitre les codes promo du moment !';
                                                                            }elseif($code_promo_admin && trim($code_promo_admin) == trim($my_code_promo) ) {                                                                            
                                                                                echo "Vous avez introduit le code promo $my_code_promo valable jusqu'au $validite_code_promo_admin , veuillez contacter le <a href=\"tel:+32476174336\">+32 476 174 336</a>   afin de connaître votre remise.";
                                                                            } else {
                                                                               echo 'Pas de Code Promo inséré, contactez-nous au <a href="tel:+32476174336">+32 476 174 336</a> pour connaitre les codes promo du moment !';
                                                                            } 

                                                                        endif;
                                                                     ?>
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:710px;" width="710"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="margin:0px auto;max-width:710px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:0;text-align:center;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:710px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td height="45" style="vertical-align:top;height:45px;">
                                                                           <![endif]-->
                                                                           <div
                                                                              style="height:45px;"
                                                                              >
                                                                              &nbsp;
                                                                           </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>
   </body>
</html>

