<?php 
   $img_url = get_template_directory_uri().'/pools/email/images/avant6'; 
?>

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <title>
      </title>
      <!--[if !mso]><!-- -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!--<![endif]-->
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style type="text/css">
         #outlook a { padding:0; }
         .ReadMsgBody { width:100%; }
         .ExternalClass { width:100%; }
         .ExternalClass * { line-height:100%; }
         body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }
         table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }
         img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }
         p { display:block;margin:13px 0; }
      </style>
      <!--[if !mso]><!-->
      <style type="text/css">
         @media only screen and (max-width:480px) {
         @-ms-viewport { width:320px; }
         @viewport { width:320px; }
         }
      </style>
      <!--<![endif]-->
      <!--[if mso]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if lte mso 11]>
      <style type="text/css">
         .outlook-group-fix { width:100% !important; }
      </style>
      <![endif]-->
      <style type="text/css">
         @media only screen and (min-width:480px) {
         .mj-column-per-100 { width:100% !important; max-width: 100%; }
         .mj-column-per-50 { width:50% !important; max-width: 50%; }
         }
      </style>
      <style type="text/css">
         @media only screen and (max-width:480px) {
         table.full-width-mobile { width: 100% !important; }
         td.full-width-mobile { width: auto !important; }
         }
      </style>
      <style type="text/css">a{ text-decoration:none}
         @media only screen and (max-width:540px) {
         .btn1 a{ padding:10px 20px 10px!important}
         }
         @media only screen and (max-width:480px) {
         .blcLogo>table>tbody>tr>td{ padding:25px 0!important}
         .pad-mob>table>tbody>tr>td{ padding:25px 20px!important}
         .textContact>div{ text-align:center!important}
         .btn1 { padding-top:20px!important}
         .btn1>table{ margin:auto!important}
         .blContact>table>tbody>tr>td{ padding:0 20px 25px!important}
         .no-br br{ display:none!important}
         .blcCol>table>tbody>tr>td{ padding:25px 20px!important}
         .blcAppel>table>tbody>tr>td{ padding:20px 20px!important}
         .no-br br{ display:none!important}
         }
      </style>
   </head>
   <body style="background-color:#eeeeee;">
      <div
         style="background-color:#eeeeee;"
         >
         <!--[if mso | IE]>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="blcLogo-outlook" style="width:600px;" width="600"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  class="blcLogo" style="background:#000000;background-color:#000000;Margin:0px auto;max-width:600px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#000000;background-color:#000000;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:52px 0;text-align:center;vertical-align:top;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:600px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                                                     >
                                                                     <tbody>
                                                                        <tr>
                                                                           <td  style="width:203px;">
                                                                              <a
                                                                                 href="http://www.olsenandg.com/" target="_blank"
                                                                                 >
                                                                              <img
                                                                                 height="auto" src="<?= $img_url; ?>/logo.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:14px;" width="203"
                                                                                 />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="Margin:0px auto;max-width:600px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:0;text-align:center;vertical-align:top;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:600px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                                                     >
                                                                     <tbody>
                                                                        <tr>
                                                                           <td  style="width:600px;">
                                                                              <img
                                                                                 height="auto" src="<?= $img_url; ?>/banner.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:14px;" width="600"
                                                                                 />
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="pad-mob-outlook" style="width:600px;" width="600"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  class="pad-mob" style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#ffffff;background-color:#ffffff;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:50px 44px 31px;text-align:center;vertical-align:top;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:512px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:14px;line-height:20px;text-align:center;color:#6e6e6e;"
                                                                     >
                                                                     Bonjour <?= $nom_user; ?>,
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:14px;line-height:20px;text-align:center;color:#6e6e6e;"
                                                                     >
                                                                     Nous vous remercions pour votre intérêt pour les piscines Energy & Welness srl.
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:14px;line-height:20px;text-align:center;color:#6e6e6e;"
                                                                     >
                                                                     Olsen&G est un monde de luxe et de design, tout en ayant un prix abordable.
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:14px;line-height:20px;text-align:center;color:#6e6e6e;"
                                                                     >
                                                                     Vous trouvez en pièce jointe votre devis imprimable. <br/>Votre choix s’est porté vers le modèle <?= $numero_modele; ?> en série <?= $pose->name; ?>.
                                                                     C’est un excellent choix!
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:14px;line-height:20px;text-align:center;color:#6e6e6e;"
                                                                     >
                                                                     Vu que votre projet n’est pas pour les prochains mois, <br/>nous n’allons pas vous contacter téléphoniquement.<br/> 
                                                                     Nous vous conseillons néanmoins de visiter notre site internet : <strong><a href="http://www.olsenandg.com/" target="_blank"><font color="#6e6e6e">www.olsenandg.com</font></a></strong>, afin de parfaire votre projet <br/>et trouver des sources d’inspiration.
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;padding-bottom:5px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:14px;line-height:20px;text-align:center;color:#6e6e6e;"
                                                                     >
                                                                     Vous pouvez également aimer notre page Facebook :<br/> <a href="https://facebook.com/olsenandg" target="_blank"><font color="#6e6e6e">https://facebook.com/olsenandg</font></a> et ainsi suivre nos chantiers.
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" vertical-align="middle" class="btn1" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;"
                                                                     >
                                                                     <tr>
                                                                        <td
                                                                           align="center" bgcolor="#18adc2" role="presentation" style="border:none;border-radius:0;cursor:auto;mso-padding-alt:18px 18px 18px;background:#18adc2;" valign="middle"
                                                                           >
                                                                           <a
                                                                              href="https://www.facebook.com/olsenandg/" style="display:inline-block;background:#18adc2;color:#ffffff;font-family:arial;font-size:14px;font-weight:normal;line-height:17px;Margin:0;text-decoration:none;text-transform:none;padding:18px 18px 18px;mso-padding-alt:0px;border-radius:0;" target="_blank"
                                                                              >
                                                                           <strong>S’abonner à notre page Facebook</strong>
                                                                           </a>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="pad-mob-outlook" style="width:600px;" width="600"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  class="pad-mob" style="background:#eeeeee;background-color:#eeeeee;Margin:0px auto;max-width:600px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#eeeeee;background-color:#eeeeee;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:30px 42px 40px;text-align:center;vertical-align:top;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:516px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="left" style="font-size:0px;padding:0;padding-bottom:20px;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:14px;line-height:20px;text-align:left;color:#6e6e6e;"
                                                                     >
                                                                     Vous pouvez également prendre rendez-vous à tout moment avec votre Conseiller Energy & Welness srl, directement en ligne en fonction de vos disponibilités.
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:14px;line-height:20px;text-align:center;color:#6e6e6e;"
                                                                     >
                                                                     Si vous souhaitez évoquer votre projet piscine avec votre Conseiller ?<br/>N’hésitez pas à le contacter dès maintenant&nbsp;:
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="blContact-outlook" style="width:600px;" width="600"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  class="blContact" style="background:#eeeeee;background-color:#eeeeee;Margin:0px auto;max-width:600px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#eeeeee;background-color:#eeeeee;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:0 48px 28px;text-align:center;vertical-align:top;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:252px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="left" class="textContact" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:18px;line-height:26px;text-align:left;color:#16a5b9;"
                                                                     >
                                                                     <a href="tel:+320476174336" title="+32 (0)476 17 43 36"><font color="#16a5b9">+32 (0)476 17 43 36</font></a> <br/>
                                                                     <a href="mailto:info@olsenang.com" title="info@olsenang.com" line-height="26px"><font color="#16a5b9"><u>info@olsenang.com</u></font></a>
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:252px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-50 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="right" vertical-align="middle" class="btn1" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;"
                                                                     >
                                                                     <tr>
                                                                        <td
                                                                           align="center" bgcolor="#18adc2" role="presentation" style="border:none;border-radius:0;cursor:auto;mso-padding-alt:10px 48px 10px;background:#18adc2;" valign="middle"
                                                                           >
                                                                           <a
                                                                              href="mailto:info@olsenandg.com" style="display:inline-block;background:#18adc2;color:#ffffff;font-family:arial;font-size:14px;font-weight:normal;line-height:17px;Margin:0;text-decoration:none;text-transform:none;padding:10px 48px 10px;mso-padding-alt:0px;border-radius:0;" target="_blank"
                                                                              >
                                                                           <strong>Prendre rendez-vous <br/>avec notre conseiller</strong>
                                                                           </a>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="blContact-outlook" style="width:600px;" width="600"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  class="blContact" style="background:#eeeeee;background-color:#eeeeee;Margin:0px auto;max-width:600px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:0 20px 35px;text-align:center;vertical-align:top;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:560px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;"
                                                                     >
                                                                     <tbody>
                                                                        <tr>
                                                                           <td  style="width:500px;">
                                                                              <a
                                                                                 href="<?= $lien_video; ?>" target="_blank"
                                                                                 >
                                                                              <img
                                                                                 height="auto" src="<?= $img_url; ?>/video.jpg" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:14px;" width="500"
                                                                                 />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                            <tr>
                                                               <td
                                                                  align="center" vertical-align="middle" class="btn1" style="font-size:0px;padding:0;padding-top:35px;word-break:break-word;"
                                                                  >
                                                                  <table
                                                                     border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:separate;line-height:100%;"
                                                                     >
                                                                     <tr>
                                                                        <td
                                                                           align="center" bgcolor="#18adc2" role="presentation" style="border:none;border-radius:0;cursor:auto;mso-padding-alt:10px 48px 10px;background:#18adc2;" valign="middle"
                                                                           >
                                                                           <a
                                                                              href="<?= $e_book; ?>" style="display:inline-block;background:#18adc2;color:#ffffff;font-family:arial;font-size:14px;font-weight:normal;line-height:17px;Margin:0;text-decoration:none;text-transform:none;padding:10px 48px 10px;mso-padding-alt:0px;border-radius:0;" target="_blank"
                                                                              >
                                                                           <strong>Télécharger l'e-book</strong>
                                                                           </a>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>

         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="blcAppel-outlook" style="width:600px;" width="600"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  class="blcAppel" style="background:#18adc2;background-color:#18adc2;Margin:0px auto;max-width:600px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#18adc2;background-color:#18adc2;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:36px 20px 33px;text-align:center;vertical-align:top;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:560px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" class="no-br" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:18px;line-height:25px;text-align:center;color:#ffffff;"
                                                                     >
                                                                     Appelez votre conseiller pour connaître <br/>votre code promotion et la réduction dont vous avez droit.
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div  style="background:#eeeeee;background-color:#eeeeee;Margin:0px auto;max-width:600px;">
                     <table
                        align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#eeeeee;background-color:#eeeeee;width:100%;"
                        >
                        <tbody>
                           <tr>
                              <td
                                 style="direction:ltr;font-size:0px;padding:25px 0;text-align:center;vertical-align:top;"
                                 >
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          align="center" class="" style="vertical-align:top;width:600px;"
                                          >
                                          <![endif]-->
                                          <div
                                             class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;"
                                             >
                                             <table
                                                border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%"
                                                >
                                                <tbody>
                                                   <tr>
                                                      <td  style="vertical-align:top;padding:0;">
                                                         <table
                                                            border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%"
                                                            >
                                                            <tr>
                                                               <td
                                                                  align="center" style="font-size:0px;padding:0;word-break:break-word;"
                                                                  >
                                                                  <div
                                                                     style="font-family:arial;font-size:14px;line-height:20px;text-align:center;color:#8c8c8c;"
                                                                     >
                                                                     © 2020 Olsen&G.com
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>
   </body>
</html>

