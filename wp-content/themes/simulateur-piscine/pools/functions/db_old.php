<?php 
    function ogp_create_table()
    {
        // do NOT forget this global
        global $wpdb;
     
        // this if statement makes sure that the table doe not exist already
        if($wpdb->get_var("show tables like my_table_name") != 'my_table_name') 
        {
            $sql = "CREATE TABLE ".$wpdb->prefix."TEST (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            one_column tinytext NOT NULL,
            another_column tinytext NOT NULL,
            UNIQUE KEY id (id)
            );";
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }
    }
    // this hook will cause our creation function to run when the plugin is activated
    register_activation_hook( __FILE__, 'ogp_create_table' );

    // Insert Customer
    function insert_customer($nom, $prenom, $code_postal, $telephone, $email){
        global $wpdb;
        $customer_table = $wpdb->prefix.'pool_customers';

        $wpdb->insert($customer_table, array(
            "nom"         => $nom,
            "prenom"      => $prenom,
            "code_postal" => $code_postal,
            "telephone"   => $telephone,
            "email"       => $email
        ));
        return $wpdb->insert_id;
    }

    // Check Customer
    function check_customer($telephone, $email){
        global $wpdb;
        $customer_table = $wpdb->prefix.'pool_customers';

        $result = $wpdb->get_results(
            "SELECT * FROM $customer_table 
            WHERE telephone = '".$telephone."' 
                OR email = '".$email."' "
        ); 
        if (!empty($result)) {
            return $result[0]->id;
        }else {
            return false;
        }
    }

    // Update Customer
    function update_customer($id, $nom, $prenom){
        global $wpdb;
        $customer_table = $wpdb->prefix.'pool_customers';

        $up_cust = $wpdb->update($customer_table, 
            array(
                "nom"     => $nom,
                "prenom"  => $prenom,
            ),
            array('id' => $id)
        );
        if($up_cust){
            return true;
        }else {
            return false;
        }

    }

    /* ------- STEPS ------*/

    // Insert Customer
    function insert_customer_step($customer_id, $step, $content_step){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $content_step = json_encode( $content_step );
        $date_time = date('Y-m-d H:i:s');

        $wpdb->insert($customer_step_table, array(
            "customer_id"  => $customer_id,
            "step"         => $step,
            "content_step" => $content_step,
            "last_modify"  => $date_time
        ));
        return $wpdb->insert_id;
    }

    // Get Customer Step
    function get_customer_info($customer_id){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';

        $result = $wpdb->get_results(
            "SELECT * FROM $customer_step_table 
            WHERE customer_id =".$customer_id
        ); 
        if (!empty($result)) {
            return $result;
        }else {
            return false;
        }
    }

    // Get all Customer Step
    function get_customer_unsuccessful_step(){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $customer_table = $wpdb->prefix.'pool_customers';

        $result = $wpdb->get_results(
            "SELECT * FROM $customer_step_table as s 
            INNER JOIN $customer_table as c 
                ON c.id = s.customer_id 
            WHERE step < 10 AND remind < 3"
        ); 
        if (!empty($result)) {
            return $result;
        }else {
            return false;
        }
    }

    function get_current_page_id(){
        global $post;
        return $post->ID;
    }

    // Update Customer Info
    function update_customer_step($customer_id, $step, $array_info, $image=null, $finished=0, $pool_time=null){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $content_step = json_encode( $array_info );

        $date_time = date('Y-m-d H:i:s');

        $poolData = array();

        $poolData = array(                
            "step"         => $step,
            "content_step" => $content_step,
            "last_modify"  => $date_time,
            "img"          => $image,               
            "code_promo"   => $code_promo,                
            "finished"     => $finished,                
            "remind"       => 0,                
        );

        if($pool_time != null){
            $poolData['pool_time'] = $pool_time;
        }        

        $up_cust = $wpdb->update($customer_step_table, 
            $poolData ,
            array("customer_id"  => $customer_id )
        );
        if($up_cust){
            return true;
        }else {
            return false;
        }
    }

    // Update Code Promo
    function update_customer_code_promo($customer_id, $code_promo=null){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $content_step = json_encode( $array_info );

        $date_time = date('Y-m-d H:i:s');

        $up_cust = $wpdb->update($customer_step_table, 
            array( 
                "last_modify" => $date_time,
                "code_promo" => $code_promo                
            ),
            array("customer_id"  => $customer_id )
        );
        if($up_cust){
            return true;
        }else {
            return false;
        }
    }

    // Get Customer Step
    function get_customer_code_promo($customer_id){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';

        $result = $wpdb->get_results(
            "SELECT code_promo FROM $customer_step_table 
            WHERE customer_id =".$customer_id
        ); 
        if (!empty($result)) {
            return $result[0]->code_promo;
        }else {
            return false;
        }
    }


    // Get Item Customer Info
    function get_item_customer_info($customer_id, $type){
        $customer_step_info = get_customer_info($customer_id);
        if (!$type) {
            $res = 'Aucun type de donnée spécifié';
        }else {
            $res = $customer_step_info[0]->$type;
        }
        return $res;
    }

    // Get Customer Step
    function get_user_pool_data($id){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_customers';

        $result = $wpdb->get_results(
            "SELECT * FROM $customer_step_table 
            WHERE id =".$id
        ); 
        if (!empty($result)) {
            return $result;
        }else {
            return false;
        }
    }

    // Link Detail
    function link_detail($pool_id){
        return 'edit.php?post_type=pool&page=pool_simulation_detail&pool_id='.$pool_id;
    }

    // Link Detail
    function link_export(){
        return 'edit.php?post_type=pool&pool_report=leads&_wpnonce='.wp_create_nonce('pool_lead_csv').'';
    }

    // Upadte 
    function update_customer_status($customer_id){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $customer_info = get_customer_info($customer_id);

        if(!empty($customer_info)){
            $last_remind = $customer_info[0]->remind + 1;
            $date_time = date('Y-m-d H:i:s');

            $up_cust = $wpdb->update($customer_step_table, 
                array(                
                    "last_modify" => $date_time,
                    "remind" => $last_remind
                ),
                array("customer_id"  => $customer_id )
            );
            if($up_cust){
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    } 


    

?>