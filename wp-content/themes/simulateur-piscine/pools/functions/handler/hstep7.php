<?php 
    $step = 7;
    $data_step = sanitize_content_step(get_item_customer_info($current_user_id, 'content_step'));

    if (isset($eclairage_id) && $eclairage_id != null) {

        $eclairage_format = $_POST['eclairage_format'.$eclairage_id];
        $name_eclairage   = $_POST['name_eclairage'.$eclairage_id];
        $prix_eclairage   = $_POST['prix_eclairage'.$eclairage_id];
        $prix_par_unite   = $_POST['prix_par_unite'.$eclairage_id];
        $nb_unite         = $_POST['nb_unite'.$eclairage_id];


        if( $eclairage_format == 'perso'){
            $prix_eclairage = $prix_par_unite;
        }else {
            $prix_eclairage = $prix_eclairage;
        }

        $content_step_array      = array( 
            'eclairage_id'     => $eclairage_id,
            'eclairage_format' => $eclairage_format,
            'prix_eclairage'   => $prix_eclairage,
            'nb_unite'         => $nb_unite,
        );

        $data_step->eclairage_id     = $eclairage_id;
        $data_step->eclairage_format = $eclairage_format;
        $data_step->nb_unite         = $nb_unite;
        $data_step->eclairage_prix   = $prix_eclairage;


        $content_step_array = $data_step;
        update_customer_step($current_user_id, $step, $content_step_array);  
    }
    elseif($vstep && $uid)
    {
        $data_step = sanitize_content_step(get_item_customer_info($uid, 'content_step'));
        $content_step_array = array(
            'modele'         => $data_step->modele,
            'type_pose'      => $data_step->type_pose,
            'choix_couleur'  => $data_step->choix_couleur,
            'choix_margelle' => $data_step->choix_margelle,
            
            'eclairage_id'     => $data_step->eclairage_id,
            'eclairage_format' => $data_step->eclairage_format,
            'eclairage_prix'   => $data_step->eclairage_prix,
            'nb_unite'         => $data_step->nb_unite,
        );
        update_customer_step($uid, $vstep, $content_step_array); 
    }

    // Get Data
    $modele_id    = $data_step->modele;
    $pose_id      = $data_step->type_pose;
    $couleur_id   = $data_step->choix_couleur;
    $margelle_id  = $data_step->choix_margelle;

    $eclairage_id     = $data_step->eclairage_id;
    $eclairage_format = $data_step->eclairage_format;
    $eclairage_prix   = $data_step->eclairage_prix;
    $nb_unite         = $data_step->nb_unite;

    if ($eclairage_format == 'perso') {
        $ecl_price = $nb_unite * $eclairage_prix;
    }else {
        $ecl_price = $eclairage_prix;
    }
    
    // View
    $view = locate_template( 'pools/step/step'.$step.'.php', false, false );
    include( $view ); 