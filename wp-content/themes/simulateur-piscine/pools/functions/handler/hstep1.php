<?php

if (!empty($nom) && !empty($prenom) && !empty($adresse) && !empty($ville) && !empty($code_postal) && !empty($telephone) && !empty($email)) { 	
    get_template_part('pools/step/step'.$step); 
}
elseif($vstep && $uid)
{
	$data_step = sanitize_content_step(get_item_customer_info($uid, 'content_step'));	

	$content_step_array = array();
	update_customer_step($uid, 2, $content_step_array, null, 0, null); 
	get_template_part('pools/step/step'.$vstep);
}
else {
	get_template_part('pools/step/step1');
}