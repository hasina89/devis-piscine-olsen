<?php 
	$step = 5;
    $data_step = sanitize_content_step(get_item_customer_info($current_user_id, 'content_step'));

    if (isset($choix_couleur) && $choix_couleur != null) {  
        $content_step_array = array( 'choix_couleur' => $choix_couleur );
        $data_step->choix_couleur = $choix_couleur;
        $content_step_array = $data_step;
        update_customer_step($current_user_id, $step, $content_step_array);  
    }
    elseif($vstep && $uid)
    {
        $data_step = sanitize_content_step(get_item_customer_info($uid, 'content_step'));
        $content_step_array = array(
            'modele'        => $data_step->modele,
            'type_pose'     => $data_step->type_pose,
            'choix_couleur' => $data_step->choix_couleur
        );
        update_customer_step($uid, $vstep, $content_step_array); 
    }

    // Get Data
    $modele_id = $data_step->modele;
    $pose_id = $data_step->type_pose;
    $couleur_id = $data_step->choix_couleur;
	
	// View
	$view = locate_template( 'pools/step/step'.$step.'.php', false, false );
	include( $view ); 