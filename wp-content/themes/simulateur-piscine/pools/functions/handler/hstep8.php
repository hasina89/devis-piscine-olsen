<?php 
    $step = 8;
    $data_step               = sanitize_content_step(get_item_customer_info($current_user_id, 'content_step'));

    if (isset($local_teknik) && $local_teknik != null) {
        $content_step_array      = array( 'local_teknik' => $local_teknik );
        $data_step->local_teknik = $local_teknik;
        $content_step_array      = $data_step;
        update_customer_step($current_user_id, $step, $content_step_array);  
    }
    elseif($vstep && $uid)
    {
        $data_step = sanitize_content_step(get_item_customer_info($uid, 'content_step'));
        $content_step_array = array(
            'modele'         => $data_step->modele,
            'type_pose'      => $data_step->type_pose,
            'choix_couleur'  => $data_step->choix_couleur,
            'choix_margelle' => $data_step->choix_margelle,

            'eclairage_id'     => $data_step->eclairage_id,
            'eclairage_format' => $data_step->eclairage_format,
            'eclairage_prix'   => $data_step->eclairage_prix,
            'nb_unite'         => $data_step->nb_unite,

            'local_teknik'   => $data_step->local_teknik
        );
        update_customer_step($uid, $vstep, $content_step_array); 
    }

    // Get Data
    $modele_id          = $data_step->modele;
    $pose_id            = $data_step->type_pose;
    $couleur_id         = $data_step->choix_couleur;
    $margelle_id        = $data_step->choix_margelle;
    $local_technique_id = $data_step->local_teknik;

    $eclairage_id     = $data_step->eclairage_id;
    $eclairage_format = $data_step->eclairage_format;
    $eclairage_prix   = $data_step->eclairage_prix;
    $nb_unite         = $data_step->nb_unite;
    
    // View
    $view = locate_template( 'pools/step/step'.$step.'.php', false, false );
    include( $view ); 