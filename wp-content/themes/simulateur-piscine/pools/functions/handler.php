<?php

$vstep = isset($_GET['vstep']) ? (int)$_GET['vstep'] : null;
$uid = isset($_GET['uid']) ? (int)$_GET['uid'] : null;

extract($_POST);

global $cur_simulation_id;

if ($vstep && $uid) {

	$id_pool = sanitize_content_step(get_item_customer_info($uid, 'id'));

	$last_step = sanitize_content_step(get_item_customer_info($uid, 'step'));

	if($last_step >= $vstep ){
		include_once "handler/hstep".$vstep.".php";
	}else {
		get_template_part('pools/step/step1');
	}

}else {

	/* Start Simulator */
    if (isset($_POST['cur_step'])) {        
        global $current_user_id;

        if ($cur_step == 0){
        	$step = 1;
        }elseif ($cur_step == 1) {
	    		// Clean & reset Session
					if ( isset( $_SESSION['current_user_id'] ) ) {
						unset( $_SESSION['current_user_id'] );
					}

	    		// echo 'Step 2';
	      	$exist_customer = check_customer($telephone, $email);
	        
			    if ( $exist_customer ) {
			    	// Update Data

			    	update_customer($exist_customer, $nom, $prenom);
			    	$UnFinished_step = get_customer_simulations($exist_customer, 0);		    	

					// Current Customer
				    $id_customer = $exist_customer;
				
			        if( empty($UnFinished_step) ){
			        	// Register Customer
				        $content_step_array = array();
				        $step = $next_step;
				        $id_pool = insert_customer_step($id_customer, $step, $content_step_array);  
			        }else {
					    	// Current_step
					    	$step = $UnFinished_step[0]->step;		
					    	$id_pool = $UnFinished_step[0]->id;					    	
			        }
			        

			    }else {
			        // Register Customer
			        $id_customer = insert_customer($nom, $prenom, $code_postal, $telephone, $email);
			        $content_step_array = array();

			        // Current_step
			        $step = $next_step;
			        $id_pool = insert_customer_step($id_customer, $step, $content_step_array);
			    }
			    update_pool_time_step($id_customer, $pool_time);
			    update_prix_lead_step($id_customer, $pool_time);
					update_customer_address($id_customer, $code_postal, $province);

			    /* Store User ID */
		    	$_SESSION['current_user_id'] = $id_customer;
		    	$current_user_id = $_SESSION['current_user_id'];	    	


		}else {
			$current_user_id = $_SESSION['current_user_id'];


			$id_pool = get_item_customer_info($current_user_id, 'id');
			if(!$id_pool){
		    	$id_pool = get_customer_simulations($current_user_id, 1, true)[0]->id;
			}

			$step = get_item_customer_info($current_user_id, 'step');
			if ($step < $next_step) {
				$step = $next_step;
			}

		}

		/* Store Simulation ID */
    	$_SESSION['cur_simulation_id'] = $id_pool;
    	$cur_simulation_id = $_SESSION['cur_simulation_id'];
		
	    if($step){
	    	include_once "handler/hstep".$step.".php";
	    }

    }else {
        $step = 0;
    	get_template_part('pools/step/step'.$step);
    }

}

		
