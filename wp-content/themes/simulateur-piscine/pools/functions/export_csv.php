<?php

class CSVExport {

    /** Constructor */
    public function __construct() {
        if (isset($_GET['pool_report'])) {

          $csv = $this->generate_xls();
          header("Pragma: public");
          header("Expires: 0");
          header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
          header("Cache-Control: private", false);
          header("Content-Type: application/octet-stream");
          header("Content-Disposition: attachment; filename=\"report.csv\";");
          header("Content-Transfer-Encoding: binary");

          echo $csv;
          exit;
        }

        // Add extra menu items for admins
        // add_action('admin_menu', array($this, 'admin_menu'));
    }

    /**
    * Add extra menu items for admins
    */
    /*public function admin_menu() {
        add_submenu_page( 
            '',
            'Export Simulation',
            'Export Simulation',
            'manage_options',
            'pool_simulation_list',
            ''
        );
    }*/

    // Get Customer Step
    public function get_leads_data(){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $customer_table = $wpdb->prefix.'pool_customers';

        $result = $wpdb->get_results(
            "SELECT * FROM $customer_step_table as s 
            INNER JOIN $customer_table as c 
                ON c.id = s.customer_id "
        ); 
        
        if (!empty($result)) {
            return $result;
        }else {
            return false;
        }
    }

    public function get_leads_data_by_province_asc(){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $customer_table = $wpdb->prefix.'pool_customers';

        $result = $wpdb->get_results(
            "SELECT * FROM $customer_step_table as s 
            INNER JOIN $customer_table as c 
                ON c.id = s.customer_id  
                ORDER BY IF(s.province_step <> '',0,1), s.province_step ASC"
        ); 
        if (!empty($result)) {
            return $result;
        }else {
            return false;
        }
    }
    public function get_leads_data_by_province_desc(){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $customer_table = $wpdb->prefix.'pool_customers';

        $result = $wpdb->get_results(
            "SELECT * FROM $customer_step_table as s 
            INNER JOIN $customer_table as c 
                ON c.id = s.customer_id  
                ORDER BY IF(s.province_step <> '',0,1), s.province_step DESC"
        ); 
        if (!empty($result)) {
            return $result;
        }else {
            return false;
        }
    }

    public function get_leads_data_by_province($province){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $customer_table = $wpdb->prefix.'pool_customers';

        $result = $wpdb->get_results(
            "SELECT * , s.id as simulation_id  FROM $customer_step_table as s 
            INNER JOIN $customer_table as c 
                ON c.id = s.customer_id 
                WHERE s.province_step = '$province'
                ORDER BY last_modify DESC"
        ); 

        if (!empty($result)) {
            return $result;
        }else {
            return false;
        }
    }

    public function get_leads_data_by_prix_asc(){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $customer_table = $wpdb->prefix.'pool_customers';

        $result = $wpdb->get_results(
            "SELECT * FROM $customer_step_table as s 
            INNER JOIN $customer_table as c 
                ON c.id = s.customer_id 
                ORDER BY c.prix_lead ASC"
        ); 
        if (!empty($result)) {
            return $result;
        }else {
            return false;
        }
    }
    public function get_leads_data_by_prix_desc(){
        global $wpdb;
        $customer_step_table = $wpdb->prefix.'pool_step';
        $customer_table = $wpdb->prefix.'pool_customers';

        $result = $wpdb->get_results(
            "SELECT * FROM $customer_step_table as s 
            INNER JOIN $customer_table as c 
                ON c.id = s.customer_id  
                ORDER BY c.prix_lead DESC"
        ); 
        if (!empty($result)) {
            return $result;
        }else {
            return false;
        }
    }

    /**
    * Converting data to CSV
    */
    public function generate_csv() {
        if( !current_user_can( 'manage_options' ) ){ return false; }
        if( !is_admin() ){ return false; }

        // Nonce Check
        $nonce = isset( $_GET['_wpnonce'] ) ? $_GET['_wpnonce'] : '';
        if ( ! wp_verify_nonce( $nonce, 'pool_lead_csv' ) ) {
            die( 'Security check error' );
        }
        
        ob_start();

            $filename = 'Leads_devis_piscine_'.date('d-m-Y_H-i-s').'.csv';
                        
            $header_row = array('Nom', 'Prénom', 'Téléphone', 'Email', 'Code Postal', 'Province','Délai','Prix du lead','Etape', 'Nombre de Rappel', 'Date' );

            $data_rows = array();
            $info_users = $this->get_leads_data();
            foreach ( $info_users as $info_user ) {
                $row = array(
                    $info_user->nom,
                    $info_user->prenom,
                    $info_user->telephone,
                    $info_user->email,
                    $info_user->code_postal,
                    $info_user->province,
                    $info_user->province,
                    $info_user->prix_lead,
                    $info_user->step,
                    $info_user->remind,
                    $info_user->last_modify
                );
                $data_rows[] = $row;
            }
            
            $fh = @fopen( 'php://output', 'w' );
            fprintf( $fh, chr(0xEF) . chr(0xBB) . chr(0xBF) );
            header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
            header( 'Content-Description: File Transfer' );
            header( 'Content-type: text/csv' );
            header( "Content-Disposition: attachment; filename={$filename}" );
            header( 'Expires: 0' );
            header( 'Pragma: public' );
            fputcsv( $fh, $header_row );
            foreach ( $data_rows as $data_row ) {
                fputcsv( $fh, $data_row );
            }
            fclose( $fh );
        
        ob_end_flush();
        
        die();
    }

    /**
     * Converting data to XLS
     */
    public function generate_xls() {
        if( !current_user_can( 'manage_options' ) ){ return false; }
        if( !is_admin() ){ return false; }

        // Nonce Check
        $nonce = isset( $_GET['_wpnonce'] ) ? $_GET['_wpnonce'] : '';
        if ( ! wp_verify_nonce( $nonce, 'pool_lead_csv' ) ) {
            die( 'Security check error' );
        }

        ob_start();

        $filename = 'Leads_devis_piscine_'.date('d-m-Y_H-i-s').'.xls';

        $header_row = array('Nom', 'Prénom', 'Téléphone', 'Email', 'Code Postal', 'Province','Délai', 'Prix du lead','Etape', 'Nombre de Rappel', 'Date' );

        $data_rows = array();
        if ( isset($_GET['orderby']) && !empty($_GET['orderby']) && isset($_GET['order']) && !empty(['order']) ){
            if ( strip_tags($_GET['orderby']) == 'prix' && strip_tags($_GET['order']) == "asc" ) {
                $info_users = $this->get_leads_data_by_prix_asc();
            }else if ( strip_tags($_GET['orderby']) == 'prix' && strip_tags($_GET['order']) == "desc" ) {
                $info_users = $this->get_leads_data_by_prix_desc();
            }else if ( strip_tags($_GET['orderby']) == 'province' && strip_tags($_GET['order']) == "asc" ) {
                $info_users = $this->get_leads_data_by_province_asc();
            }else if ( strip_tags($_GET['orderby']) == 'province' && strip_tags($_GET['order']) == "desc" ) {
                $info_users = $this->get_leads_data_by_prix_desc();
            }
        }else if( isset($_GET['province']) && !empty($_GET['province']) ){
            $province = strip_tags($_GET['province']);
            $info_users = $this->get_leads_data_by_province($province);
        }else{
            $info_users = $this->get_leads_data();
        }        
        foreach ( $info_users as $info_user ) {
            $cp = $info_user->code_postal_step != '0' ? $info_user->code_postal_step : $info_user->code_postal;
            $prov = $info_user->province_step != '' ? $info_user->province_step : $info_user->province;
            $prix = $info_user->prix_lead_step != '' ? $info_user->prix_lead_step : $info_user->prix_lead;
            $row = array(
                $info_user->nom,
                $info_user->prenom,
                $info_user->telephone,
                $info_user->email,
                $cp,
                $prov,
                $info_user->pool_time,
                $prix,
                $info_user->step,
                $info_user->remind,
                $info_user->last_modify
            );
            $data_rows[] = $row;
        }


        echo pack("CCC",0xef,0xbb,0xbf);
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: application/vnd.ms-excel; charset=UTF-8' );
        header( "Content-Disposition: attachment; filename={$filename}" );
        header( 'Expires: 0' );
        header( 'Pragma: public' );

        echo '<table style="width:100%" border="1">';
        echo '<tr>';

        // print header
        foreach($header_row as $h){
            echo '<th>'.$h.'</th>';
        }
        echo '</tr>';

        // print data
        foreach ( $data_rows as $row ) {
            echo '<tr>';
            foreach( $row as $d){
                echo '<td>'.$d.'</td>';
            }
            echo '</tr>';
        }

        echo '</table>';

        ob_end_flush();

        die();
    }

}

// Instantiate a singleton of this plugin
$csvExport = new CSVExport();