<?php

add_action( 'wp_ajax_upload_ajax', 'upload_ajax' );
add_action( 'wp_ajax_nopriv_upload_ajax', 'upload_ajax' );


function upload_ajax(){
    include_once ABSPATH . 'wp-admin/includes/media.php';
    include_once ABSPATH . 'wp-admin/includes/file.php';
    include_once ABSPATH . 'wp-admin/includes/image.php';
    require_once (ABSPATH . 'wp-includes/pluggable.php');

    if (isset ( $_POST ) && isset($_FILES) && !empty($_FILES['file'])) {
        parse_str($_POST['fData'], $Data);
        extract($Data);
        // die();

        if (! function_exists ( 'wp_handle_upload' )){
            require_once (ABSPATH . 'wp-admin/includes/file.php');
        }
        $uploadedfile = $_FILES ['file'];
        if (! empty ( $uploadedfile ['name'] )) {
            $upload_overrides = array (
                'test_form' => false 
            );
            $movefile = wp_handle_upload ( $uploadedfile, $upload_overrides );

            $response = array();
            if ($movefile) {
                $response['code'] = 1;
                $response['url'] = $movefile['url'];

                // $data_step = sanitize_content_step(get_item_customer_info($id_user, 'content_step'));
                $data_step = sanitize_content_step(get_item_step_data($cur_simulation_id, 'content_step'));

                $content_step_array = $data_step;                

                update_customer_img($cur_simulation_id, $response['url'] );

            } else {
                $response['code'] = 0;
                $response['msg'] = "Possible file upload attack!";
            }
        } else {
            $response['code'] = 0;
            $response['msg'] = "Veuillez choisir une image";
        }

        echo json_encode( $response);

        die();

    }
}