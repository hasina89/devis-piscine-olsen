<?php 

add_action( 'wp_ajax_pool', 'pool' );
add_action( 'wp_ajax_nopriv_pool', 'pool' );
function pool(){
    if (isset($_POST) && !empty($_POST)) :
        parse_str($_POST['data'], $Data);
        extract($Data);

        // For Each simulation in BO
        if(isset($each_simulation_id) && $each_simulation_id != null ){

            $pdf = locate_template( 'pools/pdf/each_simulation.php', false, false );
            include($pdf);

        }else {

            $pdf = locate_template( 'pools/pdf/index.php', false, false );
            include($pdf);
            $provinces = get_field('emails_responsable','option');
            $provinces = $provinces['le_responsable'];
            $responsable = '';
            foreach ( $provinces as $pro ){
                if ($pro['province'] == $province_user ) $responsable = $pro['email'];
            }

            ob_start();
                include("email/client.php");
            $body_mail = ob_get_clean();


            // Mail selon Piscine (Pool_time)
            ob_start();

                if($data_step_pool_time == 'Après 6 mois'){
                    include("email/dans6.php");
                }
                elseif ($data_step_pool_time == 'Dans les 6 mois' || $data_step_pool_time == 'Dans 6 mois') {
                    include("email/avant6.php");
                }
                elseif ($data_step_pool_time == 'Urgent') {
                    include("email/urgent.php");
                }
                
            $body_mail_pool_time = ob_get_clean();
                 
            // Function to change sender name
            function wpb_sender_name( $original_email_from ) {
                return 'Piscine Energy & Welness srl';
            }
            add_filter( 'wp_mail_from_name', 'wpb_sender_name' );
            
            $destinataire = $email_user;
            $cc_email = get_field('cc_email', 'option');
            $headers = array("From: $sender_email", 'Content-Type: text/html; charset=UTF-8; MIME-Version: 1.0');
            $headers[] = "Cc: Propriétaire Site <$sender_email>";
            
            if($cc_email) {
                $headers[] = "Cc: Modérateur <$cc_email>";                
            }
            if ($responsable){
                $headers[] = "Cc: Commercial <$responsable>";
            }

            $data = array();
            if(@wp_mail( $destinataire, 'Récapitulatif de votre simulation', $body_mail, $headers, $attachments )){
                /* ovaina ny header: ny client ihany no mila an'ito mail io */
                $headers = array("From: $sender_email", 'Content-Type: text/html; charset=UTF-8; MIME-Version: 1.0');
                $headers[] = "Cc: Propriétaire Site <$sender_email>";
                if(@wp_mail( $destinataire, 'Energy & Welness srl vous remercie', $body_mail_pool_time, $headers )) {

                    $data['result'] = 1; 
                    $data['msg'] = '<p class="success">Un email Récapitulatif de votre simulation vous a été envoyé.</p>';

                }
            }else {
                $data['result'] = 0;
                $data['msg'] = '<p class="error">Une erreur s\'est produite, veuillez reconfirmer SVP.</p>';
            }
            
            echo json_encode($data);
            die();

        }          
    	
    endif;
}

add_action( 'wp_ajax_tri_par_province', 'tri_par_province' );

function tri_par_province(){
    
    if ( strip_tags( $_POST['province'] ) == '' ){
        echo "empty";
        die();
    }
    $return = [];
    $html = '';
    $province = strip_tags( $_POST['province'] );

    if ($province == 'all' ){ 
        $users = get_leads();
    }else{
        $users = get_leads_province($province);
    }    

    if (!$users){
        $total = 0;
        $html = "<tr><td colspan='11' class='no-result'>Aucun résultat</td></tr>";
    }else{
        $total = count($users);
        foreach ($users as $user) :
            $pool_id   = $user->simulation_id;
            $lead_id   = $user->customer_id;
            $nom       = $user->nom .' '.$user->prenom;
            $email     = $user->email;
            if ( $user->code_postal_step != '0' ){
                $codepost  = $user->code_postal_step;
            }else{
                $codepost  = $user->code_postal;
            }
            if ( $user->province_step != '' ){
                $province  = $user->province_step;
            }else{
                $province  = $user->province;
            }
            
            $step      = $user->step;
            $telephone = $user->telephone;
            $finished  = $user->finished;
            $pool_time = $user->pool_time;
            if ( $user->prix_lead_step != ''){
                $prix_lead = $user->prix_lead_step;
            }else{
                $prix_lead = $user->prix_lead;
            }
            $remind    = $user->remind;
            $date      = $user->last_modify;

            if ($finished == 1) {
                $txt_finished = 'Terminé';
                $class_finished = 'finished';
            }else {
                $txt_finished = 'En cours';
                $class_finished = 'unfinished';
            }

            $html .= '
                <tr>
                    <td>'.$nom.'<br><a href="'.link_detail( $lead_id, $pool_id ).'">Voir détail</a></td>
                    <td>'.$email.'</td>
                    <td>'.$codepost.'</td>
                    <td>'.$province.'</td>
                    <td>'.$telephone.'</td>
                    <td>'.$step.'</td>
                    <td class="'. $class_finished .'">'.$txt_finished.'</td>
                    <td>'.$pool_time.'</td>
                    <td>'.$prix_lead.'</td>
                    <td>'.$remind.'</td>
                    <td>'.$date.'</td>
                </tr>
            ';

        endforeach;
    }
    $return['total'] = $total;
    $return['html'] = $html;
    $return['province'] = $province;
    echo json_encode($return);

    die();
}