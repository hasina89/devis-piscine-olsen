<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage somaco
 */
?>

<?php 
    if (is_archive() ) :
        get_template_part( 'template-parts/content', get_post_type() );
    endif; 
?>