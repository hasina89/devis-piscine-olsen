<?php 
    $email          = get_theme_mod('email_master', '');
    $telephone1     = get_theme_mod('telephone1', '');
    $adresse        = get_theme_mod('adresse', '');
?>
<aside class="aside">
    <div class="blocSince">
        <img src="<?= get_template_directory_uri().'/images/icon-since.svg'; ?>" />
        <p class="titre">Le collège est ouvert depuis<br/> 2 ans et accueille 5 collégiens.</p>
    </div>
    <div class="blocInsc">
        <h3 class="titre">Inscription en cours d’année </h3>
        <p>Possible si place disponible. Contacter la directrice: </p>
        <a href="mailto:<?= $email; ?>" class="mail" title="<?= $email; ?>"><?= $email; ?></a>
        <a href="tel:<?= str_replace(' ', '', $telephone1); ?>" class="call" title="<?= $telephone1; ?>"><?= $telephone1; ?></a>
    </div>
    <div class="blocSouti">
        <h3 class="titre">Soutien scolaire </h3>
        <p>Cours de soutien scolaire le mercredi après-midi avec les enseignantes de l'école et le matériel montessori en petits groupes pour le nombre de séances souhaitées. <br>Prendre contact par mail avec la directrice.</p>
    </div>
</aside>