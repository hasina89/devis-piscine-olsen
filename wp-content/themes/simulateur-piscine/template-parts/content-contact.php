<div class="banner_title">
	<img src="<?php echo get_template_directory_uri () ; ?>/img/bg_contact.png" alt="">
	<h2 class="title_single"><?php the_title(); ?></h2>
	<div class="bread_crumb">
		<ul>
			<li><a href="<?php echo esc_url( home_url( '/' )); ?>">Accueil</a></li>
			<li><span>Nous contacter</span></li>
		</ul>
	</div>
</div>
<div class="adress_box">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 all_adress_contact">
				<div class="row">
					<div class="col-md-3 col-xs-6 item_box_adress">
						<div class="inner_box_adress">
							<h3>Adresse</h3>
							<p>52, rue de la Bourse, <br> 75000 Paris,</p>
						</div>
					</div>
					<div class="col-md-3 col-xs-6 item_box_adress">
						<div class="inner_box_adress">
							<h3>Téléphone</h3>
							<p>( +33 )  01 54 74 25 21  <br>01 54 74 25 21 </p>
						</div>
					</div>
					<div class="col-md-3 col-xs-6 item_box_adress">
						<div class="inner_box_adress">
							<h3>email</h3>
							<p>contact (at) adoredieu.com <br>comm (at) adoredieu.com</p>
						</div>
					</div>
					<div class="col-md-3 col-xs-6 item_box_adress">
						<div class="inner_box_adress">
							<h3>disponibilité</h3>
							<p>Lundi - Vendredi : 9h00 à 18h00 <br>Samedi  : 9h00 à 18h00</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="the_mmap">
	<div id="map"></div>
</div>
<div class="form_contact">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				
				<?php the_content(); ?>

				<?php //echo do_shortcode('[contact-form-7 id="29534" title="Formulaire de contact 1"]'); ?>

				<?php /* <form action="" class="contact_form">
					<div class="form-group half_width">
						<input type="text" class="form-control" placeholder="Nom">
					</div>
					<div class="form-group half_width">
						<input type="email" class="form-control" placeholder="Email">
					</div>
					<div class="form-group half_width">
						<input type="text" class="form-control" placeholder="Tél">
					</div>
					<div class="form-group half_width">
						<input type="text" class="form-control" placeholder="Sujet">
					</div>
					<div class="form-group">
						<textarea class="form-control" placeholder="Message"></textarea>
					</div>
					<div class="form-group btn_send">
						<button type="submit" class="btn btn-warning"><span>Envoyer le message</span></button>
					</div>
				</form> */ ?>
			</div>
		</div>
	</div>
</div>

<!-- 

<div class="form-group half_width">
	[text* text-185 class:form-control placeholder "Nom"]
</div>
<div class="form-group half_width">
	[email* email-670 class:form-control placeholder "E-mail"]
</div>
<div class="form-group half_width">
	[tel* tel-888 class:form-control placeholder "Téléphone"]
</div>
<div class="form-group half_width">
	[text* text-462 class:form-control placeholder "Sujet"]
</div>
<div class="form-group">
	[textarea* textarea-751 class:form-control placeholder "Message"]
</div>
<div class="form-group btn_send">
	[submit class:btn class:btn-warning "Envoyer le message"]
</div>

 -->