<?php

add_action('init', 'go_startSession', 1);
function go_startSession() {
    if(!session_id()) {
        session_start();
    }
}

add_theme_support( 'post-thumbnails' );

define('CSS_URL', get_template_directory_uri() . '/assets/css/');
define('JS_URL', get_template_directory_uri() . '/assets/js/');
define('IMG_URL', get_template_directory_uri() . '/assets/images/');
define('THEME_URL', get_template_directory_uri() . '/');


// load front js and css
function ad_load_front_scripts() { 
    
    if (is_front_page()) {    
        // wp_enqueue_style( 'slider-pro', THEME_URL . 'assets/libs/slider-pro/slider-pro.css' );
    }
    wp_enqueue_style( 'mCustomScrollbar', CSS_URL . 'jquery.mCustomScrollbar.min.css' );
    wp_enqueue_style( 'slick', CSS_URL . 'slick.css' );
    wp_enqueue_style( 'reset', CSS_URL . 'reset.css' );

    
    // <!-- Add Button helper (this is optional) -->
    wp_register_script( 'jquery_min', JS_URL . 'jquery.min.js', array(), true, false, true );      
    wp_register_script( 'jquery_migrate', JS_URL . 'jquery-migrate.min.js', array(), true, false, true );      
    wp_register_script( 'slick', JS_URL . 'slick.min.js', array(), true, false, true );      
    wp_register_script( 'jvalidate', JS_URL . 'jquery.validate.min.js', array(), true, false, true );   
    wp_register_script( 'validate', JS_URL . 'validate.js', array(), true, false, true );   
    wp_register_script( 'mCustomScrollbar', JS_URL . 'jquery.mCustomScrollbar.min.js', array(), true, false, true );   
    wp_register_script( 'step', JS_URL . 'step.js', array(), true, false, true );   
    wp_register_script( 'custom', JS_URL . 'custom.js', array(), true, false, true );   
    wp_register_script( 'pool', JS_URL.'pool_ajax.js', array(), true, false, true );   
    wp_register_script( 'upload', JS_URL.'upload.js', array(), true, false, true );   
    wp_register_script( 'upload_ajax', JS_URL.'upload_ajax.js', array(), true, false, true );   
    wp_register_script( 'code_promo_ajax', JS_URL.'code_promo_ajax.js', array(), true, false, true );   
        

    wp_enqueue_script( 'jquery_min', false, array(), false, true );
    wp_enqueue_script( 'jquery_migrate', false, array(), false, true );
    wp_enqueue_script( 'slick', false, array(), false, true );
    wp_enqueue_script( 'jvalidate', false, array(), false, true );
    wp_enqueue_script( 'validate', false, array(), false, true );
    wp_enqueue_script( 'mCustomScrollbar', false, array(), false, true );
    wp_enqueue_script( 'custom', false, array(), false, true );
   
    wp_enqueue_script( 'step', false, array(), false, true );
    wp_enqueue_script( 'pool', false, array(), false, true );
    wp_enqueue_script( 'upload', false, array(), false, true );
    wp_enqueue_script( 'upload_ajax', false, array(), false, true );
    wp_enqueue_script( 'code_promo_ajax', false, array(), false, true );
    // pass Ajax Url to script.js
    wp_localize_script('pool', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    wp_localize_script('upload_image', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    wp_localize_script('code_promo_ajax', 'ajaxurl', admin_url( 'admin-ajax.php' ) );


}
add_action( 'wp_enqueue_scripts', 'ad_load_front_scripts' );

// Enqueue File in Admin
function ad_load_bo_scripts( $hook ) {
     wp_enqueue_style( 'sort', CSS_URL . 'sort.css' );

    wp_register_script( 'sorttable', JS_URL.'jquery.tablesorter.min.js', array(), true, false, true ); 
    wp_register_script( 'sorttable-widget', JS_URL.'jquery.tablesorter.widgets.js', array(), true, false, true ); 
    wp_register_script( 'pool', JS_URL.'pool_ajax.js', array(), true, false, true );   
    wp_enqueue_script( 'sorttable', false, array(), false, true );  
    wp_enqueue_script( 'sorttable-widget', false, array(), false, true );  
    wp_enqueue_script( 'pool', false, array(), false, true ); 
     
}
add_action( 'admin_enqueue_scripts', 'ad_load_bo_scripts' );

// Handler ajax Pool Photo
require_once get_template_directory() . '/pools/functions/upload_ajax.php';

// Handler ajax Pool
require_once get_template_directory() . '/pools/pool_ajax.php';

// Handler ajax Pool
require_once get_template_directory() . '/pools/functions/code_promo_ajax.php';

// menus
function ad_register_menus() {
    register_nav_menus( array( 'main_menu' => __( 'Main Menu', 'ad') ) );
    register_nav_menus( array( 'footer' => __( 'Menu footer', 'ad') ) );
}
add_action( 'init', 'ad_register_menus' );

// archive menu filter
require get_template_directory() . '/functions/global_function.php';
require get_template_directory() . '/functions/archive_menu_filter.php';
require get_template_directory() . '/functions/pagination.php';
require get_template_directory() . '/functions/ad_search_form.php';
require_once get_template_directory() . '/functions/filter_code.php';
require get_template_directory() . '/functions/sidebar.php';
require get_template_directory() . '/inc/ad.customizer.php';

require get_template_directory() . '/pools/pool.php';
require get_template_directory() . '/pools/functions/export_csv.php';

function login_page() {
 echo '<style type="text/css">
 h1 a {background: #fff url('.get_template_directory_uri().'/images/logo.png) center no-repeat !important; background-size: contain !important;
     width: 150px !important; height: 150px!important; border-radius: 50%; border: 1px solid #fff;}
 </style>';
}
add_action('login_head', 'login_page');

function logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'logo_url' );
 
function logo_title() {
    return 'Aller vers la page d\'accueil';
}
add_filter( 'login_headertitle', 'logo_title' );

/* Allow SVG */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// ctp meta box
// inject cpt archives meta box
add_action( 'admin_head-nav-menus.php', 'inject_cpt_archives_menu_meta_box' );
function inject_cpt_archives_menu_meta_box() {
    add_meta_box( 'add-cpt', __( 'CPT Archives', 'default'), 'wp_nav_menu_cpt_archives_meta_box', 'nav-menus', 'side', 'default' );
}
function wp_nav_menu_cpt_archives_meta_box () {
    /* get custom post types with archive support */
    $post_types = get_post_types ( array ( 'show_in_nav_menus' => true, 'has_archive' => true), 'object' );
    /* hydrate the necessary object properties for the walker */
    foreach ( $post_types as &$post_type ) {
        $post_type->classes = array();
        $post_type->type = $post_type->name;
        $post_type->object_id = $post_type->name;
        $post_type->title = $post_type->labels->name;
        $post_type->object = 'cpt-archive';
    }
    $walker = new Walker_Nav_Menu_Checklist( array() );
?>
    <div id="cpt-archive" class="posttypediv">
      <div id="tabs-panel-cpt-archive" class="tabs-panel tabs-panel-active">
        <ul id="ctp-archive-checklist" class="categorychecklist form-no-clear">
          <?php echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $post_types ), 0, (object)array( 'walker' => $walker )); ?>
        </ul>
      </div>
    </div>
    <p class="button-controls">
      <span class="add-to-menu">
        <input type="submit"<?php disabled( $nav_menu_selected_id, 0 ); ?> class="button-secondary submit-add-to-menu" value="<?php esc_attr_e( 'Add to Menu' ); ?>" name="add-ctp-archive-menu-item" id="submit-cpt-archive" />
      </span>
    </p>
<?php
}

function register_team_show_case_setting() {
//register our settings
    register_setting('my_team_show_case_setting', 'my_file_upload');
}
add_action('admin_init', 'register_team_show_case_setting');

/* page d'option */
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Option Page',
        'menu_title'    => 'Option Page',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => true
    ));
}

// Add Role 
$role_moderateur = get_role( 'Moderateur' );
if($role_moderateur ){
    if(empty($role_moderateur->capabilities)){
        $role_moderateur->add_cap( 'manage_options', true );            
        $role_moderateur->add_cap( 'read', true );            
    } 

    $role_moderateur->add_cap( 'edit_others_posts', false ); 
    $role_moderateur->add_cap( 'upload_files', false ); 
    $role_moderateur->add_cap( 'edit_published_posts', false ); 
    $role_moderateur->add_cap( 'publish_posts', false ); 
    $role_moderateur->add_cap( 'delete_posts', false );  
}else {
    add_role('Moderateur', 'Moderateur');
    $role_moderateur = get_role( 'Moderateur' );
    $role_moderateur->add_cap( 'manage_options', true ); 
    $role_moderateur->add_cap( 'read', true );
    $role_moderateur->add_cap( 'edit_others_posts', false ); 
    $role_moderateur->add_cap( 'upload_files', false ); 
    $role_moderateur->add_cap( 'edit_published_posts', false ); 
    $role_moderateur->add_cap( 'publish_posts', false ); 
    $role_moderateur->add_cap( 'delete_posts', false );  
}

function remove_acf_options_page() {
    $current_user   = wp_get_current_user();
    $role_name      = $current_user->roles[0];
    if ( $role_name == 'Moderateur' ) {
        remove_menu_page( 'export-personal-data.php' );
        remove_menu_page('options-general.php');
        remove_menu_page('duplicator-pro'); 
        remove_menu_page('cptui_main_menu'); 
        remove_menu_page('wpfastestcacheoptions'); 
        remove_menu_page('index.php'); 
        remove_menu_page('edit.php?post_type=acf-field-group');
    }   
}
add_action('admin_init', 'remove_acf_options_page', 99);

/* Cron */
add_action( 'remember_email', 'send_remember_email' );
function send_remember_email(){
    $cc_email = get_field('cc_email', 'option');
    $lists = get_customer_unsuccessful_step();
    if(!empty($lists)){
        foreach ($lists as $key => $list) :
            $Cid = $list->customer_id;
            $Nom = $list->nom;
            $Prenom = $list->prenom;
            $Phone = $list->telephone;
            $Mail = $list->email;
            $Adresse = $list->adresse;
            $Ville = $list->ville;
            $Cp = $list->code_postal;
            $Remind = $list->remind;
            $lastStep = $list->step;
            $Date = $list->last_modify;
            $PoolTime = $list->pool_time;
            $Url_site = site_url();

            $subject = "N’oubliez pas de finaliser votre configuration de piscine Olsen&G";            

            ob_start();
                include("pools/email/cron.php");
            $body = ob_get_clean();        

            $headers = array("From: $Mail", 'Content-Type: text/html; charset=UTF-8');
            if($cc_email) {
                $headers[] = "Cc: $cc_email";
            }

            // Function to change sender name
            function wpb_sender_name( $original_email_from ) {
                return 'Piscine Olsen & G';
            }
            add_filter( 'wp_mail_from_name', 'wpb_sender_name' );

            if(@wp_mail( $Mail, $subject, $body, $headers )){
                update_customer_status($Cid);
            }
        endforeach;        
    }
}
