<?php get_header(); ?>

<?php get_template_part('pools/step/head'); ?>

<!--ABOUT-->
<section>
    <div id="csi-about" class="csi-about">
        <div class="csi-inner">
            <div class="container">
                <?php while ( have_posts() ) : the_post(); ?>
                <div class="row">                    
                    <div class="col-sm-12 col-md-offset-2 col-md-8">
                        <div class="csi-about-content-area csi-about-content-area-left">
                            <div class="csi-heading">
                                <h1 class="heading"><?php the_title(); ?></h1>
                            </div>
                            <div class="csi-about-content">
                                <?= the_content(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endwhile; wp_reset_query(); ?>
            </div><!-- //.CONTAINER -->
        </div><!-- //.INNER -->
    </div>
</section>
<!--ABOUT END-->

<?php get_footer(); ?>