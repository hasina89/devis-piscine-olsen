<?php 
add_filter( 'wp_get_nav_menu_items', 'cpt_archive_menu_filter', 10, 3 );

function cpt_archive_menu_filter( $items, $menu, $args ) {

    foreach ( $items as &$item ) {

        if ($item->object != 'cpt-archive') continue;

        $item->url = get_post_type_archive_link( $item->type );

        if ( get_query_var( 'post_type' ) == $item->type ) {

            $item->classes [] = 'current-menu-item';

            $item->current = true;

        }

    }

    return $items;

}

 ?>