<?php 
/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		// 'aside',
		// 'image',
		'video',
		// 'quote',
		// 'link',
		// 'gallery',
		'audio',
	) );

	// cpt format 
	function wpc_init_theme() {
		add_post_type_support('actualite', 'post-formats');
		add_post_type_support('citation', 'post-formats');
		add_post_type_support('parole', 'post-formats');
		add_post_type_support('enseignement', 'post-formats');
		add_post_type_support('pensee', 'post-formats');
		add_post_type_support('temoignage', 'post-formats');
		add_post_type_support('musique', 'post-formats');
		add_post_type_support('lifestyle', 'post-formats');
		add_post_type_support('homme', 'post-formats');
	}
	add_action('init', 'wpc_init_theme');

 ?>