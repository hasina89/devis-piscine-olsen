<?php 
add_filter( 'comment_form_defaults', 'juiz_manage_default_fields');
 
// $default contient tous les messages du formulaire de commentaire
// il contient également "comment_field", le textarea du message
 
if ( !function_exists('juiz_manage_default_fields')) {
   function juiz_manage_default_fields( $default ) {
 
      // Récupération des infos connues sur le visiteur
      // Permet de pré-remplir nos nouveaux champs
 	
      $commenter = wp_get_current_commenter();
 
      // Suppression d'un champ par défaut parmi : author, email, url
 	
		unset ( $default['fields']['url'] );

		$default['fields']   =  array(
		
        'author' => '<div class="form-group half_width">' . '<label for="author">' . __( 'Name' ) . ( ' <span class="required">*</span>') . '</label> ' .
                    '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245"  placeholder= "Nom" class="form-control" /></div>',
        'email'  => '<div class="form-group half_width"><label for="email">' . __( 'Email' ) . ( ' <span class="required">*</span>' ) . '</label> ' .
                    '<input id="email" name="email" ' . ( 'type="email"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . ' placeholder= "Email" class="form-control" /></div>', 
    );
		
 	
      // On retourne le tableau des champs
      return $default;
   }
}

function message($arg) {
    $arg['comment_field'] = '<div class="form-group"><label for="comment">' . _x( 'Commentaire', 'noun' ) . '</label>
    <textarea id="comment" name="comment" aria-required="true" placeholder= "Commentaire" class="form-control" ></textarea></div>';
    return $arg;
}

add_filter('comment_form_defaults', 'message');

// emplacement du message
function move_comment_field( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['comment'] = $comment_field;
	return $fields;
}

add_filter( 'comment_form_fields', 'move_comment_field' );

function awesome_comment_form_submit_button($button) {
	$button =
		'<input name="submit" type="submit" class="form_submit_comment btn btn-primary" tabindex="5" id="submit" value="Envoyer" />' .
		get_comment_id_fields();
	return $button;
}
add_filter('comment_form_submit_button', 'awesome_comment_form_submit_button');










?>