<?php 
function ad_pagination($pages = '', $range = 2)
{  
     $showitems = ($range * 6)+1;  
     global $paged;
     if(empty($paged)) $paged = 1;
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a class='arrow_page' href='".get_pagenum_link(1)."'><i class='fa fa-angle-double-left'></i></a>";
         if($paged > 1 && $showitems < $pages) echo "<a class='arrow_page' href='".get_pagenum_link($paged - 1)."'><i class='fa fa-angle-left'></i></a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a class='arrow_page' href='".get_pagenum_link($paged + 1)."'><i class='fa fa-angle-right'></i></a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a class='arrow_page' href='".get_pagenum_link($pages)."'><i class='fa fa-angle-double-right'></i></a>";
         echo "</div>\n";
     }
}

// page filter
function get_sfilter_page($num) {
    $query_string = $_SERVER['QUERY_STRING'];
    $params = explode('&', $query_string);
    $key = array_pop(array_keys($params));
    $last_param = $params[$key];
    $link_pg = 'archivist_page='.$num;
    $qlq_param = str_replace($last_param, $link_pg, $query_string);
    return $qlq_param;
}


function ad_pagination_filter($pages = '', $range = 2)
{  
    global $wp;
    $current_url = get_home_url(null, $wp->request, null);    
    $query_string = $_SERVER['QUERY_STRING'];
    $params = explode('&', $query_string);
    $key = array_pop(array_keys($params));
    $last_param = $params[$key];
    // $page_post = explode('=', $last_param);
    // $k = array_pop(array_keys($page_post));
    // $the_page = $page_post[$k];
    // echo $last_param;           

    $showitems = ($range * 6)+1;  
    global $paged;
    $paged = $_GET['archivist_page'];
    if(empty($paged)) $paged = 1;
    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
         $pages = 1;
        }
    }   

     if(1 != $pages)
     {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a class='arrow_page' href='".get_sfilter_page(1)."'><i class='fa fa-angle-double-left'></i></a>";
         if($paged > 1 && $showitems < $pages) echo "<a class='arrow_page' href='".get_sfilter_page($paged - 1)."'><i class='fa fa-angle-left'></i></a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_sfilter_page($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a class='arrow_page' href='".get_sfilter_page($paged + 1)."'><i class='fa fa-angle-right'></i></a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a class='arrow_page' href='".get_sfilter_page($pages)."'><i class='fa fa-angle-double-right'></i></a>";
         echo "</div>\n";
    }
     
}

 ?>