<?php 
function ad_search_form() { ?>
	
<form role="search" method="get" id="searchform"
    class="searchform" action="<?php echo site_url(); ?>">
    <div class="input_block_search_side">        
        <input type="text" placeholder="Rechercher dans les articles" class="field_search_side" value="<?php echo get_search_query(); ?>" name="s" id="s" />
        <input type="submit" id="searchsubmit" class="btn_search"
            value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" />
    </div>
</form>
<?php }
?>