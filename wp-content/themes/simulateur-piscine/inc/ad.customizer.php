<?php

class CadCustomizer 
{
    function register( $wp_customize ) {       
        
        // section header social network
        $wp_customize->add_section(
            'ad_social_network_section',
            array(
                'title'  => __( 'Contact & Social', 'ad' ),
                'priority' => 100,
                'capability' => 'edit_theme_options',
                'description' => '',
           )
        );        
        // lien facebook
        $wp_customize->add_setting(
            'facebook_link',
            array(
                'default' => ''
           )
        );
        // lien facebook
        $wp_customize->add_setting(
            'twitter_link',
            array(
                'default' => ''
           )
        );
        // lien facebook
        $wp_customize->add_setting(
            'instagram_link',
            array(
                'default' => ''
           )
        );
        // lien facebook
        $wp_customize->add_setting(
            'email_master',
            array(
                'default' => ''
           )
        );
        // lien facebook
        $wp_customize->add_setting(
            'telephone1',
            array(
                'default' => ''
           )
        );
        // lien facebook
        $wp_customize->add_setting(
            'telephone2',
            array(
                'default' => ''
           )
        );     
        // lien facebook
        $wp_customize->add_setting(
            'adresse',
            array(
                'default' => ''
           )
        );   
        
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                'facebook_link',
                array(
                    'label' => __( 'Lien Facebook', 'ad' ),
                    'section' => 'ad_social_network_section',
                    'settings' => 'facebook_link',
                    'type' => 'text'
                )
            )
        );
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                'twitter_link',
                array(
                    'label' => __( 'Lien Twitter', 'ad' ),
                    'section' => 'ad_social_network_section',
                    'settings' => 'twitter_link',
                    'type' => 'text'
                )
            )
        );
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                'instagram_link',
                array(
                    'label' => __( 'Lien Instagram', 'ad' ),
                    'section' => 'ad_social_network_section',
                    'settings' => 'instagram_link',
                    'type' => 'text'
                )
            )
        );
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                'email_master',
                array(
                    'label' => __( 'Email', 'ad' ),
                    'section' => 'ad_social_network_section',
                    'settings' => 'email_master',
                    'type' => 'text'
                )
            )
        );
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                'telephone1',
                array(
                    'label' => __( 'Téléphone 1', 'ad' ),
                    'section' => 'ad_social_network_section',
                    'settings' => 'telephone1',
                    'type' => 'text'
                )
            )
        );
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                'telephone2',
                array(
                    'label' => __( 'Téléphone 2', 'ad' ),
                    'section' => 'ad_social_network_section',
                    'settings' => 'telephone2',
                    'type' => 'text'
                )
            )
        );        
        $wp_customize->add_control(
            new WP_Customize_Control(
                $wp_customize,
                'adresse',
                array(
                    'label' => __( 'Adresse', 'ad' ),
                    'section' => 'ad_social_network_section',
                    'settings' => 'adresse',
                    'type' => 'text'
                )
            )
        );
    }

    function live_preview() {
        wp_enqueue_script(
            'ad-themecustomizer',
            get_template_directory_uri() . '/js/ad-customizer.js',
            array( 'jquery', 'customize-preview' ),
            '1.0',
            true
       );
    }
}

add_action ('customize_register' , array ('CadCustomizer' , 'register')) ;
add_action ('customize_preview_init' , array ('CadCustomizer' , 'live_preview')) ;