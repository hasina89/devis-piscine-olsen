<?php

/*
 * somaco menu walker useful classes
*/

// top menu walker
class CTopmenuWalker extends Walker_Page {

    //start of the sub menu wrap
    function start_lvl( &$output, $depth ) {
        $output .= '' ;
    }
 
    //end of the sub menu wrap
    function end_lvl( &$output, $depth ) {
        $output .= '' ;
    }
 
    //add the description to the menu item output
    function start_el ( &$output, $item, $depth, $args ) {

        global $wp_query, $post;

        /*$locations = get_nav_menu_locations();
        $menu = wp_get_nav_menu_object( $locations['header_menu'] );
        $menu_items = wp_get_nav_menu_items( $menu->term_id );*/

        // check if parent
        $children = array();
        $children_res = get_posts( array(
                'post_type' => 'nav_menu_item', 
                'nopaging' => true, 
                'numberposts' => 1, 
                'meta_key' => '_menu_item_menu_item_parent', 
                'meta_value' => $item->ID
            )
        );

        foreach ($children_res as $children_row) {
            $children[$children_row->ID] = $children_row;
        }
        ksort( $children );
        $is_parent = ( !empty( $children ) ) ? true : false;

        if ( ( $item->object_id == $post->ID ) || $item->current_item_parent ){
            $a_class = " active";
        }

        if ( $is_parent ) {
            $li_class = "dropdown";
            $a_class = "dropdown-toggle" . $a_class;
            $a_datatogle = ' data-toggle="dropdown"';
        }

        if ( is_archive() ){
            foreach ($children as $child_for_archive) {
                $post_meta_archive = get_post_meta( $child_for_archive->ID, '_menu_item_object_id', true );
                if ( !empty( $post_meta_archive ) ) {
                    $post_item_archive = get_page( $post_meta_archive );
                    $a_class = ( $post_item_archive->ID == $post->ID ) ? "active" : "";
                } else {
                    $cpt_type = get_post_meta( $child_for_archive->ID, '_menu_item_type',  true );
                    $a_class = ( $cpt_type == $post->post_type ) ? "active" : "";
                }
            }
        }

        $sidebar_post_type  = get_post_type( $item->object_id );
        $object_post = get_post( $item->object_id );
        if ( empty( $item->menu_item_parent ) ){
            $item_output = "<li class='" . $li_class . "'>";
            $item_output .= '<a href="' . $item->url . '" title="' . $item->title . '" class="' . $a_class . '">';
            $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
            $item_output .= '</a>';
            if ($is_parent) {
                $item_output .= '<ul class="dropdown-menu">';
                foreach ($children as $child) {
                    $item_output .= '<li>';
                    $title = "";
                    $link = "";
                    $active_child = "";
                    $post_meta = get_post_meta( $child->ID, '_menu_item_object_id', true );
                    if ( !empty( $post_meta ) ) {
                        $post_item = get_page( $post_meta );
                        $title = $post_item->post_title;
                        $link = esc_url( get_permalink ( $post_item->ID ) );
                        $active_child = ( $post_item->ID == $post->ID ) ? 'class="current"' : '';
                    } else {
                        $cpt_type = get_post_meta( $child->ID, '_menu_item_type',  true );
                        $title = $child->post_title;
                        $link = esc_url ( get_post_type_archive_link ( $cpt_type ) );
                        $active_child = ( $cpt_type == $post->post_type ) ? 'class="current"' : '';
                    }
                    $item_output .= '<a href="' . $link . '" title="' . $title . '" ' . $active_child . '>';

                    $item_output .= $title;
                    $item_output .= '</a>';
                    $item_output .= '</li>';
                }
                $item_output .= '</ul>';
            }
            $item_output .= "</li>";
        }
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}
?>