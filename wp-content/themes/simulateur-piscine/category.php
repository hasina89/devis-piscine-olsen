<?php get_header(); ?>

<?php 


// Actu plus populaire
$ad_popular_args = array (
    'posts_per_page'    => 5,
    'range'             => 'daily',
    'freshness'         => false,
    'meta_key'          =>'post_views_count',
    'orderby'           =>'meta_value_num',
    'order'             =>'DESC'
);


?>

    <div class="banner_title">
        <img src="<?php echo get_template_directory_uri () ; ?>/img/bg_single.png" alt="">
        <h2 class="title_single"><?php single_cat_title(); ?></h2>
        <div class="bread_crumb">
            <ul>
                <li><a title="Accueil" rel="nofollow" href="<?php echo esc_url( home_url( '/' ) ); ?>">Accueil</a></li>
                <li><span><?php single_cat_title(); ?></span></li>
            </ul>
        </div>
    </div>
    <!-- master content -->
    <div id="master_content">
        <div class="inner_master_content">
            <div class="container">
                <div class="row">  
                    <div class="left_content col-sm-8">
                        <div class="inner_right_content row">  
                            <div class="col-xs-12">  
                                <h2 class="title_cat subtitle_secondary"><?php single_cat_title(); ?></h2>                       
                                <div class="row all_post_cat">

                                    <?php if( have_posts()) : ?> 
                                        <?php  while ( have_posts() ) : the_post(); ?>

                                            <?php 
                                                // Category 
                                                $category = get_the_category(); 
                                                $category_id = get_cat_ID( $category[0]->cat_name );
                                                $category_link = get_category_link( $category_id );
                                                // format
                                                $format = get_post_format( $post->ID );  
                                                if (!empty($format)) {
                                                    $format = get_post_format( $post->ID ); 
                                                }else {
                                                    $format = 'quote';
                                                }
                                            ?>
                                            <div class="col-xs-12 col-sm-6 item_post_cat only_cat">
                                                <div class="row no_marg cadre_box">
                                                    <div class="col-xs-5 col-sm-12 img_thumb_box">
                                                        <?php if(has_post_thumbnail()) : ?>
                                                            <?php
                                                                $url_img = get_the_post_thumbnail_url();                  
                                                                $fake_url = site_url().'/wp-includes/images/';
                                                                if (stristr($url_img, $fake_url)) {
                                                                    $ph_img = '';
                                                                }else {
                                                                    $ph_img = $url_img;
                                                                }
                                                            ?>
                                                            <?php if(!empty($ph_img)) : ?>
                                                                <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo $ph_img; ?>" alt="" ></a>
                                                            <?php else: ?>
                                                                <a class="<?php echo 'at_'.$format; ?>" href="<?php  the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><img src="<?php echo catch_first_img(); ?>" ></a>
                                                            <?php endif; ?>
                                                        <?php else : ?>
                                                            <a class="<?php echo 'at_'.$format; ?>" href="<?php the_permalink(); ?>"><img src="<?php echo catch_first_img() ?>" alt="" ></a>
                                                        <?php endif; ?>
                                                        <a class="cat_sub_img" href="<?php echo $category_link; ?>"><?php $category = get_the_category(); echo $category[0]->cat_name; ?></a>

                                                    </div>
                                                    <div class="inner_item_p_c col-xs-7 col-sm-12 ">
                                                        <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                        <?php 
                                                            $extrait = substr(get_the_excerpt(), 0, 200);
                                                            $extrait .= ' ...';
                                                        ?>
                                                        <p><?php echo $extrait; ?></p>                
                                                        <div class="link_to row">
                                                            <div class="left_link_to col-sm-8">
                                                                <a class="author_cat" href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_login' ) ); ?>"><?php the_author(); ?></a>
                                                                <span class="date_cat">- <?php $post_date = get_the_date( 'j M, Y' ); echo $post_date; ?></span>
                                                            </div>
                                                            <div class="right_link_to col-sm-4">
                                                                <span class="total_comment_cat"><?php comments_number( '0', '1', '%' ); ?> <i class="fa fa-comments-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            
                                        <?php  endwhile; wp_reset_query(); ?>

                                        <div class="page_it col-xs-12">
                                            <?php the_posts_pagination( array(
                                            'prev_text'          => __( 'Précédente', 'cm' ),
                                            'next_text'          => __( 'Suivante', 'cm' )
                                            ) ); 
                                        ?></div>

                                    <?php else : ?>
                                        <div class="col-xs-12">
                                            <p>Aucun article relié à cette catégorie.</p>
                                        </div>
                                    <?php endif; ?>

                                    
                                                                 
                                </div>
                            </div>                           
                        </div>
                    </div>
                    <!-- End left -->

                    <!-- Sidebar right -->
                    <?php get_template_part( 'template-parts/content', 'sidebar-right' ); ?>
                    
                </div>
            </div>          
        </div>  


<?php get_footer(); ?>