jQuery(document).ready(function($) {	
	// FORM
    $('.placeholder').click(function() {
	  	$(this).siblings('input').focus();
	});
	$('.form-control').on('focus', function() {
	  	$(this).siblings('.placeholder').addClass('focus').animate({top:'-13px'}, 400);
	});
	$('.form-control').blur(function() {
	  	var $this = $(this);
	  	if ($this.val() == 0){
	    	$(this).siblings('.placeholder').removeClass('focus').animate({top:10 }, 400);
	  	}
	});
	$('.form-control').blur();

	$(".page label.col").click(function(){
		var vis=$(this);
		var check = vis.find('input[type=checkbox]');
		var radio = vis.find('input[type=radio]');

		if(radio.length > 0){
			$(".page label.col").removeClass('selected')
			vis.addClass('selected');
		}

		if(check.length > 0){
			$('.select').change(function(){
				var selected =  $('.select').find("option:selected").attr("data-form");
				if (selected == 'modele') {
					$(this).parents('label').addClass('selected');
				}else {
					$(this).parents('label').removeClass('selected');
				}
			});
			$(check).change(function(){
				var cvis = $(this);
				if (cvis.is(':checked')) {
					cvis.parent('label').addClass('selected');
				}else {
					cvis.parent('label').removeClass('selected');
				}
			});			
		}
	});


	// HAUTEUR ASIDE
	function h_aside(){
		var rstep=$('aside .recap .recap-step');
		if(rstep.length > 0){
			var h_recap=$('aside .recap .recap-step').height();
			if (h_recap>300){
				$('aside').addClass('scroll')
			} else{
				$('aside').removeClass('scroll')
			}			
		}
	}
	h_aside()

	// Active form
	$('.go').click(function(e){
		$('.formulaire').removeClass('off');
		$(this).addClass('off');
		e.preventDefault();
		return false;
	});

	// TOOLTIPS
	$('.btn-tooltips').click(function() {
	  $(this).siblings().toggleClass('active').focus();
	});
	
	/* Number format */
	function number_format(number, decimals=0, dec_point='', thousands_sep=' ') {
        number = number.toFixed(decimals);
        var nstr = number.toString();
        nstr += '';
        x = nstr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? dec_point + x[1] : '';
        var rgx = /(\d+)(\d{3})/;

        while (rgx.test(x1)){
            x1 = x1.replace(rgx, '$1' + thousands_sep + '$2');
        }
        return x1 + x2;
    }


	function changeNumber(){
		if ($('form').hasClass('step8')){
			$(".numbers-row .button").on("click",function(){
				var $button=$(this);
				var oldValue=$button.parent().find("input").val();
				if($button.text()=="+"){
					var newVal=parseFloat(oldValue)+1;
				}else{
					if(oldValue>6){
						var newVal=parseFloat(oldValue)-1;
					}else{newVal=6;}
				}
				$button.parent().find("input").val(newVal);
			});
		}else {
			$(".numbers-row .button").on("click",function(){
				var $button=$(this);
				var oldValue=$button.parent().find("input").val();
				if($button.text()=="+"){
					var newVal=parseFloat(oldValue)+1;
				}else if($button.text()=="-"){

					if(oldValue>1){
						var newVal=parseFloat(oldValue)-1;
					}else{newVal=1;}
				}
				$button.parent().find("input").val(newVal);
			});
		}
	}
	changeNumber()
	
	/* Change value of price on Click */
	function changeValPrice(cible, element, nbVariable=false, prix_par_m=false, nb_distance=false){
		if($(element).length > 0){
			$(cible).click(function(){
				var vis = $(this);
				var content_elt = vis.find(element).val();
				var container = $('.recap').find('.price');
				var oldprice = container.attr('oldprice');				

				var pPm = vis.find(prix_par_m).val();
				var nbDistance = vis.find(nb_distance).val();

				// console.log(nb_distance);

				if(nbVariable && vis.find(prix_par_m).length > 0){	
					var variable_price_elt;

					if(nb_distance == '.nb_unite'){
						variable_price_elt = parseInt(nbDistance) * parseInt(pPm);	
					}else {
						if(nbDistance <= 6){
							variable_price_elt = 1 * parseInt(pPm);
						}else {
							var realDistance = nbDistance - 5;
							variable_price_elt = parseInt(realDistance) * parseInt(pPm);		
						}
					}
						
					var total = number_format( parseInt(variable_price_elt) + parseInt(oldprice));
					container.find('span').text(total);
				}else {
					if(content_elt < 0){
						var total = number_format( parseInt(oldprice));
						container.find('span').text(total);
					}else {
						var total = number_format( parseInt(content_elt) + parseInt(oldprice));
						container.find('span').text(total);												
					}
				}

			})
		}
	}

	/* Change value on Click */
	function changeValClick(cible, element, conteneur, imgsrc=false){
		if($(element).length > 0){
			$(cible).click(function(){
				var vis = $(this);
				if(imgsrc == 'src'){
					var content_elt = vis.find(element).attr('src');
					var container = $('.recap').find(conteneur);
					container.attr('src', content_elt);
				}else {
					var content_elt = vis.find(element).val();
					var container = $('.recap').find(conteneur);
					container.text(content_elt);
				}		

			});
		}
		
	}

	/* Change value Sidebar on Click */
	function changeValRecapClick(cible, element, element_price, conteneur, nbVariable=false, prix_par_m=false, nb_distance=false){
		if($(element).length > 0){
			$(cible).click(function(){
				var vis = $(this);
				var content_elt = vis.find(element).val();
				var price_elt = vis.find(element_price).val();
				var pcontainer = $('.recap-step').find(conteneur);
				var container = pcontainer.find('span');
				var container_price = pcontainer.find('.price-step');

				var pPm = vis.find(prix_par_m).val();
				var nbDistance = vis.find(nb_distance).val();

				var txt_price;			
				if(nbVariable && vis.find(prix_par_m).length > 0){
					var variable_price_elt;
					if(nb_distance == '.nb_unite'){
						variable_price_elt = parseInt(nbDistance) * parseInt(pPm);	
					}else {

						if(nbDistance <= 6){
							variable_price_elt = 1 * parseInt(pPm);
						}else {
							var realDistance = nbDistance - 5;
							variable_price_elt = parseInt(realDistance) * parseInt(pPm);		
						}	
					}

					container.text( content_elt);

					if(variable_price_elt == 0){
						txt_price = 'Inclus';
					}else {
						txt_price = '+ '+ variable_price_elt + ' €';
					}

					container_price.text( txt_price );
					pcontainer.show();	
				}else {
					container.text( content_elt);
					if(price_elt == 0){
						txt_price = 'Inclus';
					}else if(price_elt == -1){
						txt_price = ' Sur Devis';
					}else if( price_elt == -2){
						txt_price = ' Sur Devis';
					}else {
						txt_price = '+ '+ price_elt + ' €';
					}
					container_price.text( txt_price );
					pcontainer.show();						
				}

			})
		}
	}

	function changeValRecapClickOption(cible, element, element_price, conteneur){
		if($(element).length > 0){			

			$(cible).click(function(){
				var vis = $(this);

				var Cx = vis.find('input[type=checkbox]');
				var lstOption = $('.lstOption');
				var ulParent = lstOption.find('ul');
				var all_options = $('.all_options');
				var content_elt = vis.find(element);
				var container = $('.recap').find('.price');
				var oldprice = container.attr('oldprice');	

				lstOption.show();

				var prices = [];
				var names = [];
				var namePrices = [];
				$(Cx).change(function(){
					$("input:checkbox:checked").each(function(){
						var vice = $(this);
						var CxParent = vice.parent('label');
						var CxItemPrice = CxParent.find('input[name=prix_option_supplementaire]').val();
						var CxItemName = CxParent.find('input[name=option_supplementaire_name]').val();
						var data = {};

						data.nom = CxItemName;
						data.prix = CxItemPrice;

					    prices.push(CxItemPrice);
					    names.push(CxItemName);
					    namePrices.push(data);
					});

					if (names.length === 0) {
					    lstOption.hide();
					}

					/* Names */
					ulParent.empty();
					$.each(namePrices, function(index, option) {
						// console.log(option.nom);
						if(option.prix > 0){
							$( '<li>'+ option.nom +'<b class="price-step">+'+option.prix+'€</b></li>' ).appendTo( ulParent );
						}else if( option.nom == 'Enlèvement des terres' || option.nom == 'Volet Hors-Sol PVC' ){							
							$( '<li>'+ option.nom +'<b class="price-step">Sur Devis</b></li>' ).appendTo( ulParent );
						}else {
							$( '<li>'+ option.nom +'<b class="price-step">Inclus</b></li>' ).appendTo( ulParent );							
						}
					    
					});

					/* Prices*/
					var total_item_price = 0;
					$.each(prices, function(index, value) {
					    total_item_price = parseInt(total_item_price) + parseInt(value);
					});

					var total = number_format( parseInt(total_item_price) + parseInt(oldprice));
					container.find('span').text(total);	
				});

			})

		}
	}

	// Option supp check_uncheck
	function check_uncheck_all(cible, element){
		if($(element).length > 0){
			$(cible).click(function(){

				var isChecked = $( cible + ' .cb_empty_value:checked').length;

				if(isChecked) {
					$(element + ' input[name="option_supp[]"]').each(function() { 
						this.checked = false; 
						$(this).removeAttr('id');
						$(element).removeClass('selected').addClass('disabled');
					});
				}else {
					$(element).removeClass('disabled');
					$(element + ' input[name="option_supp[]"]').each(function() { 
						this.checked = false; 
						var aid = $(this).attr('dataid');
						$(this).attr('id', aid );
					});
				}

			})
		}
	}	


	/* ---- MODELE ----*/
	changeValPrice('label', 'input[name=prix_modele]');
	changeValClick('label', 'input[name=numero_modele]', '.num-modele');
	changeValClick('label', '.plan-modele img', '.plan-modele img', 'src');

	/* ---- POSE ----*/
	changeValRecapClick('label', 'input[name=name_pose]', 'input[name=prix_pose]', '.item_pose');
	changeValPrice('label', 'input[name=prix_pose]');

	/* -----COULEUR ---*/
	changeValRecapClick('label', 'input[name=couleur_name]', 'input[name=prix_couleur]', '.item_couleur');
	changeValPrice('label', 'input[name=prix_couleur]');

	/* -----MARGELLE ---*/
	changeValRecapClick('label', 'input[name=choix_margelle_name]', 'input[name=prix_margelle]', '.item_margelle');
	changeValPrice('label', 'input[name=prix_margelle]');

	/* -----ECLAIRAGE ---*/
	changeValRecapClick('label', '.name_eclairage', '.prix_eclairage', '.item_eclairage', true, '.prix_par_unite', '.nb_unite');
	changeValPrice('label', '.prix_eclairage', true, '.prix_par_unite', '.nb_unite');	

	/* -----LOCAL TECHNIQUE ---*/
	changeValRecapClick('label', 'input[name=name_local_teknik]', 'input[name=prix_local_teknik]', '.item_localteck');
	changeValPrice('label', 'input[name=prix_local_teknik]');
	
	/* -----SKIMMER ---*/
	changeValRecapClick('label', 'input[name=skimmer_name]', 'input[name=prix_skimmer]', '.item_skimmer');
	changeValPrice('label', 'input[name=prix_skimmer]');

	/* -----ACCES MAISON ---*/
	changeValRecapClick('label', 'input[name=acces_maison_name]', 'input[name=prix_acces_maison]', '.item_acces');
	changeValPrice('label', 'input[name=prix_acces_maison]');

	/* ----- DISTANCE ---*/
	changeValRecapClick('label', 'input[name=distance_name]', 'input[name=distance_prix]', '.item_distance_local', true, 'input[name="prix_par_m"]', 'input[name="nb_distance"]');
	changeValPrice('label', 'input[name=distance_prix]', true, 'input[name="prix_par_m"]', 'input[name="nb_distance"]');

	/* -----OPTIONS SUPP ---*/
	changeValRecapClickOption('label', 'input[name=option_supplementaire_name]', 'input[name=prix_option_supplementaire]', '.item_acces');

	check_uncheck_all('.empty_value', '.other_values');

	/* -----TRAITEMENT DE L'EAU ---*/
	changeValRecapClick('label', 'input[name=eau_name]', 'input[name=prix_eau]', '.item_eau');
	changeValPrice('label', 'input[name=prix_eau]');

	$('.pvc-disabled.disabled').unbind("click");



	function scroll(){
		$( ".step3 .col " ).each(function( index ) {
			var vis = $(this);
			var cText = vis.find(".contentText");
			if(cText.length > 0){
				var h = vis.find(".contentText").height();
				if (h>229){
					vis.find('.blcScroll').addClass('show')

				} else{
					vis.find('.blcScroll').removeClass('show')
				}
				vis.find(".blcScroll").click(function() {
					vis.toggleClass('h-auto')
				});
			}
		});
	}
	scroll();

	/*--- CUSTOM SCROLLBAR ---*/
	(function($){
        $(window).on("load",function(){
            $.mCustomScrollbar.defaults.scrollButtons.enable=true;
            $("aside .recap .recap-step").mCustomScrollbar();
        });
    })(jQuery);

    // SCROLL //
	$(".scrollTo").click(function() {
		var c = $(this).attr("href");
		$('html, body').animate({ scrollTop: $("#" + c).offset().top }, 1000, "linear");
		return false;
	});

	
});
