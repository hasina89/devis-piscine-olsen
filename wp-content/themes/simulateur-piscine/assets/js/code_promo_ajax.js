
jQuery(function($){
    $('#btnsend').click(function (e) {
        e.preventDefault();

        var vis = $(this);
        var forme = $('#cpromo_fomr');             

        var txtButton = vis.text();
        var container = $('#result_cp');

        $.ajax(ajaxurl, {    
            data: { 
                'action' : 'code_promo_ajax', 
                'data' : forme.serialize()             
            },    
            type:"POST",
            beforeSend:function(){
                vis.text('Veuillez patienter ...');
                container.empty();
            },
            success:function(data){
                console.log(data);
                vis.text(txtButton);
                var stringData;
                stringData = $.parseJSON(data);
                container.empty();
                if(stringData.status == 1){
                    container.html('<p class="successMsg">'+ stringData.msg +'</p>');
                }else {
                    container.html('<p class="errorMsg">'+ stringData.msg +'</p>');                    
                }
                forme.get(0).reset();  
            }
        });
        return false;
    });
});