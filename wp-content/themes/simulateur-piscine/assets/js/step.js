/* ------- */

	$(window).on('load', function(){
		var inpute = $('input[name="type_pose"]');
		if($(inpute).is(':checked')){
			var vis = $(this);
			console.log(inpute);

			// POSE
			changeVal('label.selected', 'input[name=name_pose]', 'ul', 'item_pose');
			changeValPrice('label.selected', 'input[name=prix_pose]');

		}
	})

	function changeVal(cible, element, conteneur, classItem=false){
		var vis = $(cible);
		var content_elt = vis.find(element).val();
		var container = $('.recap').find(conteneur);
		container.html('<li class="'+classItem+'"><span>'+content_elt+'</span></li>');
	}

	function changeValPrice(cible, element){
		var vis = $(cible);
		var content_elt = vis.find(element).val();
		var container = $('.recap').find('.price');
		var oldprice = container.attr('oldprice');
		var total = parseInt(content_elt) + parseInt(oldprice);
		container.find('span').text(total);
	}

	/* ------------------------------ */

	function changeValClick(cible, element, conteneur, imgsrc=false){
		$(cible).click(function(){
			var vis = $(this);

			if(imgsrc == 'src'){
				var content_elt = vis.find(element).attr('src');
				var container = $('.recap').find(conteneur);
				container.attr('src', content_elt);
				// console.log(content_elt);
			}else {
				var content_elt = vis.find(element).val();
				// console.log(content_elt);
				var container = $('.recap').find(conteneur);
				container.text(content_elt);
			}			

		});
	}

	function changeValRecapClick(cible, element, conteneur, classItem=false){
		$(cible).click(function(){
			var vis = $(this);
			var content_elt = vis.find(element).val();
			var pcontainer = $('.recap-step').find(conteneur);
			var container = pcontainer.find('span');
			// var liste = '<li class="'+classItem+'"><span>'+content_elt+'</span></li>';
			container.text( content_elt);
			pcontainer.show();	
		})
	}

	// prix
	changeValClick('label', 'input[name=prix_modele]', '.ng_price');
	// numero
	changeValClick('label', 'input[name=numero_modele]', '.num-modele');
	// image
	changeValClick('label', '.plan-modele img', '.plan-modele img', 'src');

	/*-------- Couleur  */
	changeValRecapClick('label', 'input[name=couleur_name]', '.item_couleur');
	/* --------*/