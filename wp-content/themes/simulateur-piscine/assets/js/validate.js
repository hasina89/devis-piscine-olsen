	
/* Validate */
jQuery(document).ready(function($) {
	$(".formulaire").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",

		rules: {
			nom: {
				required: true,
				minlength: 2
			},
			prenom: {
				required: true,
				minlength: 2
			},
			code_postal: {
				required: true,
				number: true
			},
			pool_time: "required",
			telephone: {
				required: true,
				minlength: 5,
				number: true
			},
			email: {
				required: true,
				email: true
			},
			province: "required",
			cgu: "required"
		},
		messages: {

			nom: {
				required: "Nom : obligatoire",
				minlength: "Nom : Caractère minimum 2"
			},
			prenom: {
				required: "Prénom : obligatoire",
				minlength: "Prénom : Caractère minimum 2"
			},
			adresse: {
				required: "Adresse : obligatoire",
				minlength: "Adresse : Caractère minimum 2"
			},
			code_postal: {
				required: "Code postal : obligatoire",
				number: "Code postal : Veuillez saisir des chiffres"
			},
			pool_time: "Besoin Piscine : Champ obligatoire",
			ville: {
				required: "Ville : obligatoire",
				minlength: "Ville : Caractère minimum 2"
			},
			telephone: {
				required: "Télephone : obligatoire",
				minlength: "Télephone : Caractère minimum 2",
				number: "Télephone : Veuillez saisir des chiffres"
			},
			email: {
				required: "E-mail : obligatoire",
				email: "Veuillez saisir un email valide"
			},
			province: "Province : obligatoire",
			cgu: "RGPD : Champ obligatoire",

			
		}
	});
	$(".step2").validate({
		errorPlacement: function(error, element) {
		// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",

		rules: {
			prix_modele: {
				required: true,

			},
		},
		messages: {

		prix_modele: {
			required: "Choix obligatoire",
		},

		
		}
		});
	$(".step3").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",

		rules: {
			type_pose: {
				required: true,

			},
		},
		messages: {

			type_pose: {
				required: "Choix obligatoire",
			},

			
		}
	});
	$(".step4").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",

		rules: {
			choix_couleur: {
				required: true,

			},
		},
		messages: {

			choix_couleur: {
				required: "Choix obligatoire",
			},

			
		}
	});
	
	/*if($(".step5").length > 0) {

	}*/


	$(".step5").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",

		rules: {
			choix_margelle: {
				required: true,

			},
		},
		messages: {
			choix_margelle: {
				required: "Choix obligatoire",
			},

			
		}
	});
	$(".step6").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",

		rules: {
			eclairage_id: {
				required: true,

			},
		},
		messages: {

			eclairage_id: {
				required: "Choix obligatoire",
			},

			
		}
	});
	$(".step7").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",

		rules: {
			local_teknik: {
				required: true,

			},
		},
		messages: {

			local_teknik: {
				required: "Choix obligatoire",
			},

			
		}
	});
	$(".step8").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",

		rules: {
			distance_locale: {
				required: true,

			},
		},
		messages: {

			distance_locale: {
				required: "Choix obligatoire",
			},

			
		}
	});
	$(".step9").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",

		rules: {
			type_skimmer: {
				required: true,

			},
		},
		messages: {

			type_skimmer: {
				required: "Choix obligatoire",
			},

			
		}
	});
	$(".step10").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",
		rules: {
			type_acces_maison: {
				required: true,

			},
		},
		messages: {
			type_acces_maison: {
				required: "Choix obligatoire",
			},			
		}
	});
	$(".step11").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",
		rules: {
			option_supp: {
				required: true,

			},
		},
		messages: {
			option_supp: {
				required: "Choix obligatoire",
			},			
		}
	});
	$(".step12").validate({
		errorPlacement: function(error, element) {
			// Append error within linked label
			$( element ).closest( "form" ).find( "#error_form" ).append( error );
		},
		errorElement: "li",
		rules: {
			traitement_eau: {
				required: true,
			},
		},
		messages: {
			traitement_eau: {
				required: "Choix obligatoire",
			},			
		}
	});
});