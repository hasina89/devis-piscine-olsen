jQuery(document).ready(function() {
    // Function for Preview Image.
    jQuery(function() {
        jQuery(":file").change(function() {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
             }
        });
    }); 
    function imageIsLoaded(e) {
        jQuery('#message').css("display", "none");
        jQuery('#preview').css("display", "block");
        jQuery('#previewimg').attr('src', e.target.result);
    };
    // Function for Deleting Preview Image.
    jQuery("#deleteimg").click(function() {
        jQuery('#preview').css("display", "none");
        jQuery('#file').val("");
    });
    // Function for Displaying Details of Uploaded Image.
    jQuery("#submit").click(function() {
        jQuery('#preview').css("display", "none");
        jQuery('#message').css("display", "block");
    });
 });